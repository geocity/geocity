#!/bin/node

/**
 * Generate a SVG sprite at static/images/icons.svg
 * from icons present in assets/icons
 */

const path = require('node:path');
const fs = require('node:fs');
const glob = require('glob');
const SVGSpriter = require('svg-sprite');

const cwd = path.join(__dirname, '..');
const dest = path.join(cwd, 'geocity/apps/core/static/images');
const files = glob.sync('assets/icons/*.svg', { cwd });

const spriter = new SVGSpriter({
    dest,
    shape: {
        id: {
            generator(p) {
                const { name } = path.parse(p);
                return name;
            },
            transform: [],
        },
    },
    log: 'debug',
    svg: {
        doctypeDeclaration: false,
        xmlDeclaration: false,
    },
});

function loadFiles(spriter, files) {
    for (const file of files) {
        const filePath = path.join(cwd, file);
        spriter.add(
            path.resolve(filePath),
            file,
            fs.readFileSync(filePath, 'utf8')
        );
    }

    return spriter;
}

loadFiles(spriter, files).compile(
    {
        symbol: {
            dest: '.',
            sprite: 'icons.svg',
            example: false,
        },
    },
    (error, result) => {
        for (const type of Object.values(result.symbol)) {
            fs.mkdirSync(path.dirname(type.path), { recursive: true });
            fs.writeFileSync(type.path, type.contents);
        }
    }
);
