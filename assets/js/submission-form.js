document.addEventListener('DOMContentLoaded', () => {
  const saveAndQuitButton = document.querySelector('#save-and-quit-button');

  if (saveAndQuitButton) {
    saveAndQuitButton.addEventListener('click', () => {
      if (saveAndQuitButton.form) {
        const field = document.createElement('input');
        field.type = 'hidden';
        field.name = 'form-redirect-after-save';
        field.value = 'quit';
        saveAndQuitButton.form.appendChild(field);
        saveAndQuitButton.form.submit();
      }
    });
  }
});
