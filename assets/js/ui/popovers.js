import { offset, autoUpdate, computePosition } from '@floating-ui/dom';

const openPopovers = new WeakMap();

function positionPopover(e) {
  const popover = e.target;
  const placement = popover.dataset.popoverPlacement ?? 'bottom-start';
  const invoker = document.querySelector(
    `[popovertarget="${popover.getAttribute('id')}"]`
  );

  function updatePopoverPosition() {
    computePosition(invoker, popover, {
      placement,
      middleware: [offset(8)],
    }).then(({ x, y }) => {
      Object.assign(popover.style, {
        left: `${x}px`,
        top: `${y}px`,
      });
      popover.classList.add('is-positioned');
    });
  }

  if (e.newState === 'open') {
    openPopovers.set(
      popover,
      autoUpdate(invoker, popover, updatePopoverPosition)
    );
    invoker.classList.add('is-popover-open');
  } else {
    const destroy = openPopovers.get(popover);
    if (typeof destroy === 'function') {
      destroy();
      openPopovers.delete(popover);
      popover.classList.remove('is-positioned');
    }
    invoker.classList.remove('is-popover-open');
  }
}

document.addEventListener('DOMContentLoaded', () => {
  document
    .querySelectorAll('[popover]')
    .forEach((p) => p.addEventListener('toggle', positionPopover));
});
