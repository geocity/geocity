document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('[data-role="show-dialog"]').forEach((p) =>
    p.addEventListener('click', () => {
      const dialog = document.querySelector(`#${p.dataset.dialog}`);
      if (dialog) {
        dialog.showModal();
        dialog.dispatchEvent(new Event('open'));
      }
    })
  );

  document.querySelectorAll('[data-role="hide-dialog"]').forEach((p) =>
    p.addEventListener('click', () => {
      const dialog = document.querySelector(`#${p.dataset.dialog}`);
      dialog?.close();
    })
  );
});
