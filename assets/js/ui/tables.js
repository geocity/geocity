document.addEventListener('DOMContentLoaded', () => {
  // Handle table’s select all checkbox
  document
    .querySelectorAll('[data-role="table-select-all"]')
    .forEach((checkbox) => {
      const table = checkbox.closest('table');

      if (!table) return;

      const controlledCheckboxes = table.querySelectorAll(
        'tbody input[type="checkbox"]:not(:disabled)'
      );

      checkbox.addEventListener('change', () => {
        controlledCheckboxes.forEach((c) => {
          c.checked = checkbox.checked;
          c.dispatchEvent(new Event('change'));
        });
      });
    });
});
