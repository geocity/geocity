import AddressAutocomplete from '@/components/AddressAutocomplete';

document.addEventListener('DOMContentLoaded', () => {
  const autocompletes = document.querySelectorAll(
    '[data-address-autocomplete]'
  );

  autocompletes.forEach((a) => {
    new AddressAutocomplete(a);
  });
});
