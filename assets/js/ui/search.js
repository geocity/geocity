import GlobalSearchForm from '@/components/GlobalSearchForm';

document.addEventListener('DOMContentLoaded', () => {
  // Focus the search field when opening the dropdown
  const globalSearchDropdown = document.querySelector(
    '#global-search-dropdown'
  );
  globalSearchDropdown?.addEventListener('toggle', (e) => {
    if (e.newState === 'open') {
      setTimeout(() => {
        globalSearchDropdown
          .querySelector('[data-role="search-input"]')
          .focus();
      }, 0);
    }
  });

  const globalSearchForm = document.querySelector(
    'form[data-role="global-search-form"]'
  );
  if (globalSearchForm) {
    new GlobalSearchForm(globalSearchForm);
  }
});
