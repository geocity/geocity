// Delegate change to the document as some file inputs can be dynamically
// inserted into the DOM afterwards
document.addEventListener('change', (e) => {
  const el = e.target;

  if (el.matches('[data-role="custom-file-input"]')) {
    let filesSelection = el
      .closest('[data-role="custom-file-control"]')
      .querySelector('[data-role="custom-file-input-selection"]');

    if (el.files?.length > 1) {
      filesSelection.innerHTML = interpolate(
        gettext(`<strong>%s</strong> fichiers sélectionnés`),
        [el.files.length]
      );
      filesSelection.hidden = false;
    } else if (el.files?.length === 1) {
      filesSelection.innerHTML = `${el.files[0].name}`;
      filesSelection.hidden = false;
    } else {
      filesSelection.hidden = true;
    }
  }
});
