document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('[data-role="show-panel"]').forEach((p) =>
    p.addEventListener('click', () => {
      const panel = document.querySelector(`#${p.dataset.panel}`);
      panel?.show();
    })
  );

  document.querySelectorAll('[data-role="hide-panel"]').forEach((p) =>
    p.addEventListener('click', () => {
      const panel = document.querySelector(`#${p.dataset.panel}`);
      panel?.close();
    })
  );
});
