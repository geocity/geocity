import '@zachleat/seven-minute-tabs';

import './address-autocomplete';
import './dialogs';
import './file-inputs';
import './panels';
import './popovers';
import './search';
import './tables';
import './toaster';
import './maybe-back-button';
import './confirms';
