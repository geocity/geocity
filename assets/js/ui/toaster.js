const toaster = {
  init() {
    this.el = document.querySelector('#global-toaster');

    this.handleClick = this.handleClick.bind(this);
    this.el.addEventListener('click', this.handleClick);

    const initialAlerts = this.el.querySelectorAll('[role="alert"]');
    initialAlerts.forEach((a) => {
      this.prepareToHide(a);
    });
  },

  notify({ type, message, duration }) {
    const alert = document.createElement('div');
    alert.setAttribute('role', 'alert');
    alert.classList.add('alert', `alert--${type}`);
    alert.innerHTML = message;
    this.el.appendChild(alert);
    this.prepareToHide(alert, duration);
  },

  prepareToHide(alert, delay = 8000) {
    setTimeout(() => {
      this.removeAlert(alert);
    }, delay);
  },

  handleClick(e) {
    const alert = e.target.closest('.alert');
    this.removeAlert(alert);
  },

  removeAlert(alert) {
    if (alert) {
      alert.remove();
    }
  },
};

document.addEventListener('DOMContentLoaded', () => {
  toaster.init();
});

export default toaster;
