document.addEventListener('DOMContentLoaded', () => {
  // If the browser has history, take over the link and just go back;
  // otherwise do nothing, so it simply uses the link href
  document
    .querySelectorAll('[data-role="maybe-back-button"]')
    .forEach((button) => {
      button.addEventListener('click', (e) => {
        if (history.length > 1) {
          e.preventDefault();
          history.back();
        }
      });
    });
});
