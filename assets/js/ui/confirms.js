document.addEventListener('DOMContentLoaded', () => {
  /**
   * Display a confirmation dialog whenever an element with the attribute
   * `data-confirm` is clicked, using the value of the attribute
   * as the modal content.
   *
   * Usage example: data-confirm="Are you sure?"
   *
   * This requires _confirm-dialog.html to be present in the page to work.
   */
  const confirmDialog = document.querySelector('#confirmation-dialog');
  const confirmContent = confirmDialog.querySelector(
    '#confirmation-dialog-content'
  );
  const confirmButton = confirmDialog.querySelector(
    '[data-role="confirmation-confirm"]'
  );

  if (confirmDialog) {
    // Delegate event listener to the document so it works also for elements
    // added dynamically later on
    document.addEventListener('click', handleClick);
  }

  function handleClick(e) {
    const confirmTrigger = e.target.closest('[data-confirm]');

    if (confirmTrigger) {
      // The action was confirmed, let it go through
      if (confirmTrigger.dataset.confirmed) {
        delete confirmTrigger.dataset.confirmed;
        return;
      }

      // Prevent behavior and ask confirmation
      e.preventDefault();
      e.stopPropagation();

      // It’s important to clean-up since we recycle the confirmation dialog
      function removeListeners() {
        confirmButton.removeEventListener('click', onConfirm);
        confirmDialog.removeEventListener('close', onClose);
      }

      function onConfirm() {
        removeListeners();
        confirmTrigger.dataset.confirmed = true;
        confirmTrigger.click();
      }

      function onClose() {
        removeListeners();
      }

      confirmButton.addEventListener('click', onConfirm);
      confirmDialog.addEventListener('close', onClose);

      confirmContent.innerHTML = confirmTrigger.dataset.confirm;
      confirmDialog.showModal();
    }
  }
});
