import SubmissionComplementaryDocuments from '@/components/SubmissionComplementaryDocuments';
import SubmissionAmending from '@/components/SubmissionAmending';
import SubmissionPayments from '@/components/SubmissionPayments';

document.addEventListener('DOMContentLoaded', () => {
  // Remove the default tab to show from the URL so the local storage can take
  // over from now on, if the user refresh the page
  const url = new URL(location.href);
  if (url.searchParams.has('prev_active_form')) {
    url.searchParams.delete('prev_active_form');
    history.replaceState(null, null, url);
  }

  const amending = document.querySelector('#amend');
  if (amending) {
    new SubmissionAmending(amending);
  }

  const complementaryDocuments = document.querySelector(
    '#complementary-documents'
  );
  if (complementaryDocuments) {
    new SubmissionComplementaryDocuments(complementaryDocuments);
  }

  const payments = document.querySelector('#payments');
  if (payments) {
    new SubmissionPayments(payments);
  }
});
