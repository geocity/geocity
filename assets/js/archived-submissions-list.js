const downloadForm = document.getElementById('download-form');
const downloadCheckboxes = Array.from(
  document.querySelectorAll(
    'input[type="checkbox"][data-role="select-submission"]'
  )
);
const downloadButton = downloadForm.querySelector(
  'button[data-role="download-submissions"]'
);

downloadCheckboxes.forEach((c) => {
  c.addEventListener('change', updateButtonState);
});

function updateButtonState() {
  const hasCheckedCheckboxes = downloadCheckboxes.find((a) => a.checked);
  downloadButton.disabled = !hasCheckedCheckboxes;
}

updateButtonState();

downloadForm?.addEventListener('submit', () => {
  const checkedCheckboxes = document.querySelectorAll(
    'input[type="checkbox"][data-role="select-submission"]'
  );
  if (!checkedCheckboxes.length) return;

  for (const id of [...checkedCheckboxes].map((checkbox) => checkbox.value)) {
    let input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', 'to_download');
    input.setAttribute('value', id);
    downloadForm.appendChild(input);
  }
});
