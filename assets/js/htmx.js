import toaster from '@/ui/toaster';

import htmx from 'htmx.org';
// Allow to swap HTMX responses using morphing instead of basic replacement
// to keep focus on text fields with auto-save for example
import 'idiomorph/dist/idiomorph-ext';

htmx.on('htmx:sendError', () => {
  toaster.notify({
    type: 'danger',
    message: gettext(
      'Impossible d’envoyer la requête, vous êtes actuellement hors connexion.'
    ),
  });
});

document.addEventListener('DOMContentLoaded', () => {
  const autosaveNotifier = document.querySelector('#global-autosave-notifier');
  let autosaveNotifierTimeout = null;

  function hideAutosaveNotifier() {
    clearTimeout(autosaveNotifierTimeout);
    autosaveNotifierTimeout = null;
    autosaveNotifier.classList.remove('is-visible');
  }

  htmx.on('htmx:afterRequest', (e) => {
    if (e.detail.failed) {
      toaster.notify({
        type: 'danger',
        message: gettext('Une erreur s’est produite lors de la sauvegarde.'),
      });
    } else {
      if (!autosaveNotifier) return;

      if (autosaveNotifierTimeout) {
        hideAutosaveNotifier();
      }

      setTimeout(() => {
        autosaveNotifier.classList.add('is-visible');
      }, 20);

      autosaveNotifierTimeout = setTimeout(hideAutosaveNotifier, 2000);
    }
  });
});
