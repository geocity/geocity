import PermitDocuments from '@/components/PermitDocuments';

document.addEventListener('DOMContentLoaded', () => {
  const permitDocuments = document.querySelector('#permit-documents');
  if (permitDocuments) {
    new PermitDocuments(permitDocuments);
  }
});
