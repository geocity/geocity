/* global jQuery */

document.addEventListener('DOMContentLoaded', () => {
  const $typeField = jQuery('[name="service_fee_type"]');
  const monetaryAmountField = document.querySelector('#id_monetary_amount');
  const rawData = document.querySelector('#service-fees')?.textContent;

  if (!rawData || !monetaryAmountField) return;

  const data = JSON.parse(rawData);

  $typeField.on('select2:select', () => {
    const serviceFeeType = document.querySelector(
      '#select2-id_service_fee_type-container'
    );

    const item = data.find((i) => i.name === serviceFeeType.title);

    if (!item) return;

    monetaryAmountField.value = item.fix_price;

    if (item.fix_price_editable) {
      monetaryAmountField.readOnly = false;
    } else {
      monetaryAmountField.readOnly = true;
    }
  });
});
