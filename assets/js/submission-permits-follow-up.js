import PersistedDetails from '@/components/PersistedDetails';

document.addEventListener('DOMContentLoaded', () => {
  const checklistContainer = document.querySelector(
    '[data-role="submission-step-checklist"]'
  );

  if (checklistContainer) {
    new PersistedDetails(checklistContainer);
  }
});
