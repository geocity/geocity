const PREFIX_KEY = `GEOCITY_`;

export default {
  set(name, value) {
    return localStorage.setItem(`${PREFIX_KEY}${name}`, JSON.stringify(value));
  },

  get(name) {
    return JSON.parse(localStorage.getItem(`${PREFIX_KEY}${name}`));
  },
};
