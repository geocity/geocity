export function toCapitalizedWords(value) {
  return (
    String(value)
      // Replace all non-alphanumeric chars, dashes and underscores
      // by spaces while keeping latin characters
      .replace(/[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f-_.]/g, ' ')
      // Capitalize words
      .split(' ')
      .map((w) => w.charAt(0).toUpperCase() + w.slice(1))
      // Remove duplicate spaces
      .filter((w) => !!w)
      .join(' ')
  );
}
