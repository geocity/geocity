import toaster from './ui/toaster';

document.addEventListener('DOMContentLoaded', () => {
  const archiveForm = document.getElementById('archive-form');

  if (!archiveForm) return;

  const archiveCheckboxes = Array.from(
    document.querySelectorAll(
      'input[type="checkbox"][data-role="select-submission"]'
    )
  );
  const selectAllCheckbox = archiveCheckboxes[0]
    ?.closest('table')
    .querySelector('[data-role="table-select-all"]');
  const archiveButton = archiveForm.querySelector(
    'button[data-role="archive-submissions"]'
  );

  archiveCheckboxes.forEach((c) => {
    c.addEventListener('change', updateButtonState);
  });

  function updateButtonState() {
    const hasCheckedCheckboxes = archiveCheckboxes.find((a) => a.checked);
    archiveButton.disabled = !hasCheckedCheckboxes;
  }

  updateButtonState();

  archiveForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    if (
      !confirm(
        gettext(
          'Archiver des demandes est une action irréversible, êtes-vous sûr·e de vouloir continuer?'
        )
      )
    ) {
      return;
    }

    const checkedCheckboxes = document.querySelectorAll(
      'input[type="checkbox"][data-role="select-submission"]:checked'
    );
    if (!checkedCheckboxes.length) return;

    // Immediately disable the button to avoid multiple requests
    archiveButton.disabled = true;
    selectAllCheckbox.disabled = true;

    const data = new FormData();

    checkedCheckboxes.forEach((checkbox) => {
      data.append('to_archive[]', checkbox.value);
      checkbox.disabled = true;
    });

    data.append(
      'csrfmiddlewaretoken',
      archiveForm.querySelector('input[name="csrfmiddlewaretoken"]').value
    );
    data.append('action', 'archive-requests');

    fetch(archiveForm.action, {
      method: 'post',
      mode: 'same-origin',
      body: data,
    }).then(async (response) => {
      const data = await response.json();

      toaster.notify({
        message: data.message,
        type: response.ok ? 'success' : 'danger',
      });

      checkedCheckboxes.forEach((checkbox) => {
        if (response.ok) {
          checkbox.closest('tr')?.remove();
        } else {
          checkbox.disabled = false;
        }
      });

      selectAllCheckbox.disabled = false;
      selectAllCheckbox.checked = false;

      updateButtonState();
    });
  });
});
