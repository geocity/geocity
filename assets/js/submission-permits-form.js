const handleDependentFields = (element) => {
  let dependsOn = element.dataset.dependsOn;
  let expectedValues = element.dataset.expectedValues.split(',');

  let dependsOnElement = document.querySelector(
    `input[name="${dependsOn}"][type="radio"]:checked,
          input[name="${dependsOn}"][type="checkbox"],
          select[name="${dependsOn}"]`
  );

  if (!dependsOnElement) return;

  const elementToToggle =
    element.closest('fieldset.field-group') ??
    element.closest('.field-group') ??
    element;

  const visible =
    (dependsOnElement.type === 'checkbox' &&
      expectedValues.includes(String(dependsOnElement.checked))) ||
    expectedValues.includes(dependsOnElement.value);

  elementToToggle.classList.toggle('hidden', !visible);
};

document.addEventListener('DOMContentLoaded', () => {
  const conditionalElements = document.querySelectorAll('[data-depends-on]');

  document
    .querySelector('#permit-submission-form')
    .addEventListener('input', () => {
      conditionalElements.forEach((element) => {
        handleDependentFields(element);
      });
    });
});
