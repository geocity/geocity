import SubmissionFormContacts from '@/components/SubmissionFormContacts';

document.addEventListener('DOMContentLoaded', () => {
  new SubmissionFormContacts(document.querySelector('#forms-container'));
});
