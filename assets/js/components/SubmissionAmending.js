const STATUS_AWAITING_SUPPLEMENT = 4;
const STATUS_RECEIVED = 7;

export default class SubmissionAmending {
  constructor(el) {
    this.el = el;

    this.notifyCheckbox = this.el.querySelector('#id_notify_author');
    this.statusField = this.el.querySelector('#id_status');
    this.reasonFieldGroup = this.el
      .querySelector('#id_reason')
      .closest('.field-group');

    this.updateNotify = this.updateNotify.bind(this);
    this.updateReason = this.updateReason.bind(this);

    this.statusField.addEventListener('change', this.updateNotify);
    this.notifyCheckbox.addEventListener('change', this.updateReason);

    this.updateNotify();
  }

  updateNotify() {
    const value = parseInt(this.statusField.value);

    this.notifyCheckbox.checked = false;
    this.notifyCheckbox.closest('.field-group').classList.remove('hidden');

    if (value === STATUS_AWAITING_SUPPLEMENT) {
      this.notifyCheckbox.checked = true;
    } else if (value === STATUS_RECEIVED) {
      this.notifyCheckbox.closest('.field-group').classList.add('hidden');
    }

    this.updateReason();
  }

  updateReason() {
    const display = this.notifyCheckbox.checked;
    this.reasonFieldGroup.classList[display ? 'remove' : 'add']('hidden');
  }
}
