export default class AdvancedGeometryWidget {
  constructor(fieldId) {
    this.dialog = document.getElementById(`map-custom-modal-${fieldId}`);
    this.readOnlyMap = document.getElementById(`map-ro-result-${fieldId}`);
    this.editableMap = document.getElementById(
      `web-component-advanced-${fieldId}`
    );
    this.serializedField = document.querySelector(
      `[data-role='serialized-${fieldId}']`
    );
    this.validateSelectionButton = document.getElementById(
      `modal-validation-button-${fieldId}`
    );
    this.geometryWidgetEl = document.querySelector(
      `#geometry-widget-${fieldId}:not([data-initialize='0'])`
    );

    this.mapIsCreated = false;
    this.selectedValues = '';
    this.options = undefined;

    if (this.geometryWidgetEl?.dataset.options) {
      this.options = JSON.parse(this.geometryWidgetEl.dataset.options);
    }

    this.setupEditableMap = this.setupEditableMap.bind(this);
    this.onModalClose = this.onModalClose.bind(this);

    /**
     * In read-only mode some elements might not be available
     */

    this.dialog?.addEventListener('open', this.setupEditableMap);
    this.dialog?.addEventListener('close', this.onModalClose);

    this.validateSelectionButton?.addEventListener('click', () => {
      if (this.selectedValues != '') {
        this.serializedField.value = this.selectedValues;
        this.closeModal();
        this.updateReadOnlyMap();
      }
    });

    window.addEventListener('position-selected', (event) => {
      if (event.detail) {
        this.selectedValues = event.detail;
      } else {
        this.selectedValues = '';
      }
    });

    if (this.serializedField.value) {
      this.setupReadOnlyMap();
    }
  }

  parseCoordinates(data) {
    const selections = [];
    if (
      this.options.map_widget_configuration[0].outputFormat ==
      'GeometryCollection'
    ) {
      data.geometries.forEach((geometry) => {
        selections.push(geometry.coordinates[0]);
      });
    } else {
      data.features.forEach((feature) => {
        selections.push(feature.geometry.coordinates[0]);
      });
    }
    return selections;
  }

  setupMap(container, readonly) {
    if (this.options?.map_widget_configuration) {
      const wc = document.createElement('openlayers-element');
      wc.options = this.options.map_widget_configuration[0];

      const states = {
        readonly: readonly,
      };

      if (this.serializedField.value) {
        const data = JSON.parse(this.serializedField.value);
        const selections = this.parseCoordinates(data);
        states['currentSelections'] = selections;
      }

      wc.states = states;
      container.appendChild(wc);
    }
  }

  setupReadOnlyMap() {
    this.readOnlyMap.classList.remove('hidden');
    this.setupMap(this.readOnlyMap, true);
  }

  updateReadOnlyMap() {
    // update map only if exists. If there is no value when the form is set.
    // The read-only map is not instanciated but created when data is set.
    if (
      this.readOnlyMap &&
      this.readOnlyMap.lastChild &&
      this.serializedField.value
    ) {
      const data = JSON.parse(this.serializedField.value);
      const selections = this.parseCoordinates(data);
      this.readOnlyMap.lastChild.states = {
        readonly: true,
        currentSelections: selections,
      };
    } else if (this.serializedField.value) {
      this.setupReadOnlyMap();
    }
  }

  setupEditableMap() {
    if (!this.mapIsCreated) {
      if (this.editableMap) {
        this.setupMap(this.editableMap, false);
        this.mapIsCreated = true;
      }
    } else if (this.editableMap.lastChild) {
      if (this.serializedField.value) {
        const data = JSON.parse(this.serializedField.value);
        const selections = this.parseCoordinates(data);
        this.editableMap.lastChild.states = {
          readonly: false,
          currentSelections: selections,
        };
      }
    }
  }

  closeModal() {
    this.dialog.close();
  }

  onModalClose() {
    this.editableMap.lastChild.states = {
      currentSelections: [],
      readonly: true,
    };
  }
}
