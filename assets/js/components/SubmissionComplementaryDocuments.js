export default class SubmissionComplementaryDocuments {
  constructor(el) {
    this.listEl = el.querySelector(
      '[data-role="complementary-documents-list"]'
    );
    this.allDocuments = this.listEl.querySelectorAll(
      '[data-role="complementary-document"]'
    );
    this.submitButton = el.querySelector('button[type="submit"]');
    this.firstDocument = this.allDocuments[0];
    this.nbDocuments = this.allDocuments.length;
    this.totalField = el.querySelector('#id_form-TOTAL_FORMS');

    if (this.nbDocuments == 0) {
      return;
    }

    this.hideChildrenDocumentTypes(el);

    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.addDocument = this.addDocument.bind(this);
    this.removeDocument = this.removeDocument.bind(this);

    this.setEventOnTemplateSelect(0);
    this.allDocuments.forEach((doc) => {
      this.displayFieldsForSource(doc);
      this.displayChildSelect(doc);
    });

    el.querySelector('[data-role="add-document"]').addEventListener(
      'click',
      this.addDocument
    );

    el.addEventListener('click', this.handleClick);
    el.addEventListener('change', this.handleChange);

    // Submission can take a while, especially when generating documents
    // So let’s disable the button to prevent the user from submitting again
    el.addEventListener('submit', () => {
      // Wait for the submit to occur before disabling it, otherwise the
      // button value (`save_continue`) isn’t included in the request
      setTimeout(() => {
        this.submitButton.setAttribute('disabled', true);
      }, 1);
    });
  }

  handleClick(e) {
    if (e.target.closest('[data-role="remove-document"]')) {
      this.removeDocument(
        e.target.closest('[data-role="complementary-document"]')
      );
    }
  }

  handleChange(e) {
    if (e.target.matches('[name$="document_source"]')) {
      const parentForm = e.target.closest(
        '[data-role="complementary-document"]'
      );

      this.displayFieldsForSource(parentForm);
    }
  }

  addDocument(e) {
    e.preventDefault();

    ++this.nbDocuments;

    // regex to find all instances of the form number
    const regex = RegExp(`form-(\\d){1}`, 'g');
    const index = this.nbDocuments - 1;

    // clone the form
    let clone = this.firstDocument.cloneNode(true);
    clone.id = `document-${index}`;
    clone.innerHTML = clone.innerHTML.replace(regex, `form-${index}`);
    clone.querySelector('[data-role="document-number"]').innerHTML = index + 1;
    clone
      .querySelector('[data-role="remove-document"]')
      .classList.remove('hidden');
    clone.querySelector(
      'input[type="file"], input[type="text"], select, textarea'
    ).value = '';
    clone.querySelector('.file-input__selection').setAttribute('hidden', true);
    clone.querySelectorAll('.errorlist').forEach((el) => el.remove());
    clone
      .querySelectorAll('.field-group--has-errors')
      .forEach((el) => el.classList.remove('field-group--has-errors'));

    this.totalField.value = this.nbDocuments;

    this.hideChildrenDocumentTypes(clone);
    this.listEl.appendChild(clone);
    this.displayChildSelect(clone);
    this.setEventOnTemplateSelect(index);
  }

  removeDocument(doc) {
    --this.nbDocuments;
    this.totalField.value = this.nbDocuments;
    doc.remove();
  }

  displayFieldsForSource(doc) {
    const selectedSourceRadio = Array.from(
      doc.querySelectorAll('[name$="document_source"]')
    ).find((r) => r.checked);

    // Persist the attribute in the DOM so it’s reused when cloning
    selectedSourceRadio.setAttribute('checked', true);

    const source = selectedSourceRadio.value;

    doc
      .querySelectorAll(`[data-complementary-document-fields]`)
      .forEach((el) => el.classList.add('hidden'));
    doc
      .querySelector(`[data-complementary-document-fields="${source}"]`)
      .classList.remove('hidden');
  }

  displayChildSelect(parent) {
    const regex = RegExp(`form-(\\d){1}`, 'g');
    const prefix = parent.innerHTML.match(regex)[0];

    const documentType = document.getElementById(`id_${prefix}-document_type`);
    documentType.addEventListener('change', (e) => {
      for (const child of parent.querySelectorAll('.child-type')) {
        child.setAttribute('hidden', '');
        child.closest('.field-group').classList.add('hidden');
      }

      const targetChild = document.getElementById(
        `id_${prefix}-parent_${e.target.value}`
      );

      if (targetChild) {
        targetChild.removeAttribute('hidden');
        targetChild.closest('.field-group').classList.remove('hidden');
      }
    });

    if (documentType.value) {
      documentType.dispatchEvent(new Event('change'));
    }
  }

  hideChildrenDocumentTypes(parent) {
    for (const child of parent.querySelectorAll('.child-type')) {
      child.closest('.field-group').classList.add('hidden');
    }
  }

  setEventOnTemplateSelect(id) {
    let reportInput = document.getElementById(
      `id_form-${id}-document_template`
    );
    let childDocTypeInputs = document.querySelectorAll(
      `[id^="id_form-${id}-parent_"]`
    );
    let previewBtn = document.getElementById(`id_form-${id}-print_preview`);
    let docxBtn = document.getElementById(`id_form-${id}-print_docx`);

    previewBtn.disabled = true;
    docxBtn.disabled = true;

    reportInput.addEventListener('change', (e) => {
      if (e.target.value == '') {
        previewBtn.disabled = true;
        docxBtn.disabled = true;

        for (let childDocTypeInput of childDocTypeInputs) {
          childDocTypeInput.disabled = false;
        }
      } else {
        previewBtn.disabled = false;
        docxBtn.disabled = false;

        for (let childDocTypeInput of childDocTypeInputs) {
          childDocTypeInput.disabled = true;
        }

        previewBtn.onclick = () => {
          let [form_pk, report_pk, doc_type_pk, transaction_pk] =
            reportInput.value.split('/');

          let url = previewBtn.dataset.linkTpl
            .replace('888888888', form_pk)
            .replace('999999999', report_pk)
            .replace('000000000', transaction_pk);
          window.open(url, '_blank');

          return false;
        };

        docxBtn.onclick = () => {
          let [form_pk, report_pk, doc_type_pk, transaction_pk] =
            reportInput.value.split('/');

          let url = docxBtn.dataset.linkTpl
            .replace('888888888', form_pk)
            .replace('999999999', report_pk)
            .replace('000000000', transaction_pk);
          window.open(url, '_blank');

          return false;
        };
      }
    });
  }
}
