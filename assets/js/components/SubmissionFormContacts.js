export default class SubmissionFormContacts {
  constructor(el) {
    this.userProfile = JSON.parse(el.dataset.userProfile);

    this.showNextExtraForm = this.showNextExtraForm.bind(this);
    this.fillFormWithUserProfile = this.fillFormWithUserProfile.bind(this);

    const button = document.querySelector('[data-role="show-extra-form"]');
    if (button) {
      button.addEventListener('click', this.showNextExtraForm);
    }

    el.querySelectorAll('[data-role="fill-with-user-profile"]').forEach((c) => {
      c.addEventListener('change', this.fillFormWithUserProfile);
    });
  }

  showNextExtraForm(e) {
    const button = e.currentTarget;
    const hiddenExtraForms = document.querySelector(
      '[data-role="contact-form"][hidden]'
    );

    if (!hiddenExtraForms) {
      document.getElementById('contact-alert').classList.remove('hidden');
      button.closest('.form-actions').classList.add('hidden');
      return;
    }

    hiddenExtraForms.removeAttribute('hidden');
  }

  fillFormWithUserProfile(e) {
    const checkbox = e.currentTarget;
    const form = checkbox.closest('[data-role="contact-form"]');

    if (checkbox.checked) {
      for (const [key, value] of Object.entries(this.userProfile)) {
        let element = form.querySelector(`[name$="${key}"]`);
        if (element) {
          element.value = value;
          // Pseudo disable the dropdown with CSS instead of setting it read-only
          if (key === 'country') {
            element.classList.add('is-readonly');
          } else {
            element.readOnly = true;
          }
        }
      }
    } else {
      for (const [key] of Object.entries(this.userProfile)) {
        let element = form.querySelector(`[name$="${key}"]`);
        if (element) {
          element.value = '';
          element.readOnly = false;
          if (key === 'country') {
            element.classList.remove('is-readonly');
          } else {
            element.readOnly = false;
          }
        }
      }
    }
  }
}
