import { toCapitalizedWords } from '@/helpers/string';
import toaster from '@/ui/toaster';

export default class PermitDocuments {
  constructor(el) {
    this.fieldsEl = el.querySelector('[data-role="permit-documents-fields"]');
    this.listEl = el.querySelector('[data-role="permit-documents-list"]');
    const allDocuments = this.listEl.querySelectorAll(
      '[data-role="permit-document"]'
    );
    this.submitButton = el.querySelector('button[type="submit"]');
    this.firstDocument = allDocuments[0];
    this.nbDocuments = allDocuments.length;
    this.totalField = el.querySelector('#id_form-TOTAL_FORMS');

    if (this.nbDocuments == 0) {
      return;
    }

    this.handleClick = this.handleClick.bind(this);
    this.addDocument = this.addDocument.bind(this);
    this.removeDocument = this.removeDocument.bind(this);
    this.dispatchMultipleFiles = this.dispatchMultipleFiles.bind(this);

    el.addEventListener('click', this.handleClick);
    el.addEventListener('change', this.handleChange);

    el.addEventListener('submit', () => {
      setTimeout(() => {
        this.submitButton.setAttribute('disabled', true);
      }, 1);
    });

    this.multipleUploader = document.querySelector(
      '#multiple-permit-documents-uploader'
    );

    this.multipleUploader.addEventListener('dragenter', (e) => {
      e.preventDefault();
      e.target.classList.add('is-highlighted');
    });

    this.multipleUploader.addEventListener('dragover', (e) => {
      e.preventDefault();
    });

    this.multipleUploader.addEventListener('dragleave', (e) => {
      e.preventDefault();
      e.target.classList.remove('is-highlighted');
    });

    this.multipleUploader.addEventListener('drop', (e) => {
      e.preventDefault();
      e.target.classList.remove('is-highlighted');
      this.dispatchMultipleFiles(e.dataTransfer.files);
    });

    this.multipleUploader
      .querySelector('input[type="file"]')
      .addEventListener('change', (e) => {
        e.preventDefault();
        this.dispatchMultipleFiles(e.target.files);
      });
  }

  handleClick(e) {
    if (e.target.closest('[data-role="remove-document"]')) {
      this.removeDocument(e.target.closest('[data-role="permit-document"]'));
    }
  }

  handleChange(e) {
    const el = e.target;

    if (el.matches('[name$="document"]')) {
      // When the selected file changes, fill-in the description with a humanized
      // version of the file name
      const parentForm = e.target.closest('[data-role="permit-document"]');
      const description = parentForm.querySelector('[name$="description"]');
      let filename = el.files[0]?.name;

      if (description && filename) {
        description.value = toCapitalizedWords(
          filename.slice(0, filename.lastIndexOf('.'))
        );
      }
    }
  }

  addDocument(e) {
    e?.preventDefault();

    ++this.nbDocuments;

    // regex to find all instances of the form number
    const regex = RegExp(`form-(\\d){1}`, 'g');
    const index = this.nbDocuments - 1;

    // clone the form
    let clone = this.firstDocument.cloneNode(true);
    clone.id = `document-${index}`;
    clone.innerHTML = clone.innerHTML.replace(regex, `form-${index}`);
    clone.querySelector('[data-role="document-number"]').innerHTML = index + 1;
    clone
      .querySelector('[data-role="remove-document"]')
      .classList.remove('hidden');
    clone.querySelector('.file-input__selection').setAttribute('hidden', true);
    clone.querySelectorAll('.errorlist').forEach((el) => el.remove());
    clone
      .querySelectorAll('.field-group--has-errors')
      .forEach((el) => el.classList.remove('field-group--has-errors'));

    this.totalField.value = this.nbDocuments;

    this.listEl.appendChild(clone);
  }

  removeDocument(doc) {
    --this.nbDocuments;
    this.totalField.value = this.nbDocuments;
    doc.remove();
  }

  getDocumentsEmptyFileInputs() {
    return Array.from(
      this.listEl.querySelectorAll('input[type="file"][name$="-document"]')
    ).filter((input) => {
      return input.files.length === 0;
    });
  }

  dispatchMultipleFiles(files) {
    if (!files.length) return;

    const nbOfDocumentsToAdd =
      files.length - this.getDocumentsEmptyFileInputs().length;

    for (let i = 0; i < nbOfDocumentsToAdd; i++) {
      this.addDocument();
    }

    const emptyFileInputs = this.getDocumentsEmptyFileInputs();

    Array.from(files).forEach((file, i) => {
      const fileInput = emptyFileInputs[i];
      const dataTransfer = new DataTransfer();
      dataTransfer.items.add(file);
      fileInput.files = dataTransfer.files;
      fileInput.dispatchEvent(new Event('change', { bubbles: true }));
    });

    this.fieldsEl.classList.remove('hidden');

    toaster.notify({
      type: 'info',
      message: gettext(
        'Les fichiers ont été répartis dans la liste ci-dessous. Veuillez vérifier les données puis enregistrer pour attacher les documents.'
      ),
    });
  }
}
