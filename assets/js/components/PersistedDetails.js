import storage from '@/helpers/storage';

export default class PersistedDetails {
  constructor(el) {
    this.el = el;

    if (!this.el?.id) {
      throw new Error(
        'PersistedDetails requires the element to have a unique ID'
      );
    }

    const storedValue = storage.get(this.el.id);

    if (storedValue !== null && storedValue !== this.el.open) {
      this.el.open = storedValue;
    }

    this.handleToggle = this.handleToggle.bind(this);

    this.el.addEventListener('toggle', this.handleToggle);
  }

  handleToggle(event) {
    storage.set(this.el.id, event.newState === 'open');
  }
}
