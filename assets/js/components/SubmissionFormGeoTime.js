export default class SubmissionFormGeoTime {
  constructor(el) {
    this.formsContainerNode = el.querySelector('[data-geo-time-role="forms"]');
    this.emptyFormNode = el.querySelector('[data-geo-time-role="emptyForm"]');
    this.addButtonNode = el.querySelector('[data-geo-time-role="addForm"]');
    this.totalFormsInputNode = el.querySelector(
      '[data-geo-time-role="managementForm"] input[name$="-TOTAL_FORMS"]'
    );
    this.formNumber = el.querySelector('[data-geo-time-role="formNumber"]');

    if (this.formNumber.textContent == 1) {
      this.formNumber.classList.add('hidden');
    }

    if (!this.emptyFormNode) {
      throw 'No empty form node. Make sure there’s a node with `data-geo-time-role="emptyForm"`.';
    }

    if (!this.totalFormsInputNode) {
      throw 'No total forms input node. Make sure there’s a node with `data-geo-time-role="managementForm"` which contains the management form.';
    }

    this.addEventListeners();

    // If you add some forms and then refresh the page, the browser restores
    // the inputs values but not the dynamic data (the added forms), which
    // means the TOTAL_FORMS input will be out of sync
    this.fixTotalForms();
  }

  addForm() {
    let newNode = this.emptyFormNode.children[0].cloneNode(true);
    let newNodeId = parseInt(this.totalFormsInputNode.value);
    newNode.innerHTML = newNode.innerHTML.replace(/__prefix__/g, newNodeId);
    let geometryWidgetNode = newNode.querySelector('[data-geometry-widget]');

    // Flag the widget to be initialized by the geometry widget manager
    if (geometryWidgetNode !== null) {
      geometryWidgetNode.dataset.initialize = '1';
    }
    // Set the correct form number to appear in the form title
    newNode.querySelector('[data-geo-time-role="formNumber"]').textContent =
      newNodeId + 1;

    this.copyFormData(
      [
        ...this.formsContainerNode.querySelectorAll(
          "[data-geo-time-role='form']"
        ),
      ].pop(),
      newNode,
      newNodeId
    );

    this.formsContainerNode.appendChild(newNode);

    // Keep only the freshly added collapse open
    const collapses = this.formsContainerNode.querySelectorAll(
      '[data-role="geo-time-collapse"]'
    );
    collapses.forEach((node, i) => {
      node.open = i === collapses.length - 1;
    });

    // Initialize `geometryWidget` on the newly created element
    if (window.geometryWidgetManager) {
      window.geometryWidgetManager.rebind();
    }

    this.totalFormsInputNode.value++;
  }

  addEventListeners() {
    if (this.addButtonNode) {
      this.addButtonNode.addEventListener('click', this.addForm.bind(this));
    }
  }

  fixTotalForms() {
    let nbForms = this.formsContainerNode.dataset.nbForms;

    if (nbForms !== undefined) {
      this.totalFormsInputNode.value = nbForms;
    }
  }

  copyFormData(from, to, newFormId) {
    let fromInputs = [...from.querySelectorAll('input, textarea')];

    for (let input of fromInputs) {
      // Skip invalid inputs
      if (!input.name) {
        continue;
      }

      // Formset inputs have names like `prefix-X-fieldName`
      let newName = input.name.replace(/-(\d+)-/, `-${newFormId}-`);
      let inputNode = to.querySelector("[name='" + newName + "']");

      if (inputNode) {
        inputNode.value = input.value;
      }
    }
  }
}
