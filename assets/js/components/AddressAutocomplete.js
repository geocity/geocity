import { debounce } from '../helpers/debounce';

export default class AddressAutocomplete {
  constructor(input) {
    this.input = input;
    this.options = JSON.parse(this.input.dataset.addressAutocomplete);
    this.searchSuffix = this.input.dataset.searchSuffix;
    this.dataList = document.querySelector(
      `datalist#${this.input.getAttribute('list')}`
    );

    this.suggestions = new Map();
    this.lastQuery = this.input.value.trim();

    this.search = this.search.bind(this);
    this.debouncedSearch = debounce(this.search, 200);

    this.input.addEventListener('input', () => {
      let query = this.input.value.trim();

      if (this.suggestions.has(query)) {
        this.fetchDetails(this.suggestions.get(query));
      } else if (query.length > 2 && query !== this.lastQuery) {
        this.debouncedSearch(query);
      }
    });
  }

  search(query) {
    if (query.length < 2) return;

    this.lastQuery = query;

    let searchText = this.input.value;
    if (this.searchSuffix) {
      searchText += ` ${this.searchSuffix}`;
    }

    const params = new URLSearchParams({
      searchText: searchText,
      limit: 10,
      partitionlimit: 24,
      type: 'locations',
      sr: 2056,
      lang: 'fr',
      origins: this.options.origins,
    });

    fetch(`${this.options.apiurl}?${params}`)
      .then((data) => data.json())
      .then(({ results }) => {
        this.suggestions.clear();
        const options = [];

        results.forEach((r) => {
          const value = r.attrs.label
            .replace(/\s*<b>/, ', ')
            .replace('</b>', '');
          options.push(new Option(value));
          this.suggestions.set(value, r);
        });

        this.dataList.replaceChildren(...options);
      });
  }

  fetchDetails(result) {
    fetch(
      `${this.options.apiurlDetail}${result.attrs.featureId}?returnGeometry=false`
    )
      .then((data) => data.json())
      .then(({ feature }) => {
        const nmr = feature.attributes.deinr;

        let street = feature.attributes.strname.join(' ');
        if (typeof nmr == 'string' && nmr !== '') {
          street += ` ${nmr}`;
        }

        const formPrefix = this.input.id.substring(0, 9);

        if (this.options.singleAddressField) {
          this.input.value = `${street}, ${feature.attributes.dplz4} ${feature.attributes.dplzname}`;
          return;
        }

        this.input.value = street;

        if (this.options.zipcodeField != '' && !this.options.singleContact) {
          document.querySelector(
            `#${formPrefix}-${this.options.zipcodeField}`
          ).value = feature.attributes.dplz4;
        } else {
          document.querySelector(`#id_${this.options.zipcodeField}`).value =
            feature.attributes.dplz4;
        }

        if (this.options.cityField != '' && !this.options.singleContact) {
          document.querySelector(
            `#${formPrefix}-${this.options.cityField}`
          ).value = feature.attributes.dplzname;
        } else {
          document.querySelector(`#id_${this.options.cityField}`).value =
            feature.attributes.dplzname;
        }
      });
  }
}
