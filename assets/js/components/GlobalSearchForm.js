export default class GlobalSearchForm {
  constructor(formElem) {
    this.searchFormElem = formElem;
    this.resultsElem = formElem.querySelector('[data-role="search-results"]');
    this.searchInputElem = formElem.querySelector('[data-role="search-input"]');
    this.resultsListElem = this._createResultsListElem();
    this.debounceTimer = null;
    this.highlightedResultIndex = null;
    this.results = [];

    this._addEventListeners();
  }

  hideSuggestions() {
    this.resultsElem.classList.add('hidden');
  }

  showSuggestions() {
    this.resultsElem.classList.remove('hidden');
  }

  _createResultsListElem() {
    const resultsListElem = document.createElement('ul');
    resultsListElem.setAttribute(
      'class',
      'grid divide-y divide-grey-300 max-h-72 overflow-y-auto'
    );
    return resultsListElem;
  }

  _addEventListeners() {
    this.searchFormElem.addEventListener('submit', (e) => {
      e.preventDefault();
    });

    this._addSearchEventListeners();
    this._addKeyUpEventListener();
    this._addClickOutsideEventListener();

    // Show suggestions after page refresh if search input is populated
    if (this.searchInputElem.value !== '') {
      this._search(this.searchInputElem.value);
      this.searchInputElem.focus();

      // Position the caret at the end of the input
      this.searchInputElem.setSelectionRange(
        this.searchInputElem.value.length,
        this.searchInputElem.value.length
      );
    }

    // Show suggestions after closing the suggestions and then clicking again
    // in the search input
    this.searchInputElem.addEventListener('focus', () => {
      if (this.results.length > 0) {
        this.showSuggestions();
      }
    });
  }

  _addClickOutsideEventListener() {
    // We can’t just use a blur event on the search input to hide it because
    // it would hide it when trying to click on a result
    document.addEventListener('click', (e) => {
      let parent = e.target;
      let clickInsideSearchForm = false;

      while (parent) {
        if (parent == this.searchFormElem) {
          clickInsideSearchForm = true;
          break;
        }

        parent = parent.parentElement;
      }

      if (!clickInsideSearchForm) {
        this.hideSuggestions();
      }
    });
  }

  _highlight(index, scrollIntoView) {
    if (scrollIntoView === undefined) {
      scrollIntoView = false;
    }

    if (this.results.length === 0) {
      return;
    }

    // User might have closed the suggestions, show them again
    this.showSuggestions();

    this.resultsListElem.childNodes.forEach((node, i) => {
      if (i !== index && node.classList.contains('is-selected')) {
        node.classList.remove('is-selected');
        node.setAttribute('aria-selected', 'false');
      } else if (i == index && !node.classList.contains('is-selected')) {
        node.classList.add('is-selected');
        node.setAttribute('aria-selected', 'true');
      }
    });

    if (scrollIntoView && this.results.length > index) {
      this.resultsListElem.childNodes[index].scrollIntoView({
        block: 'center',
      });
    }
  }

  _addKeyUpEventListener() {
    this.searchInputElem.addEventListener('keyup', (e) => {
      if (e.key == 'Escape') {
        this.hideSuggestions();
      }

      if (e.key == 'ArrowDown') {
        e.preventDefault();

        this.highlightedResultIndex =
          this.highlightedResultIndex !== null
            ? this._constrainHighlight(this.highlightedResultIndex + 1)
            : 0;

        this._highlight(this.highlightedResultIndex, true);
      }

      if (e.key == 'ArrowUp') {
        e.preventDefault();

        this.highlightedResultIndex =
          this.highlightedResultIndex !== null
            ? this._constrainHighlight(this.highlightedResultIndex - 1)
            : this._constrainHighlight(this.results.length - 1);

        this._highlight(this.highlightedResultIndex, true);
      }

      if (e.key == 'Enter' && this.highlightedResultIndex !== null) {
        e.preventDefault();
        this._openResult(this.searchFormElem.querySelector('.is-selected'));
      }
    });
  }

  _addSearchEventListeners() {
    this.searchInputElem.addEventListener('input', (e) => {
      if (this.debounceTimer !== null) {
        window.clearTimeout(this.debounceTimer);
      }

      if (e.target.value.length > 1) {
        this.debounceTimer = window.setTimeout(
          () => this._search(e.target.value),
          500
        );
      } else {
        this.debounceTimer = null;
        this.results = [];
        this.hideSuggestions();
      }
    });
  }

  _createResultElement(result) {
    return `
      <div class="grid gap-1">
        <div class="font-semibold">${result.match.type == 'property' ? `${result.match.fieldLabel}: ` : ''}${result.match.fieldValue}</div>
        ${result.match.type != 'sent_date' && result.submission.sentDate ? `<div class="text-sm">Demande du ${result.submission.sentDate}</div>` : ''}
        ${result.match.type != 'author' && result.submission.author ? `<div class="text-sm">Auteur‧e: ${result.submission.author}</div>` : ''}
        <div class="text-xs text-grey-600">Trouvé dans: ${result.match.typeLabel}</div>
      </div>
    `;
  }

  _search(value) {
    fetch(
      this.searchFormElem.getAttribute('action') +
        '?' +
        new URLSearchParams({ search: value }).toString(),
      {
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/x-www-urlencoded',
        },
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.results = data.results;
        this._redraw();
        this.showSuggestions();
      });
  }

  _redraw() {
    if (this.results.length === 0) {
      this.resultsElem.innerHTML = `<div class="py-2 px-4 text-grey-600">Aucun résultat pour votre recherche.</div>`;
    } else {
      this.resultsListElem = this._createResultsListElem();
      this.highlightedResultIndex = null;

      this.results.forEach((result, i) => {
        const listElement = document.createElement('li');
        listElement.setAttribute('aria-selected', 'false');
        listElement.classList.add('dropdown__item', 'flex', 'cursor-pointer');
        listElement.dataset.url = result.submission.url;
        listElement.innerHTML = this._createResultElement(result);

        listElement.addEventListener('mouseover', () => {
          this.highlightedResultIndex = i;
          this._highlight(this.highlightedResultIndex);
        });

        listElement.addEventListener('click', () => {
          this._openResult(listElement);
        });

        this.resultsListElem.appendChild(listElement);
      });

      this.resultsElem.childNodes.forEach((node) =>
        this.resultsElem.removeChild(node)
      );
      this.resultsElem.appendChild(this.resultsListElem);

      // Hack: scroll doesn’t happen without setTimeout, probably because the
      // node hasn’t been rendered yet
      window.setTimeout(() => this.resultsListElem.scrollTo(0, 0), 0);
    }
  }

  _openResult(item) {
    window.location = item.dataset.url;
  }

  _constrainHighlight(index) {
    if (index < 0) {
      return Math.max(this.results.length - 1, 0);
    } else if (index >= this.results.length) {
      return 0;
    }

    return index;
  }
}
