export default class SubmissionPayments {
  constructor(el) {
    this.statusChangeBtns = el.querySelectorAll(
      '[data-role="change-payment-status"]'
    );

    this.statusChangeBtns.forEach((btn) => {
      btn.addEventListener('click', (event) => {
        event.preventDefault();
        const merchantRef = event.target.dataset.merchantReference;
        const url = event.target.dataset.href;
        const select = el.querySelector(
          '#payment-status-change-select-' + merchantRef
        );
        const value = select.options[select.selectedIndex].value;
        if (value !== '') {
          window.location.href = url + '?new_status=' + value;
        }
      });
    });
  }
}
