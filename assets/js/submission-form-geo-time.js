import SubmissionFormGeoTime from '@/components/SubmissionFormGeoTime';

document.addEventListener('DOMContentLoaded', () => {
  new SubmissionFormGeoTime(document.querySelector('#permit-request-geo-time'));
});
