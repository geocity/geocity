Icons from Lucide library, released under the ISC License.
https://lucide.dev/

To regenerate the icons sprite, run `npm run sprite` in the `frontend` container:

```bash
docker compose run frontend npm run sprite
```
