from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("forms", "0040_alter_field_integrator_alter_form_integrator_and_more"),
        ("forms", "0040_alter_form_default_values"),
    ]

    operations = []
