(function() {
  // Show the tab corresponding to the URL hash, handling special characters by decoding the hash
  function forceTabDisplay() {
    const currentHash = window.location.hash;

    if (currentHash) {
      const decodedHash = decodeURIComponent(currentHash);
      const targetTab = document.querySelector("#jazzy-tabs .nav-link[href=\"" + decodedHash + "\"]");

      if (targetTab) {
        const tab = new bootstrap.Tab(targetTab);
        tab.show();
      }
    }
  }

  window.addEventListener("load", forceTabDisplay);
})();
