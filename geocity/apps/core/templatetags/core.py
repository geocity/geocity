import os

from django import template
from django.urls import reverse

register = template.Library()


MENU_CURRENT_CLASS = "is-current"


@register.simple_tag(takes_context=True)
def highlight_menu_class(context, *args, **kwargs):
    """
    Return the highlighting class if the params matches the current route(s) or app(s)
    Accept:
    - 1 arg / N kwargs. Example:
      {% highlight_menu_class 'questions:client:category' category_id=category.id %}
    - N args / 0 kwargs. Examples:
      {% highlight_menu_class 'topics:topics' 'topics:topic' %}
      {% highlight_menu_class 'questions' %}
    """
    css_class = f" {MENU_CURRENT_CLASS}"

    try:
        request = context["request"]
    except KeyError:
        return ""

    # If kwargs are passed, try to match the current route to the URL with kwargs (eg.
    # /categories/2/), otherwise just check the route name
    if kwargs:
        if request.path == reverse(*args, kwargs=kwargs):
            return css_class

    # Try to match the value against the view name
    try:
        view_name = request.resolver_match.view_name
    except AttributeError:
        pass
    else:
        if view_name in args:
            return css_class

    # Try to match the value against the whole app
    try:
        app_name = request.resolver_match.app_names[0]
    except AttributeError:
        pass
    except IndexError:
        pass
    else:
        if app_name in args:
            return css_class

    return ""


@register.filter
def filename(value):
    return os.path.basename(value.file.name)
