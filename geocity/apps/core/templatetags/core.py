import os

from django import template
from django.urls import reverse

register = template.Library()


MENU_CURRENT_CLASS = "is-current"


@register.simple_tag(takes_context=True)
def menu_class_for_url(context, *args, **kwargs):
    """
    Return the current url name formatted for CSS class
    Accept:
    - 1 arg / N kwargs. Example:
      {% menu_class_for_url 'questions:client:category' category_id=category.id %}
    - N args / 0 kwargs. Examples:
      {% menu_class_for_url 'topics:topics' 'topics:topic' %}
      {% menu_class_for_url 'questions:client:category' %}
    """
    css_class = ""
    try:
        request = context["request"]
    except KeyError:
        return ""

    # If kwargs are passed, try to match the current route to the URL with kwargs (eg.
    # /categories/2/), otherwise just check the route name
    if kwargs:
        if request.path == reverse(*args, kwargs=kwargs):
            css_class = f" {MENU_CURRENT_CLASS}"
    else:
        try:
            view_name = request.resolver_match.view_name
        except AttributeError:
            pass
        else:
            if view_name in args:
                css_class = f" {MENU_CURRENT_CLASS}"

    return css_class


@register.filter
def filename(value):
    return os.path.basename(value.file.name)
