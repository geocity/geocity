from django.contrib.auth.models import Group, Permission
from django.core.management import BaseCommand, CommandError
from django.utils.translation import gettext

from geocity.apps.accounts import permissions_groups


class Command(BaseCommand):

    help = gettext(
        "Update the permissions for Groups that have is_validator = True set in the admin."
    )

    def handle(self, *args, **options):
        self.stdout.write("Adding new permissions ...")

        try:
            validator_groups = Group.objects.filter(
                submission_department__is_validator=True
            )

            for validator_group in validator_groups:
                validator_group.permissions.add(
                    *Permission.objects.filter(
                        codename__in=permissions_groups.DEFAULT_VALIDATOR_PERMISSION_CODENAMES
                    )
                )

            self.stdout.write("Update of validator permissions successful.")
        except CommandError:
            self.stdout.write("ERROR: Error while updating validator permissions!")
