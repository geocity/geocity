# Generated by Django 4.2.16 on 2025-01-09 11:55

import phonenumber_field.modelfields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0030_merge"),
    ]

    operations = [
        migrations.AlterField(
            model_name="administrativeentity",
            name="phone",
            field=phonenumber_field.modelfields.PhoneNumberField(
                blank=True, max_length=128, region=None, verbose_name="Téléphone"
            ),
        ),
        migrations.AlterField(
            model_name="historicaluserprofile",
            name="phone_first",
            field=phonenumber_field.modelfields.PhoneNumberField(
                max_length=128, region=None, verbose_name="Téléphone principal"
            ),
        ),
        migrations.AlterField(
            model_name="historicaluserprofile",
            name="phone_second",
            field=phonenumber_field.modelfields.PhoneNumberField(
                blank=True,
                max_length=128,
                region=None,
                verbose_name="Téléphone secondaire",
            ),
        ),
        migrations.AlterField(
            model_name="userprofile",
            name="phone_first",
            field=phonenumber_field.modelfields.PhoneNumberField(
                max_length=128, region=None, verbose_name="Téléphone principal"
            ),
        ),
        migrations.AlterField(
            model_name="userprofile",
            name="phone_second",
            field=phonenumber_field.modelfields.PhoneNumberField(
                blank=True,
                max_length=128,
                region=None,
                verbose_name="Téléphone secondaire",
            ),
        ),
    ]
