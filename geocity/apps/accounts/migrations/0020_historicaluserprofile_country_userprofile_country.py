# Generated by Django 4.2.11 on 2024-05-29 09:09

import django_countries.fields
from django.db import migrations


def set_country_to_null(apps, schema_editor):
    UserProfile = apps.get_model("accounts", "UserProfile")
    HistoricalUserProfile = apps.get_model("accounts", "HistoricalUserProfile")

    UserProfile.objects.all().update(country=None)
    HistoricalUserProfile.objects.all().update(country=None)


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0019_administrativeentity_agenda_domain"),
    ]

    operations = [
        migrations.AddField(
            model_name="historicaluserprofile",
            name="country",
            field=django_countries.fields.CountryField(
                default="CH", max_length=2, null=True, verbose_name="Pays"
            ),
        ),
        migrations.AddField(
            model_name="userprofile",
            name="country",
            field=django_countries.fields.CountryField(
                default="CH", max_length=2, null=True, verbose_name="Pays"
            ),
        ),
        # Dont add the default value to the existing users
        migrations.RunPython(set_country_to_null),
    ]
