from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0024_proxyformailing"),
        ("accounts", "0029_alter_administrativeentity_integrator_and_more"),
    ]

    operations = []
