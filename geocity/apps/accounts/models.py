from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.gis.db import models as geomodels
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.validators import FileExtensionValidator, RegexValidator
from django.db import models
from django.db.models import BooleanField, Count, ExpressionWrapper, Q, UniqueConstraint
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField
from simple_history.models import HistoricalRecords
from taggit.managers import TaggableManager

from .fields import AdministrativeEntityFileField, CustomLoginImageFileField

AGENDA_PUBLIC_TYPE_CHOICES = (
    (
        True,
        _("Visible"),
    ),
    (
        False,
        _("Masqué"),
    ),
)


BOOLEAN_CHOICES = (
    (
        True,
        _("Oui"),
    ),
    (
        False,
        _("Non"),
    ),
)


# Controls who can fill each Form. Anonymous forms can be filled by anyone
PUBLIC_TYPE_CHOICES = (
    (
        True,
        _("Tout public"),
    ),
    (
        False,
        _("Restreint aux utilisateurs autorisés"),
    ),
)


class SiteProfile(models.Model):
    site = models.OneToOneField(
        Site,
        on_delete=models.CASCADE,
        related_name="site_profile",
    )
    integrator = models.ForeignKey(
        Group,
        null=True,
        default=None,
        on_delete=models.SET_NULL,
        verbose_name=_("Groupe intégrateur"),
        related_name="site_profiles",
        limit_choices_to={"submission_department__is_integrator_admin": True},
    )
    custom_template = models.ForeignKey(
        "TemplateCustomization",
        null=True,
        blank=True,
        default=None,
        on_delete=models.SET_NULL,
        verbose_name=_("Page de login"),
    )

    class Meta:
        verbose_name = _(
            "Paramètres complémentaires (à définir après avoir créé le site)"
        )
        verbose_name_plural = _(
            "Paramètres complémentaires (à définir après avoir créé le site)"
        )


@receiver(post_save, sender=Site)
def save_site_profile(sender, instance, created, **kwargs):
    if created or not SiteProfile.objects.filter(site=instance).exists():
        SiteProfile.objects.create(
            site=instance,
        )


class TemplateCustomization(models.Model):
    templatename = models.CharField(
        _("Identifiant"),
        max_length=64,
        blank=True,
        help_text="Permettant d'afficher la page de login par l'url: https://geocity.ch/?template=vevey",
        validators=[
            RegexValidator(
                regex=r"^[a-zA-Z0-9_]*$",
                message="Seuls les caractères sans accents et les chiffres sont autorisés. Les espaces et autres caractères spéciaux ne sont pas autorisés",
            )
        ],
    )
    application_title = models.CharField(_("Titre"), max_length=255, blank=True)
    application_subtitle = models.CharField(_("Sous-titre"), max_length=255, blank=True)
    application_description = models.TextField(
        _("Description"), max_length=2048, blank=True
    )
    background_image = CustomLoginImageFileField(
        _("Image de fond"),
        blank=True,
        upload_to="site_profile_custom_image/",
        validators=[
            FileExtensionValidator(allowed_extensions=["svg", "png", "jpg", "jpeg"])
        ],
    )

    class Meta:
        constraints = [
            UniqueConstraint(fields=["templatename"], name="unique_template_name")
        ]
        verbose_name = _("4.1 Configuration de la page de login")
        verbose_name_plural = _("4.1 Configuration des pages de login")

    def __str__(self):
        return self.templatename


class SubmissionDepartment(models.Model):
    group = models.OneToOneField(
        Group, on_delete=models.CASCADE, related_name="submission_department"
    )
    description = models.CharField(_("description"), max_length=100, default="Service")
    shortname = models.CharField(
        _("nom court"),
        max_length=100,
        help_text=_(
            "Nom affiché par défaut dans les différentes étapes du formulaire, ne s'affiche pas dans l'admin (max. 100 caractères)"
        ),
        blank=True,
    )
    is_validator = models.BooleanField(
        _("validateur"),
        help_text=_(
            "Cocher si les membres peuvent être appelés à valider des demandes"
        ),
    )
    is_backoffice = models.BooleanField(
        _("coordinateur"),
        default=False,
        help_text=_(
            "Cocher si les membres sont coordinateurs (notification lors de nouvelles demandes, envoi des demandes en validation, classement des demandes, etc.)"
        ),
    )
    administrative_entity = models.ForeignKey(
        "AdministrativeEntity",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="departments",
        verbose_name=_("entité administrative"),
    )
    is_default_validator = models.BooleanField(
        _("sélectionné par défaut pour les validations"), default=False
    )
    integrator = models.IntegerField(
        _("Groupe intégrateur"),
        help_text=_("Identifiant du groupe"),
        default=0,
    )
    is_integrator_admin = models.BooleanField(
        _("Intégrateur"),
        help_text=_("Cocher si les membres peuvent accéder à l'admin de Django"),
        default=False,
    )
    mandatory_2fa = models.BooleanField(
        _("2FA obligatoire"),
        help_text=_(
            "Cocher si les membres doivent obligatoirement utiliser la double authentification"
        ),
        default=False,
    )
    uses_generic_email = models.BooleanField(
        _("Ne notifier qu'une adresse e-mail générique"),
        help_text=_(
            "Cocher si une adresse e-mail générique doit être utilisée au lieu de l'adresse personnelle"
        ),
        default=False,
    )
    generic_email = models.EmailField(
        _("Email générique"),
        help_text=_("Email générique pour tous les membres du groupe"),
        blank=True,
    )
    integrator_email_domains = models.CharField(
        _("Domaines d'emails visibles pour l'intégrateur"),
        help_text=_(
            "Liste de domaines séparés par des virgules ',' correspondant aux utilisateurs rattachés à l'entité administrative (ex: ma-commune.ch,commune.ch)"
        ),
        blank=True,
        max_length=254,
    )
    integrator_emails_exceptions = models.CharField(
        _("Emails complets visibles pour l'intégrateur"),
        help_text=_(
            "Liste d'emails séparés par des virgules ',' d'utilisateurs spécifiques rattachés à l'entité administrative (ex: greffe@nowhere.com)"
        ),
        blank=True,
        max_length=254,
    )

    class Meta:
        verbose_name = _("2.1 Configuration du service (coordinateur, validateur...)")
        verbose_name_plural = _(
            "2.1 Configuration des services (coordinateur, validateur...)"
        )

    def __str__(self):
        return self.shortname if self.shortname else self.group.name


class AdministrativeEntityQuerySet(models.QuerySet):
    def filter_by_tags(self, tags):
        return self.filter(tags__name__in=[tag.lower() for tag in tags])


class AdministrativeEntityManager(models.Manager):
    def get_queryset(self):
        return AdministrativeEntityQuerySet(self.model)

    def public(self):
        return self.get_queryset().filter(anonymous_user__isnull=False)

    def associated_to_user(self, user):
        """
        Get the administrative entities associated to a specific user.
        If the users has entities, he's a trusted user
        """
        return (
            self.get_queryset()
            .filter(
                departments__group__in=user.groups.all(),
            )
            .order_by("group_order", "name")
        )


class AdministrativeEntity(models.Model):
    name = models.CharField(_("name"), max_length=128)
    agenda_domain = models.CharField(
        _("Domaine de l'agenda"),
        help_text=_(
            "Utilisé afin de sélectionner les agendas visible dans agenda-embed"
        ),
        max_length=128,
        blank=True,
    )
    agenda_name = models.CharField(
        _("Nom dans l'api agenda"),
        help_text=_("Nom visible dans le filtre de l'agenda"),
        max_length=128,
        blank=True,
    )
    group_order = models.PositiveIntegerField(
        _("Groupe et ordre"),
        default=0,
        help_text=_(
            "Utilisé afin de grouper et définir l'ordre des entités.<br>Affichés par ordre croissant et groupés en cas de nombre identique"
        ),
    )
    link = models.URLField(_("Lien"), max_length=200, blank=True)
    archive_link = models.URLField(_("Archives externes"), max_length=1024, blank=True)
    general_informations = models.CharField(
        _("Informations"),
        blank=True,
        max_length=1024,
    )
    custom_signature = models.TextField(
        _("Signature des emails"),
        help_text=_("Si vide, le nom de l'entité sera utilisé"),
        max_length=1024,
        blank=True,
    )
    phone = PhoneNumberField(
        _("Téléphone"),
        blank=True,
    )
    integrator = models.ForeignKey(
        Group,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Groupe intégrateur"),
        limit_choices_to={"submission_department__is_integrator_admin": True},
    )
    additional_searchtext_for_address_field = models.CharField(
        _("Filtre additionnel pour la recherche d'adresse"),
        max_length=255,
        blank=True,
        help_text=_(
            'Ex: "Yverdon-les-Bains" afin de limiter les recherches à Yverdon, <a href="https://api3.geo.admin.ch/services/sdiservices.html#search" target="_blank">Plus d\'informations</a>'
        ),
    )
    geom = geomodels.MultiPolygonField(_("geom"), null=True, srid=2056)
    tags = TaggableManager(
        blank=True,
        verbose_name=_("Mots-clés"),
        help_text="Mots clefs sans espaces, séparés par des virgules permettant de filtrer les entités par l'url: https://geocity.ch/?entityfilter=yverdon",
    )
    expeditor_name = models.CharField(
        _("Nom de l'expéditeur des notifications"), max_length=255, blank=True
    )
    expeditor_email = models.CharField(
        _("Adresse e-mail de l'expéditeur des notifications"),
        max_length=255,
        blank=True,
        validators=[
            RegexValidator(
                regex=r"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$",
                message="Le format de l'adresse e-mail n'est pas valable.",
            )
        ],
    )
    reply_to_email = models.CharField(
        _("Adresse e-mail de réponse"),
        max_length=255,
        blank=True,
        validators=[
            RegexValidator(
                regex=r"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,63}$",
                message="Le format de l'adresse e-mail n'est pas valable.",
            )
        ],
        help_text="Permet de définir une adresse e-mail de réponse autre que celle de l'expéditeur des notifications",
    )
    is_single_form_submissions = models.BooleanField(
        _("Autoriser uniquement un objet par demande"),
        default=True,
        help_text=_("Nécessaire pour l'utilisation du système de paiement en ligne"),
    )
    is_permits_enabled = models.BooleanField(
        _("Activer la gestion des permis de construire"),
        default=False,
    )
    sites = models.ManyToManyField(
        Site,
        related_name="administrative_entity",
        verbose_name=_("Détails du Site"),
    )
    directive = AdministrativeEntityFileField(
        _("directive"),
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
        blank=True,
    )
    directive_description = models.CharField(
        _("description de la directive"), max_length=200, blank=True
    )
    additional_information = models.TextField(_("autre information"), blank=True)
    signature_sheet = AdministrativeEntityFileField(
        _("Volet de transmission"),
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
        blank=True,
    )
    signature_sheet_description = models.TextField(
        _("Texte explicatif relatif au volet de transmission"), blank=True
    )
    services_fees_hourly_rate = models.DecimalField(
        decimal_places=2,
        max_digits=12,
        default=settings.DEFAULT_SERVICES_FEES_RATE,
        verbose_name=_("Tarif horaire"),
        help_text=_("Tarif horaire des prestations de l'entité administrative"),
    )

    objects = AdministrativeEntityManager()

    class Meta:
        verbose_name = _(
            "1.1 Configuration de l'entité administrative (commune, organisation)"
        )
        verbose_name_plural = _(
            "1.1 Configuration des entités administratives (commune, organisation)"
        )

    def __str__(self):
        return self.name

    def create_anonymous_user(self):
        try:
            return self.anonymous_user
        except ObjectDoesNotExist:
            username = "%s%s" % (settings.ANONYMOUS_USER_PREFIX, self.pk)
            first_name = "Anonymous user"
            last_name = self.name

            user = User.objects.create(
                username=username,
                first_name=first_name,
                last_name=last_name,
                is_active=False,
            )

            return UserProfile.objects.create(
                administrative_entity=self,
                user_id=user.id,
                zipcode=settings.ANONYMOUS_USER_ZIPCODE,
            )

    def get_user_list(self, users):
        if users:
            list_content = []
            for user in users:
                url = reverse("admin:auth_user_change", kwargs={"object_id": user.id})
                list_content.append(
                    f"<li><a href='{url}'>{user.get_full_name()}</a></li>"
                )
            list_html = "\n".join(list_content)
            return mark_safe(f"<ul>{list_html}</ul>")
        else:
            return f"<ul><li>{_('Aucun utilisateur trouvé')}</li></ul>"

    def get_integrator_users(self):
        integrator_users = User.objects.filter(groups=self.integrator)
        return self.get_user_list(integrator_users)

    def get_pilot_users(self):
        pilot_users = User.objects.filter(
            groups__submission_department__administrative_entity=self.pk,
            groups__submission_department__is_backoffice=True,
        )
        return self.get_user_list(pilot_users)

    def get_validator_users(self):
        validator_users = User.objects.filter(
            groups__submission_department__administrative_entity=self.pk,
            groups__submission_department__is_validator=True,
        )
        return self.get_user_list(validator_users)

    def clean(self):
        from geocity.apps.forms.models import Form
        from geocity.apps.submissions.models import Submission

        if (
            Form.objects.annotate(entities_count=Count("administrative_entities"))
            .filter(
                is_public=True, entities_count__gt=1, administrative_entities=self.pk
            )
            .exists()
        ):
            raise ValidationError(
                _(
                    "Des formulaires partagés avec d'autres entités "
                    "administratives sont encore disponibles."
                )
            )

        if (
            self.is_single_form_submissions
            and Submission.objects.annotate(forms_count=Count("forms"))
            .filter(
                administrative_entity_id=self.pk,
                forms_count__gt=1,
            )
            .exclude(status=Submission.STATUS_ARCHIVED)
            .exists()
        ):
            raise ValidationError(
                {
                    "is_single_form_submissions": _(
                        "Impossible tant que des demandes liées à plusieurs "
                        "formulaires sont encore actives dans cette entité "
                        "administrative."
                    )
                }
            )

        # Unique constraint for agenda_domain
        # Cannot be used on model, because None is also subject to the constraint (blank=True)
        if self.agenda_domain:
            if (
                AdministrativeEntity.objects.exclude(pk=self.pk)
                .filter(agenda_domain=self.agenda_domain)
                .exists()
            ):
                raise ValidationError(
                    {"agenda_domain": _("Le domaine de l'agenda doit être unique.")}
                )

        if (
            not self.is_single_form_submissions
            and Form.objects.filter(
                requires_online_payment=True, administrative_entities=self
            ).exists()
        ):
            raise ValidationError(
                {
                    "is_single_form_submissions": _(
                        "Il existe encore des formulaires soumis au paiement en ligne "
                        "dans cette entité administrative. Avant de permettre les "
                        "demandes à objets multiples, veuillez supprimer ces "
                        "formulaires ou y désactiver le paiement en ligne."
                    )
                }
            )


# Change the app_label in order to regroup models under the same app in admin
class AdministrativeEntityForAdminSite(AdministrativeEntity):
    class Meta:
        proxy = True
        app_label = "forms"
        verbose_name = _("1.1 Entité administrative")
        verbose_name_plural = _("1.1 Entités administratives")


class UserProfileManager(models.Manager):
    def create_temporary_user(self, entity):
        # TODO should this be moved to User instead of UserProfile?
        # TODO why isn’t the `entity` argument used?
        # Multiple temp users might exist at the same time
        last_temp_user = self.get_queryset().filter(is_temporary=True).last()
        if last_temp_user:
            nb = int(last_temp_user.user.username.split("_")[2]) + 1
        else:
            nb = 0

        username = "%s%s" % (settings.TEMPORARY_USER_PREFIX, nb)
        email = "%s@%s" % (username, settings.SITE_DOMAIN)
        zipcode = settings.TEMPORARY_USER_ZIPCODE

        temp_user = User.objects.create_user(
            username, email, password=None, first_name="Temporaire", last_name="Anonyme"
        )

        new_temp_author = super().create(
            user=temp_user,
            zipcode=zipcode,
        )

        return new_temp_author

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .annotate(
                is_temporary=ExpressionWrapper(
                    Q(user__username__startswith=settings.TEMPORARY_USER_PREFIX),
                    output_field=BooleanField(),
                )
            )
        )


class UserProfile(models.Model):
    company_name = models.CharField(_("Raison Sociale"), max_length=100, blank=True)
    vat_number = models.CharField(
        _("Numéro TVA"),
        max_length=19,
        blank=True,
        validators=[
            RegexValidator(
                regex=r"^(CHE-)+\d{3}\.\d{3}\.\d{3}(\sTVA)?$",
                message="Le code d'entreprise doit être de type \
                         CHE-123.456.789 (TVA) \
                         et vous pouvez le trouver sur \
                         le registre fédéral des entreprises \
                         https://www.uid.admin.ch/search.aspx",
            )
        ],
    )
    address = models.CharField(
        _("Rue"),
        max_length=100,
    )
    zipcode = models.PositiveIntegerField(
        _("Code postal"),
    )
    city = models.CharField(
        _("Ville"),
        max_length=100,
    )
    country = CountryField(
        _("Pays"),
        null=True,
        blank=False,
        default="CH",
    )
    phone_first = PhoneNumberField(
        _("Téléphone principal"),
    )
    phone_second = PhoneNumberField(
        _("Téléphone secondaire"),
        blank=True,
    )
    iban = models.CharField(
        _("IBAN"),
        help_text=_(
            "A remplir uniquement pour les prestations liées à un remboursement (le titulaire du compte doit correspondre aux informations indiquées ci-dessus)."
        ),
        blank=True,
        max_length=30,
        validators=[
            RegexValidator(
                regex=r"^[A-Z]{2}[0-9]{2}(?:[ ]?[0-9A-Z]{4}){4}(?:[ ]?[0-9A-Z]{1,2})?$",
                message="L'IBAN doit être de type CH12 3456 7890 1234 5678 9",
            )
        ],
    )
    notify_per_email = models.BooleanField(_("Me notifier par e-mail"), default=True)
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    history = HistoricalRecords()

    administrative_entity = models.OneToOneField(
        AdministrativeEntity,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="anonymous_user",
        verbose_name=_("entité administrative"),
    )

    objects = UserProfileManager()

    def clean(self):
        if self.user and self.user.is_active and self.administrative_entity is not None:
            raise ValidationError(
                _(
                    "Seul·e un·e auteur·e désactivé·e peut être relié directement "
                    "à une entité administrative, et ainsi être considéré·e "
                    "auteur·e anonyme de l'entité."
                )
            )

    @cached_property
    def is_anonymous(self):
        """
        UserProfile unique per AdministrativeEntity.
        Never logged in. Used to save anonymous requests.
        """
        return (
            self.user
            and not self.user.is_active
            and self.administrative_entity is not None
        )

    @cached_property
    def is_temporary(self):
        """
        UserProfile created when starting an anonymous submission,
        then deleted at the submission (replaced by an anonymous user).
        """
        return self.user and self.user.username.startswith(
            settings.TEMPORARY_USER_PREFIX
        )

    class Meta:
        verbose_name = _("3.2 Consultation de l'auteur")
        verbose_name_plural = _("3.2 Consultation des auteurs")

    def __str__(self):
        return (
            str(self.user.first_name) + " " + str(self.user.last_name)
            if self.user
            else str(self.pk)
        )


# Use a proxy on group model to integrate simple view in admin
class GroupMailProxy(Group):
    class Meta:
        proxy = True
        verbose_name = _("Mailing")
        verbose_name_plural = _("Mailing")
        permissions = (("can_list_mails_by_role", "Can see user emails by role"),)
