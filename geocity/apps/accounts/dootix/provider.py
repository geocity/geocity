from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider

from geocity.apps.accounts.dootix.views import PROVIDER_ID, DootixAdapter


class DootixAccount(ProviderAccount):
    pass


class DootixProvider(OAuth2Provider):

    id = PROVIDER_ID
    name = "Dootix"
    account_class = DootixAccount
    oauth2_adapter_class = DootixAdapter

    def extract_uid(self, data):
        return str(data["id"])

    def extract_common_fields(self, data):
        """
        Extract available/matching fields to populate the User instance.
        """
        # User profile
        return dict(
            name=data["name"],
            email=data["email"],
        )

    def get_default_scope(self):
        return []


provider_classes = [DootixProvider]
