# Generated by Django 4.2.16 on 2025-03-07 23:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("reports", "0037_alter_report_integrator_and_more"),
        ("reports", "0037_alter_sectionamendproperty_options_and_more"),
    ]

    operations = []
