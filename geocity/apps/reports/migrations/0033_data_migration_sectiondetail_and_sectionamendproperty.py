from django.db import migrations


class Migration(migrations.Migration):
    def update_sectiondetail(apps, schema_editor):
        """
        Used to replace "," with ";" as delimiter in undesired_properties
        """

        SectionDetail = apps.get_model("reports", "SectionDetail")

        for section in SectionDetail.objects.all():
            if section.undesired_properties:
                section.undesired_properties = section.undesired_properties.replace(
                    ",", ";"
                )
                section.save(update_fields=["undesired_properties"])

    def update_sectionamendproperty(apps, schema_editor):
        """
        Used to replace "," with ";" as delimiter in undesired_properties
        """

        SectionAmendProperty = apps.get_model("reports", "SectionAmendProperty")

        for section in SectionAmendProperty.objects.all():
            if section.undesired_properties:
                section.undesired_properties = section.undesired_properties.replace(
                    ",", ";"
                )
                section.save(update_fields=["undesired_properties"])

    dependencies = [
        (
            "reports",
            "0032_data_migration_sectionparagraph",
        ),
    ]

    operations = [
        migrations.RunPython(update_sectiondetail),
        migrations.RunPython(update_sectionamendproperty),
    ]
