import collections
import enum
import io
import json
import logging
import mimetypes
import os
import tempfile
import urllib.parse
import uuid
import zipfile
from collections import OrderedDict
from datetime import date, datetime, timedelta

import pandas
import PIL
import requests
from constance import config
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.gis.db import models as geomodels
from django.contrib.gis.geos import GeometryCollection, MultiPoint, Point
from django.core.exceptions import (
    ObjectDoesNotExist,
    SuspiciousOperation,
    ValidationError,
)
from django.core.files import File
from django.core.validators import FileExtensionValidator
from django.db import models, transaction
from django.db.models import (
    Case,
    CharField,
    Count,
    F,
    JSONField,
    Max,
    Min,
    OuterRef,
    ProtectedError,
    Q,
    Subquery,
    Sum,
    Value,
    When,
)
from django.db.models.functions import Concat
from django.db.models.signals import class_prepared, post_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.dateparse import parse_date
from django.utils.functional import cached_property
from django.utils.html import escape, format_html
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField
from model_utils.managers import InheritanceManager, InheritanceQuerySet, ModelT
from pdf2image import convert_from_path
from phonenumber_field.modelfields import PhoneNumberField
from PIL import Image
from simple_history.models import HistoricalRecords
from typing_extensions import Self

from geocity.apps.accounts.models import (
    AdministrativeEntity,
    SubmissionDepartment,
    User,
)
from geocity.apps.accounts.validators import validate_email
from geocity.apps.api.services import convert_string_to_api_key
from geocity.apps.forms.models import Field, Form, FormCategory

from . import fields
from .payments.models import ServiceFee, SubmissionPrice

# Actions
ACTION_AMEND = "amend"
ACTION_REQUEST_VALIDATION = "request_validation"
ACTION_VALIDATE = "validate"
ACTION_POKE = "poke"
ACTION_PROLONG = "prolong"
ACTION_COMPLEMENTARY_DOCUMENTS = "complementary_documents"
ACTION_REQUEST_INQUIRY = "request_inquiry"

# If you add an action here, make sure you also handle it in `views.get_form_for_action`,  `views.handle_form_submission`
# and services.get_actions_for_administrative_entity
ACTIONS = [
    ACTION_AMEND,
    ACTION_REQUEST_VALIDATION,
    ACTION_VALIDATE,
    ACTION_POKE,
    ACTION_PROLONG,
    ACTION_COMPLEMENTARY_DOCUMENTS,
    ACTION_REQUEST_INQUIRY,
]

logger = logging.getLogger(__name__)


def printed_submission_storage(instance, filename):
    return f"permit_requests_uploads/{instance.submission.pk}/{filename}"


class GeoTimeInfo(enum.Enum):
    DATE = enum.auto()
    GEOMETRY = enum.auto()
    # Geometry automatically genrerate from address field using geoadmin API
    GEOCODED_GEOMETRY = enum.auto()


class PolymorphicSubmissionQuerySet(InheritanceQuerySet):
    def annotate_status(self) -> Self:
        """
        Annotate status as global model attribute, because status is a specific
        field defined in each subclass of PolymorphicSubmission.
        """
        return self.annotate(
            status=Case(
                When(  # Submission
                    submission__isnull=False,
                    then=F("submission__status"),
                ),
                When(  # PermitSubmission
                    permitsubmission__isnull=False,
                    then=F("permitsubmission__status"),
                ),
                default=Value(None),
            ),
        )

    def annotate_requester(self) -> Self:
        return self.annotate(
            requester=Case(
                When(
                    submission__isnull=False,
                    then=Concat(
                        F("submission__author__first_name"),
                        Value(" "),
                        F("submission__author__last_name"),
                    ),
                ),
                When(
                    permitsubmission__isnull=False,
                    then=Case(
                        When(
                            Q(
                                permitsubmission__project_owner__company_name__isnull=True
                            )
                            | Q(
                                permitsubmission__project_owner__company_name__exact=""
                            ),
                            then=Concat(
                                F("permitsubmission__project_owner__first_name"),
                                Value(" "),
                                F("permitsubmission__project_owner__last_name"),
                            ),
                        ),
                        default=F("permitsubmission__project_owner__company_name"),
                    ),
                ),
                default=Value(None),
                output_field=CharField(),
            )
        )

    def annotate_from_specific_fields(self) -> Self:
        """
        Pull up fields in the polymorphic base model for ordering and filtering.
        Fields here, are specific, even if named the same in subclasses, options
        may differ (i.e. shortname).
        """
        annotate_data = dict(
            {
                "shortname": Case(  # Only for filtering & ordering
                    # Submission
                    When(
                        submission__isnull=False,
                        then=F("submission__shortname"),
                    ),
                    When(
                        permitsubmission__isnull=False,
                        then=F("permitsubmission__shortname"),
                    ),
                    default=Value(None),
                ),
                "sent_date": Case(
                    When(
                        submission__isnull=False,
                        then=F("submission__sent_date"),
                    ),
                    When(
                        permitsubmission__isnull=False,
                        then=F("created_at"),
                    ),
                    default=Value(None),
                ),
            }
        )

        return self.annotate_requester().annotate(**annotate_data)

    def filter_for_user(self, user) -> Self:
        """
        Filter all polymorphic submissions for the given user.
        """
        qs = self.annotate_status()

        if not user.is_superuser:
            # Either submission or permitsubmission is null in a polymorphic submission
            qs_filter = Q(
                # Owned geocity submissions
                Q(submission__isnull=False, submission__author=user)
                # Owned permit submissions
                | Q(permitsubmission__isnull=False, permitsubmission__author=user)
            )

            if user.has_perm("submissions.amend_submission") or user.has_perm(
                "submissions.read_submission"
            ):
                qs_filter |= Q(
                    Q(
                        submission__isnull=False,
                        administrative_entity__in=AdministrativeEntity.objects.associated_to_user(
                            user
                        ),
                    )
                    & ~Q(status=Submission.STATUS_DRAFT)
                )

            if user.has_perm("submissions.validate_submission"):
                qs_filter |= Q(
                    submission__isnull=False,
                    submission__validations__department__in=SubmissionDepartment.objects.filter(
                        group__in=user.groups.all()
                    ),
                )

            # Add permit submissions of my entity if I'm allowed to view them
            if user.has_perm("permits.view_permitsubmission"):
                qs_filter |= Q(
                    permitsubmission__isnull=False,
                    administrative_entity__in=AdministrativeEntity.objects.associated_to_user(
                        user
                    ),
                ) & Q(
                    status__in=[
                        PolymorphicSubmission.STATUS_AWAITING_VALIDATION,
                        PolymorphicSubmission.STATUS_PROCESSING,
                    ]
                )

            return qs.filter(qs_filter)

        return qs

    def annotate_for_table(self, user, form_filter=None, ignore_archived=True) -> Self:
        """
        Return the list of submissions this user has access to.
        """
        qs = self.filter_for_user(user)
        qs = qs.annotate_from_specific_fields()

        # Annotate for Submission
        qs = qs.annotate(
            starts_at_min=Min("submission__geo_time__starts_at"),
            ends_at_max=Max("submission__geo_time__ends_at"),
            validity_duration_max=Max("submission__forms__validity_duration"),
            remaining_validations=Count(
                "submission__validations__department", distinct=True
            )
            - Count(
                "submission__validations__department",
                distinct=True,
                filter=~Q(
                    submission__validations__validation_status=SubmissionValidation.STATUS_REQUESTED
                ),
            ),
            remaining_validations_permits=Count(
                "permitsubmission__validations__department", distinct=True
            )
            - Count(
                "permitsubmission__validations__department",
                distinct=True,
                filter=~Q(
                    permitsubmission__validations__validation_status=SubmissionValidation.STATUS_REQUESTED
                ),
            ),
            required_validations=Count(
                "submission__validations__department", distinct=True
            ),
            required_validations_permits=Count(
                "permitsubmission__validations__department", distinct=True
            ),
            author_fullname=Concat(
                F("submission__author__first_name"),
                Value(" "),
                F("submission__author__last_name"),
            ),
            author_details=Concat(
                F("submission__author__email"),
                Value(" / "),
                F("submission__author__userprofile__phone_first"),
                output_field=models.CharField(),
            ),
        )

        if form_filter is not None:
            qs = qs.annotate(form_filter=Value(form_filter))

        if ignore_archived:
            qs = qs.filter(~Q(status=Submission.STATUS_ARCHIVED))

        if not user.is_authenticated:
            return qs.none()

        return qs


class PolymorphicSubmissionManager(InheritanceManager):
    def get_queryset(self) -> InheritanceQuerySet[ModelT]:
        model: type[ModelT] = self.model  # type: ignore[attr-defined]
        return PolymorphicSubmissionQuerySet(model)


class SubmissionQuerySet(models.QuerySet):
    def filter_for_user(self, user, form_filter=None, ignore_archived=True):
        """
        Return the list of submissions this user has access to.
        """
        annotate_with = dict(
            starts_at_min=Min("geo_time__starts_at"),
            ends_at_max=Max("geo_time__ends_at"),
            validity_duration_max=Max("forms__validity_duration"),
            remaining_validations=Count("validations__department", distinct=True)
            - Count(
                "validations__department",
                distinct=True,
                filter=~Q(
                    validations__validation_status=SubmissionValidation.STATUS_REQUESTED
                ),
            ),
            required_validations=Count("validations__department", distinct=True),
            author_fullname=Concat(
                F("author__first_name"),
                Value(" "),
                F("author__last_name"),
            ),
            author_details=Concat(
                F("author__email"),
                Value(" / "),
                F("author__userprofile__phone_first"),
                output_field=models.CharField(),
            ),
        )

        if form_filter is not None:
            annotate_with.update({"form_filter": Value(form_filter)})

        qs = self.annotate(**annotate_with)

        if ignore_archived:
            qs = qs.filter(~Q(status=Submission.STATUS_ARCHIVED))

        if not user.is_authenticated:
            return qs.none()

        if not user.is_superuser:
            qs_filter = Q(author=user)

            if user.has_perm("submissions.amend_submission") or user.has_perm(
                "submissions.read_submission"
            ):
                qs_filter |= Q(
                    administrative_entity__in=AdministrativeEntity.objects.associated_to_user(
                        user
                    ),
                ) & ~Q(status=Submission.STATUS_DRAFT)

            if user.has_perm("submissions.validate_submission"):
                qs_filter |= Q(
                    validations__department__in=SubmissionDepartment.objects.filter(
                        group__in=user.groups.all()
                    )
                )
            return qs.filter(qs_filter)

        return qs


class ContactType(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("1.9 Type de contact")
        verbose_name_plural = _("1.9 Types de contacts")


# Change the app_label in order to regroup models under the same app in admin
class ContactTypeForAdminSite(ContactType):
    class Meta:
        proxy = True
        app_label = "forms"
        verbose_name = _("1.9 Type de contact")
        verbose_name_plural = _("1.9 Types de contacts")


class PolymorphicSubmission(models.Model):
    """
    Parent model for all kinds of submissions.
    Polymorphic. Not abstract (allows DB requests).

    Can hold the common attributes of "submission like" models.
    """

    STATUS_DRAFT = 0
    STATUS_SUBMITTED_FOR_VALIDATION = 1
    STATUS_APPROVED = 2
    STATUS_PROCESSING = 3
    STATUS_AWAITING_SUPPLEMENT = 4
    STATUS_AWAITING_VALIDATION = 5
    STATUS_REJECTED = 6
    STATUS_RECEIVED = 7
    STATUS_INQUIRY_IN_PROGRESS = 8
    STATUS_ARCHIVED = 9

    STATUS_DEFAULT_CHOICES = (
        (STATUS_DRAFT, _("Brouillon")),  # 0
        (STATUS_SUBMITTED_FOR_VALIDATION, _("Envoyée, en attente de traitement")),  # 1
        (STATUS_AWAITING_SUPPLEMENT, _("Demande de compléments")),  # 4
        (STATUS_PROCESSING, _("En traitement")),  # 3
        (STATUS_AWAITING_VALIDATION, _("En validation")),  # 5
        (STATUS_APPROVED, _("Approuvée")),  # 2
        (STATUS_REJECTED, _("Refusée")),  # 6
        (STATUS_RECEIVED, _("Réceptionnée")),  # 7
        (STATUS_INQUIRY_IN_PROGRESS, _("Consultation publique en cours")),  # 8
        (STATUS_ARCHIVED, _("Archivée")),  # 9
    )

    @classmethod
    def get_status_choices(cls):
        raise NotImplementedError

    administrative_entity = models.ForeignKey(
        AdministrativeEntity,
        on_delete=models.CASCADE,
        verbose_name=_("entité administrative"),
        related_name="submissions",
    )

    created_at = models.DateTimeField(_("date de création"), default=timezone.now)

    history = HistoricalRecords()
    objects = PolymorphicSubmissionQuerySet().as_manager()

    def get_id_for_display(self):
        raise NotImplementedError

    def get_specific(self):
        try:
            specific = self.submission
        except ObjectDoesNotExist:
            specific = self.permitsubmission
        return specific

    @cached_property
    def is_submission(self):
        try:
            self.submission
        except ObjectDoesNotExist:
            return False
        return True

    @cached_property
    def is_permitsubmission(self):
        try:
            self.permitsubmission
        except ObjectDoesNotExist:
            return False
        return True

    @property
    def id_for_display(self):
        return self.get_specific().get_id_for_display()

    def get_department_emails(self, department):
        email_users = (
            User.objects.filter(
                Q(
                    groups__submission_department__in=department,
                    userprofile__notify_per_email=True,
                )
            )
            .values_list("email", flat=True)
            .distinct()
        )
        return email_users

    def get_validators_email(self, departments):
        emails = []

        for department in departments:
            if department.uses_generic_email and department.generic_email:
                emails.append(department.generic_email)
            else:
                email_users = self.get_department_emails([department])
                emails.extend(email_users)

        return emails

    def get_pending_validations(self):
        return self.validations.filter(
            validation_status=SubmissionValidation.STATUS_REQUESTED
        )

    def get_backoffice_email(self):
        department_qs = self.administrative_entity.departments.filter(
            is_backoffice=True
        )

        department_qs_with_generic_email = department_qs.filter(
            uses_generic_email=True
        ).filter(uses_generic_email__isnull=False)

        if department_qs_with_generic_email:
            return [department.generic_email for department in department_qs]
        else:
            email_users = self.get_department_emails(department_qs)

            return list(email_users)

    def get_detail_url(self):
        raise NotImplementedError

    class Meta:
        indexes = [models.Index(fields=["created_at"])]

    def can_delete_submission(self, user):
        raise NotImplementedError

    def can_edit_submission(self, user):
        raise NotImplementedError


class Submission(PolymorphicSubmission):
    class AgendaStatus(models.TextChoices):
        # Cannot define `NULL = None, _("Aucun")`, cause it is transformed to string.
        # Not the best practices but is explicit and easy to understand
        NULL = "null", _("Aucun")
        CANCELLED = "CANCELLED", _("Annulé")
        SOLDOUT = "SOLDOUT", _("Complet")

    STATUS_DRAFT = 0
    STATUS_SUBMITTED_FOR_VALIDATION = 1
    STATUS_APPROVED = 2
    STATUS_PROCESSING = 3
    STATUS_AWAITING_SUPPLEMENT = 4
    STATUS_AWAITING_VALIDATION = 5
    STATUS_REJECTED = 6
    STATUS_RECEIVED = 7
    STATUS_INQUIRY_IN_PROGRESS = 8
    STATUS_ARCHIVED = 9

    STATUS_DEFAULT_CHOICES = (
        (STATUS_DRAFT, _("Brouillon")),  # 0
        (STATUS_SUBMITTED_FOR_VALIDATION, _("Envoyée, en attente de traitement")),  # 1
        (STATUS_AWAITING_SUPPLEMENT, _("Demande de compléments")),  # 4
        (STATUS_PROCESSING, _("En traitement")),  # 3
        (STATUS_AWAITING_VALIDATION, _("En validation")),  # 5
        (STATUS_APPROVED, _("Approuvée")),  # 2
        (STATUS_REJECTED, _("Refusée")),  # 6
        (STATUS_RECEIVED, _("Réceptionnée")),  # 7
        (STATUS_INQUIRY_IN_PROGRESS, _("Consultation publique en cours")),  # 8
        (STATUS_ARCHIVED, _("Archivée")),  # 9
    )

    STATUS_CHOICES = PolymorphicSubmission.STATUS_DEFAULT_CHOICES

    @classmethod
    def get_status_choices(cls):
        return cls.STATUS_CHOICES

    PolymorphicSubmission.AMENDABLE_STATUSES = {
        PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION,
        PolymorphicSubmission.STATUS_PROCESSING,
        PolymorphicSubmission.STATUS_AWAITING_SUPPLEMENT,
        PolymorphicSubmission.STATUS_RECEIVED,
    }
    # Statuses that can be used for the pilot service to add/edit the service fees
    PolymorphicSubmission.SERVICE_FEES_STATUSES = {
        PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION,
        PolymorphicSubmission.STATUS_APPROVED,
        PolymorphicSubmission.STATUS_PROCESSING,
        PolymorphicSubmission.STATUS_AWAITING_SUPPLEMENT,
        PolymorphicSubmission.STATUS_AWAITING_VALIDATION,
        PolymorphicSubmission.STATUS_REJECTED,
        PolymorphicSubmission.STATUS_RECEIVED,
        PolymorphicSubmission.STATUS_INQUIRY_IN_PROGRESS,
    }

    # Statuses that can be edited by pilot service if granted permission "edit_submission"
    # TODO Set EDITABLE_STATUSES also globally
    PolymorphicSubmission.EDITABLE_STATUSES = {
        PolymorphicSubmission.STATUS_DRAFT,
        PolymorphicSubmission.STATUS_AWAITING_SUPPLEMENT,
        PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION,
        PolymorphicSubmission.STATUS_PROCESSING,
        PolymorphicSubmission.STATUS_RECEIVED,
    }

    PolymorphicSubmission.PROLONGABLE_STATUSES = {
        PolymorphicSubmission.STATUS_APPROVED,
        PolymorphicSubmission.STATUS_PROCESSING,
        PolymorphicSubmission.STATUS_AWAITING_SUPPLEMENT,
    }

    # Statuses of submission visible in calendar (api => submissions_details)
    PolymorphicSubmission.VISIBLE_IN_CALENDAR_STATUSES = {
        PolymorphicSubmission.STATUS_APPROVED,
        PolymorphicSubmission.STATUS_INQUIRY_IN_PROGRESS,
    }

    PolymorphicSubmission.VISIBLE_IN_AGENDA_STATUSES = {
        PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION,
        PolymorphicSubmission.STATUS_PROCESSING,
        PolymorphicSubmission.STATUS_AWAITING_VALIDATION,
        PolymorphicSubmission.STATUS_APPROVED,
        PolymorphicSubmission.STATUS_RECEIVED,
        PolymorphicSubmission.STATUS_INQUIRY_IN_PROGRESS,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    PROLONGATION_STATUS_PENDING = 0
    PROLONGATION_STATUS_APPROVED = 1
    PROLONGATION_STATUS_REJECTED = 2
    PROLONGATION_STATUS_CHOICES = (
        (PROLONGATION_STATUS_PENDING, _("En attente")),
        (PROLONGATION_STATUS_APPROVED, _("Approuvée")),
        (PROLONGATION_STATUS_REJECTED, _("Refusée")),
    )

    status = models.PositiveSmallIntegerField(
        _("état"), choices=STATUS_CHOICES, default=PolymorphicSubmission.STATUS_DRAFT
    )
    shortname = models.CharField(
        _("identifiant"),
        max_length=32,
        help_text=_("Nom court de la demande (max. 32 caractères)"),
        blank=True,
    )
    validated_at = models.DateTimeField(_("date de validation"), null=True)
    sent_date = models.DateTimeField(_("date du dernier envoi"), null=True)
    forms = models.ManyToManyField(
        Form,
        through="SelectedForm",
        related_name="submissions",
        verbose_name=_("Formulaires"),
    )
    author = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("auteur"),
        related_name="submissions",
    )
    contacts = models.ManyToManyField(
        "Contact", related_name="+", through="SubmissionContact"
    )
    validation_pdf = fields.SubmissionFileField(
        _("pdf de validation"),
        validators=[FileExtensionValidator(allowed_extensions=["pdf"])],
        upload_to="validations",
    )
    creditor_type = models.ForeignKey(
        ContactType,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Destinataire de la facture"),
    )
    is_public = models.BooleanField(
        _("Publication dans le calendrier cartographique"), default=False
    )
    is_public_agenda = models.BooleanField(
        _("Publication dans l'agenda"), default=False
    )
    featured_agenda = models.BooleanField(
        _("Mise en vedette dans l'agenda"), default=False
    )
    status_agenda = models.CharField(
        _("Statut de l'évènement dans l'agenda"),
        choices=AgendaStatus.choices,
        default=AgendaStatus.NULL,
        max_length=255,
    )
    prolongation_date = models.DateTimeField(
        _("Nouvelle date de fin"), null=True, blank=True
    )
    prolongation_comment = models.TextField(_("Commentaire"), blank=True)
    prolongation_status = models.PositiveSmallIntegerField(
        _("Décision"),
        choices=PROLONGATION_STATUS_CHOICES,
        null=True,
        blank=True,
    )
    additional_decision_information = models.TextField(
        _("Information complémentaire"),
        max_length=2048,
        blank=True,
        help_text=_("Facultative, sera transmise au requérant"),
    )
    # This field is used to store SubmissionAdditionalInformationForm.reason and show historical data
    reason_supplement = models.TextField(
        _("Raison"),
        max_length=2048,
        blank=True,
        help_text=_("Raison du changement du statut de la demande"),
    )
    service_fees_total_price = models.DecimalField(
        null=True,
        blank=True,
        default=0.0,
        decimal_places=2,
        max_digits=12,
        verbose_name=_("Montant total des prestations [CHF]"),
        help_text=_("Le montant total des prestations effectuées pour cette demande. "),
    )

    history = HistoricalRecords()

    objects = SubmissionQuerySet().as_manager()

    class Meta:
        verbose_name = _("2.2 Consultation de la demande")
        verbose_name_plural = _("2.2 Consultation des demandes")
        permissions = [
            ("read_submission", _("Consulter les demandes")),
            ("amend_submission", _("Traiter les demandes")),
            ("edit_submission_validations", _("Modifier les validations")),
            ("validate_submission", _("Valider les demandes")),
            ("classify_submission", _("Classer les demandes")),
            ("edit_submission", _("Modifier les demandes")),
            ("view_private_form", _("Voir les demandes restreintes")),
            ("can_refund_transactions", _("Rembourser une transaction")),
            ("can_revert_refund_transactions", _("Revenir sur un remboursement")),
            ("can_manage_service_fee", _("Gérer une prestation")),
        ]

    def __str__(self):
        return self.shortname

    def get_specific(self):
        return self

    def is_draft(self):
        return self.status == self.STATUS_DRAFT

    def can_be_submitted_by_author(self):
        return self.can_be_edited_by_author()

    def can_be_edited_by_author(self):
        return self.status in {self.STATUS_AWAITING_SUPPLEMENT, self.STATUS_DRAFT}

    def can_be_deleted_by_author(self):
        return self.is_draft()

    def can_be_amended(self):
        return self.status in self.AMENDABLE_STATUSES

    def get_amend_field_list_always_amendable(self):
        amend_properties = []
        qs = SubmissionAmendField.objects.filter(
            Q(forms__administrative_entities=self.administrative_entity)
            & Q(can_always_update=True)
        ).distinct()
        for object in qs:
            amend_properties.append(object.name)
        return amend_properties

    def can_be_sent_for_validation(self):
        """
        This check Enables/disables the send for validation form after the permit status
        changes to STATUS_PROCESSING, which means all the validators made a decision.
        """
        statuses = self.AMENDABLE_STATUSES.copy()
        return self.status in statuses

    def can_be_edited_by_pilot(self):
        return (
            self.status in self.EDITABLE_STATUSES
            and self.forms.filter(can_always_update=True).exists()
        )

    def can_be_validated(self):
        return self.status in {self.STATUS_AWAITING_VALIDATION, self.STATUS_PROCESSING}

    def works_objects_list(self):
        return [
            f"{item.works_object.name} ({item.works_type.name})"
            for item in self.works_object_types.all()
        ]

    def works_objects_html(self):
        """
        Return the works objects as a string, separated by <br> characters.
        """
        return format_html(
            "<br>".join([escape(wo) for wo in self.works_objects_list()])
        )

    def works_objects_str(self):
        return " / ".join(self.works_objects_list())

    def has_validations(self):
        return True if self.validations.all().count() > 0 else False

    def get_min_starts_at(self):
        """
        Calculate the minimum `start_at` datetime of an event, using the current date
        + the biggest `start_delay` (in days, integer pos/neg/zero) from the existing
        works_object_types. If no works_object_types exists or none of them has a
        `start_delay`, use the current date + the default setting.
        """
        today = timezone.make_aware(datetime.today())
        max_delay = None
        if self.forms.exists():
            max_delay = self.forms.aggregate(Max("start_delay"))["start_delay__max"]

        return (
            today + timedelta(days=max_delay)
            if max_delay is not None
            else today + timedelta(days=int(settings.MIN_START_DELAY))
        )

    @cached_property
    def max_validity(self):
        """
        Calculate the maximum end date interval based on the SMALLEST validity_duration.
        Return this interval (number of days), intended to pass as a custom option
        to the widget, so the value can be used by Javascript.
        """
        return self.forms.aggregate(Min("validity_duration"))["validity_duration__min"]

    def get_max_ends_at(self):
        return self.geo_time.aggregate(Max("ends_at"))["ends_at__max"]

    def can_be_prolonged(self):
        return (
            self.status in self.PROLONGABLE_STATUSES and self.max_validity is not None
        )

    def is_prolonged(self):
        return (
            self.prolongation_status == self.PROLONGATION_STATUS_APPROVED
            and self.prolongation_date
        )

    def has_expiration_reminder(self):
        return self.forms.filter(expiration_reminder=True).exists()

    def can_prolongation_be_requested(self):
        if self.can_be_prolonged():
            today = date.today()
            # Early opt-outs:
            # None of the WOTs of the permit have a required date nor are renewables
            if self.get_max_ends_at() is None:
                return False

            if self.prolongation_status in [
                self.PROLONGATION_STATUS_REJECTED,
                self.PROLONGATION_STATUS_PENDING,
            ]:
                return False

            # Check the reminder options
            reminder = self.has_expiration_reminder()

            if reminder:
                # Here, if the reminder is active, we must have
                # the days_before_reminder value (validation on the admin)
                days_before_reminder = self.forms.aggregate(
                    Max("days_before_reminder")
                )["days_before_reminder__max"]

                if self.is_prolonged():
                    return today > (
                        self.prolongation_date.date()
                        - timedelta(days=days_before_reminder)
                    )
                else:
                    return today > (
                        self.get_max_ends_at().date()
                        - timedelta(days=days_before_reminder)
                    )
            else:
                if self.is_prolonged():
                    return today > (self.prolongation_date.date())
                else:
                    return today > (self.get_max_ends_at().date())

        else:
            # It definitively can not be prolonged
            return False

    def set_dates_for_renewable_forms(self):
        """
        Calculate and set starts_at and ends_at for the forms that have no date
        required, but can be prolonged, so they have a value in their
        validity_duration field
        """
        forms = self.forms.filter(needs_date=False).filter(validity_duration__gte=1)

        if forms.exists():
            # Determine starts_at_min and ends_at_max to check if the WOTs are combined
            # between one(s) that needs_date and already set the time interval and those
            # that do not needs_date.
            # If that's the case, the end_date must have already been limited upon
            # permit's creation to be AT MOST the minimum of the validity_duration(s).
            # Therefore we do nothing, otherwise we set both dates.
            # What a good sweat!!!

            if not self.geo_time.exists():
                # At this point following the submission steps, the Geotime object
                # must have been created only if Geometry or Dates are required,
                # if the form does not need require either, we need to create the object.
                SubmissionGeoTime.objects.create(submission_id=self.pk)

            starts_at_min = self.geo_time.aggregate(Min("starts_at"))["starts_at__min"]
            ends_at_max = self.geo_time.aggregate(Max("ends_at"))["ends_at__max"]
            if starts_at_min is None and ends_at_max is None:
                today = timezone.make_aware(datetime.today())
                self.geo_time.update(starts_at=today)
                self.geo_time.update(ends_at=today + timedelta(days=self.max_validity))

    @staticmethod
    def get_submission_site_domain(url_split):
        if "submissions" in url_split:
            submission_id_index = url_split.index("submissions") + 1
        else:
            return None

        submission_id = url_split[submission_id_index]
        submission = Submission.objects.get(id=submission_id)
        return submission.get_site(use_default=False)

    @staticmethod
    def get_absolute_url(relative_url):
        protocol = "https" if settings.SITE_HTTPS else "http"
        port = (
            f":{settings.DJANGO_DOCKER_PORT}"
            if settings.SITE_DOMAIN == "localhost"
            else ""
        )

        url_split = relative_url.split("/")
        submission_site = Submission.get_submission_site_domain(url_split)
        if submission_site:
            site_domain = submission_site
        else:
            site_domain = settings.SITE_DOMAIN

        return f"{protocol}://{site_domain}{port}{relative_url}"

    def get_site(self, use_default=False):
        """Get a site for the submission submission given"""
        sites = self.administrative_entity.sites.all()
        default_site = settings.DEFAULT_SITE

        site_not_excluded = sites.exclude(domain=default_site)
        default_site_exists = sites.filter(domain=default_site).exists()

        default_site = (
            sites.get(domain=default_site) if default_site_exists else sites.first()
        )
        other_site = (
            site_not_excluded.first() if site_not_excluded.exists() else default_site
        )

        if use_default:
            site = default_site
        else:
            site = other_site
        return site

    def start_inquiry(self):
        if self.status == self.STATUS_INQUIRY_IN_PROGRESS:
            return
        self.status = self.STATUS_INQUIRY_IN_PROGRESS
        self.save()

    @property
    def current_inquiry(self):
        """
        Try to return the current inquiry from the pre-fetched and filtered
        inquiries (needs to be added to the queryset).
        """

        if hasattr(self, "current_inquiries_filtered"):
            if len(self.current_inquiries_filtered) > 0:
                return self.current_inquiries_filtered[0]
            return None
        else:  # Check on SubmissionInquiry, it's perf leek for API if "current_inquiries_filtered" is not found
            today = datetime.today()
            return SubmissionInquiry.objects.filter(
                submission=self, start_date__lte=today, end_date__gte=today
            ).first()

    def get_forms_names_list(self):
        return ", ".join(
            list(
                self.forms.all()
                .values_list("name", flat=True)
                .distinct("name")
                .order_by("name")
            )
        )

    def archive(self, archivist):
        # make sure the request wasn't already archived
        if ArchivedSubmission.objects.filter(
            submission=self,
        ).first():
            raise SuspiciousOperation(_("La demande a déjà été archivée"))

        # make the archive
        with tempfile.SpooledTemporaryFile() as tmp_file:
            with zipfile.ZipFile(tmp_file, "w") as zip_file:
                # include the request data as CSV
                zip_file.writestr("submission.csv", self.to_csv())
                # include additional documents
                for document in self.complementary_documents.all():
                    zip_file.write(document.path, document.name)
                # include user uploaded documents
                for document in self.get_appendices_values():
                    filename = document.value["val"].split("/")[-1]
                    path = f"{settings.PRIVATE_MEDIA_ROOT}/{document.value['val']}"
                    zip_file.write(path, filename)
            # Reset file pointer
            tmp_file.seek(0)
            archived_request = ArchivedSubmission(
                submission=self,
                archivist=archivist,
            )
            archived_request.archive.save("archive.zip", tmp_file)
            archived_request.save()

        self.status = self.STATUS_ARCHIVED
        self.save()

    @property
    def is_classified(self):
        return self.status in [self.STATUS_APPROVED, self.STATUS_REJECTED]

    @property
    def is_archived(self):
        return self.status == self.STATUS_ARCHIVED

    @property
    def validation_by_validators_is_disabled(self):
        # Return true only if all forms have validation by validators disabled
        if any(self.forms.all()):
            return not any(
                not form.disable_validation_by_validators for form in self.forms.all()
            )
        else:
            return False

    @property
    def complementary_documents(self):
        return SubmissionComplementaryDocument.objects.filter(submission=self).all()

    def to_csv(self):
        from ..api.serializers import SubmissionPrintSerializer

        ordered_dict = OrderedDict(SubmissionPrintSerializer(self).data)
        ordered_dict.move_to_end("geometry")
        data_dict = dict(ordered_dict)
        data_str = json.dumps(data_dict)
        result = json.loads(data_str, object_pairs_hook=collections.OrderedDict)

        pd_dataframe = pandas.json_normalize(result)

        return pd_dataframe.to_csv(None, sep=";")

    def requires_payment(self):
        return any(form.requires_payment for form in self.forms.all())

    @transaction.atomic
    def set_field_value(self, form, field, value):
        """
        Create or update the `FieldValue` object for the given field, form and
        submission. The record will be deleted if value is an empty string or None.
        `value` can be a variety of types: str in the case of a text field, bool in
        the case of a boolean field, int in the case of a number field, and File
        or bool in the case of a file field (the latter being `False` if the user is
        asking for the file to be removed).
        """
        existing_value_obj = FieldValue.objects.filter(
            selected_form__submission=self,
            selected_form__form=form,
            field=field,
        )
        is_file = field.input_type == Field.INPUT_TYPE_FILE
        is_date = field.input_type == Field.INPUT_TYPE_DATE

        if value == "" or value is None:
            existing_value_obj.delete()
        else:
            if is_file:

                # Use private storage to prevent uploaded files exposition to the outside world
                private_storage = fields.PrivateFileSystemStorage()
                # If the given File has a `url` attribute, it means the value comes from the `initial` form data, so the
                # value hasn't changed
                if getattr(value, "url", None):
                    return

                # Remove the previous file, if any
                try:
                    current_value = existing_value_obj.get()
                except FieldValue.DoesNotExist:
                    pass
                else:
                    private_storage.delete(current_value.value["val"])
                # User has asked to remove the file. The file has already been removed from the storage, remove the property
                # value record and we're done
                if value is False:
                    existing_value_obj.delete()
                    return

                # TODO move all the low-level file processing mechanism elsewhere
                # Add the file to the storage
                directory = "permit_requests_uploads/{}".format(self.pk)
                ext = os.path.splitext(value.name)[1]
                upper_ext = ext[1:].upper()

                # Use uuid for file names to prevent thumbor to keep in cache a new file that uses the same name
                file_uuid = uuid.uuid4()
                path = os.path.join(
                    directory, "{}_{}_{}{}".format(form.pk, field.pk, file_uuid, ext)
                )

                # Check file size and extension
                from . import services

                file_extensions = services.get_allowed_extensions(
                    config.ALLOWED_FILE_EXTENSIONS, field.allowed_file_types
                )

                allowed_mimetypes = [
                    mimetypes.types_map[f".{item}"] for item in file_extensions
                ]

                services.validate_file(value, allowed_mimetypes)

                private_storage.save(path, value)

                # Postprocess images: remove all exif metadata from for better security and user privacy
                if upper_ext != "PDF":
                    upper_ext = ext[1:].upper()
                    formats_map = {"JPG": "JPEG"}
                    with Image.open(value) as image_full:
                        data = list(image_full.getdata())
                        new_image = Image.new(image_full.mode, image_full.size)
                        new_image.putdata(data)
                        new_image.save(
                            private_storage.location + "/" + path,
                            (
                                formats_map[upper_ext]
                                if upper_ext in formats_map.keys()
                                else upper_ext
                            ),
                        )
                # Postprocess PDF: convert everything to image, do not keep other content
                elif upper_ext == "PDF":
                    # File size to fix decompression bomb error
                    PIL.Image.MAX_IMAGE_PIXELS = None

                    all_images = convert_from_path(
                        private_storage.location + "/" + path, dpi=150
                    )

                    optimized_images = []

                    for image in all_images:
                        img_byte_arr = io.BytesIO()
                        image.save(
                            img_byte_arr, format="JPEG", quality=85, optimize=True
                        )
                        img_byte_arr.seek(0)
                        optimized_image = Image.open(img_byte_arr)
                        optimized_images.append(optimized_image)

                    first_image = optimized_images[0]
                    following_images = optimized_images[1:]

                    if len(following_images) > 0:
                        first_image.save(
                            private_storage.location + "/" + path,
                            save_all=True,
                            append_images=following_images,
                        )
                    else:
                        first_image.save(
                            private_storage.location + "/" + path, save_all=True
                        )

                value = path

            elif is_date:
                value = value.isoformat()

            value_dict = {"val": value}
            nb_objs = existing_value_obj.update(value=value_dict)

            # No existing property value record, create it
            if nb_objs == 0:
                (
                    selected_form,
                    created,
                ) = SelectedForm.objects.get_or_create(submission=self, form=form)
                FieldValue.objects.create(
                    selected_form=selected_form,
                    field=field,
                    value=value_dict,
                )

    def get_fields_values(self):
        """
        Return a queryset of `FieldValue` objects for this submission, excluding
        properties of type file.
        """
        return (
            FieldValue.objects.filter(selected_form__submission=self)
            .exclude(field__input_type=Field.INPUT_TYPE_FILE)
            .select_related(
                "selected_form",
                "selected_form__form",
                "field",
            )
        )

    def get_appendices_values(self):
        """
        Return a queryset of `FieldValue` objects of type file for this submission.
        """
        return FieldValue.objects.filter(
            selected_form__submission=self,
            field__input_type=Field.INPUT_TYPE_FILE,
        ).select_related(
            "selected_form__form",
            "field",
        )

    def get_form_categories(self):
        return (
            FormCategory.objects.filter(pk__in=self.forms.values_list("category_id"))
            .order_by("name")
            .distinct()
        )

    def _get_fields_filtered(self, props_filter):
        """
        Return a list of `(Form, QuerySet[Field])` for all forms of this submission.
        `props_filter` is passed the properties queryset and should return it (or a
        filtered version of it).

        TODO move this in forms app?
        """

        fields_by_form = [
            (
                form,
                props_filter(
                    form.fields.filter(form_fields__form=form).order_by(
                        "form_fields__order", "name"
                    )
                ),
            )
            for form in self.forms.order_by("order", "name")
        ]

        return [(form, fields) for form, fields in fields_by_form if fields]

    def get_fields_by_form(self, additional_type_exclusions=None):
        """
        FIXME docstring
        """
        exclusions = [
            Field.INPUT_TYPE_FILE,
        ]
        if additional_type_exclusions is not None:
            exclusions += additional_type_exclusions
        return self._get_fields_filtered(
            lambda qs: qs.exclude(
                input_type__in=[
                    Field.INPUT_TYPE_FILE,
                ]
                + exclusions
            ),
        )

    def get_appendices_fields_by_form(self):
        return self._get_fields_filtered(
            lambda qs: qs.filter(input_type=Field.INPUT_TYPE_FILE),
        )

    @transaction.atomic
    def set_selected_forms(self, new_forms):
        """
        Add the given `new_works_object_types`, which should be an iterable of `WorksObjectType` instances to the given
        `submission`. Existing `WorksObjectType` are ignored.
        """
        # Check which object type are new or have been removed. We can't just remove them all and recreate them
        # because there might be data related to these relations (eg. FieldValue)
        self.get_selected_forms().exclude(form__in=new_forms).delete()

        for form in new_forms:
            SelectedForm.objects.get_or_create(submission=self, form=form)

        geotime_objects = self.get_geotime_objects()

        if len(geotime_objects) > 0:
            geotime_required_info = self.get_geotime_required_info()
            # Reset the geometry/date if the new_works_object_type do not need Date/Geom
            if len(geotime_required_info) == 0:
                geotime_objects.delete()
            # Reset the date only
            if GeoTimeInfo.DATE not in geotime_required_info:
                geotime_objects.update(starts_at=None, ends_at=None)
            # Reset the geometry only
            if GeoTimeInfo.GEOMETRY not in geotime_required_info:
                geotime_objects.update(geom=None)

    def get_contacts_forms(self):
        """
        Get contacts forms defined for each form defined for the submission.
        """
        return (
            ContactForm.objects.filter(form_category__in=self.get_form_categories())
            .values_list("type", "is_mandatory", "is_dynamic")
            .distinct()
            .order_by("-is_mandatory", "order")
        )

    def has_any_dynamic_contacts_forms(self):
        """
        Get contacts forms assigned as dynamic, defined for each form defined for the submission
        """
        return ContactForm.objects.filter(
            form_category__in=self.get_form_categories(), is_dynamic=True
        ).exists()

    def get_missing_required_contact_forms(self):
        """
        Get contacts forms required but not filled
        """

        return self.filter_only_missing_contact_forms(
            [
                (actor_form, is_mandatory)
                for actor_form, is_mandatory, is_dynamic in self.get_contacts_forms()
                if is_mandatory
            ],
        )

    def get_geotime_required_info(self):
        forms = self.forms.all()
        required_info = set()
        if any(form.needs_date for form in forms):
            required_info.add(GeoTimeInfo.DATE)

        if any(form.has_geometry for form in forms):
            required_info.add(GeoTimeInfo.GEOMETRY)
        else:
            exclusions = [
                field[0]
                for field in Field.INPUT_TYPE_CHOICES
                if field[0] != Field.INPUT_TYPE_ADDRESS
            ]

            if (
                self.get_fields_by_form(exclusions)
                and self.get_geotime_objects()
                .filter(comes_from_automatic_geocoding=True)
                .exists()
            ):
                required_info.add(GeoTimeInfo.GEOCODED_GEOMETRY)
        return required_info

    def get_pending_validators_email(self, pending_validations):
        emails = []

        for validation in pending_validations:
            if (
                validation.department.uses_generic_email
                and validation.department.generic_email
            ):
                emails.append(validation.department.generic_email)
            else:
                email_users = self.get_department_emails([validation.department])
                emails.extend(email_users)

        return emails

    def get_complementary_documents(self, user):
        qs = self.complementary_documents.all().order_by("pk").distinct()

        if user.is_superuser:
            return qs

        return qs.filter(
            Q(is_public=True)
            | Q(owner=user)
            | Q(authorised_departments__group__in=user.groups.all()),
        )

    def get_amend_custom_fields_by_form(self):
        forms = self.forms.prefetch_related("amend_fields").select_related("category")

        for form in forms:
            yield (form, form.amend_fields.all())

    @transaction.atomic
    def set_amend_custom_field_value(self, form, field, value):
        """
        Create or update the `SubmissionAmendFieldValue` object for the given
        field, form and submission. The record will be deleted if value is
        an empty string or None. Value is only str type.
        TODO: why is there a "custom" in this method name?
        """
        existing_value_obj = SubmissionAmendFieldValue.objects.filter(
            form__submission=self,
            form__form=form,
            field=field,
        )

        if value == "" or value is None:
            existing_value_obj.delete()
        else:
            try:
                value_obj = existing_value_obj.get()
                value_obj.value = value
                value_obj.save()
            except SubmissionAmendFieldValue.DoesNotExist:
                # No existing field value record, create it
                selected_form, created = SelectedForm.objects.get_or_create(
                    submission=self, form=form
                )
                SubmissionAmendFieldValue.objects.create(
                    form=selected_form,
                    field=field,
                    value=value,
                )

    def get_amend_custom_fields_values(self):
        """
        Return a queryset of `SubmissionAmendFieldValue` objects for this submission.
        """
        return SubmissionAmendFieldValue.objects.filter(
            form__submission=self
        ).select_related(
            "form__form",
            "field",
        )

    def get_submission_directives(self):
        entity = self.administrative_entity
        entity_directives = []
        if (
            entity.directive
            or entity.directive_description
            or entity.additional_information
        ):
            entity_directives.append(
                (
                    entity.directive,
                    entity.directive_description,
                    entity.additional_information,
                )
            )

        form_directives = [
            (obj.directive, obj.directive_description, obj.additional_information)
            for obj in self.forms.exclude(
                directive="", directive_description="", additional_information=""
            )
        ]

        return entity_directives + form_directives

    @transaction.atomic
    def set_administrative_entity(self, administrative_entity):
        """
        Set the given `administrative_entity`, which should be an instance of `models.PermitAdministrativeEntity`.
        `WorksObjectTypeChoice` records that don't exist in the new `administrative_entity` will be deleted.
        """
        self.selected_forms.exclude(form__in=administrative_entity.forms.all()).delete()

        self.administrative_entity = administrative_entity
        self.save()

    def get_services_to_notify_mailing_list(self):
        mailing_list = []

        forms_to_notify = self.forms.filter(notify_services=True)

        for emails in forms_to_notify.values_list("services_to_notify", flat=True):
            emails_addresses = emails.replace("\n", ",").split(",")
            mailing_list += [
                ea.strip() for ea in emails_addresses if validate_email(ea.strip())
            ]

        return mailing_list

    def has_document_enabled(self):
        return self.forms.filter(document_enabled=True).exists()

    def filter_only_missing_contact_forms(self, contact_forms):
        """
        Filter the given `contact_forms` to return only the ones that have not been set in the given `submission`.
        """

        existing_contact_forms = self.contacts.values_list(
            "submissioncontact__contact_form", flat=True
        )

        return [
            contact_form
            for contact_form in contact_forms
            if contact_form[0] not in existing_contact_forms
        ]

    def get_geotime_objects(self, exlude_geocoded_geom=False):
        return self.geo_time.filter(comes_from_automatic_geocoding=exlude_geocoded_geom)

    def get_actions_for_administrative_entity(self):
        """
        Filter out administrative workflow step that are not coherent
        with current submission status
        """

        # Statuses for which a given action should be available
        required_statuses_for_actions = {
            "amend": list(Submission.AMENDABLE_STATUSES),
            "request_validation": [Submission.STATUS_AWAITING_VALIDATION],
            "poke": [Submission.STATUS_AWAITING_VALIDATION],
            "validate": [
                Submission.STATUS_APPROVED,
                Submission.STATUS_REJECTED,
                Submission.STATUS_AWAITING_VALIDATION,
                Submission.STATUS_PROCESSING,
            ],
            "prolong": list(Submission.PROLONGABLE_STATUSES),
            "complementary_documents": [
                Submission.STATUS_AWAITING_VALIDATION,
                Submission.STATUS_PROCESSING,
            ],
            "request_inquiry": list(Submission.AMENDABLE_STATUSES),
            "transactions": [
                Submission.STATUS_APPROVED,
                Submission.STATUS_REJECTED,
                Submission.STATUS_AWAITING_VALIDATION,
                Submission.STATUS_PROCESSING,
            ],
            "can_manage_service_fee": list(Submission.SERVICE_FEES_STATUSES),
        }

        available_statuses_for_administrative_entity = (
            SubmissionWorkflowStatus.objects.get_statuses_for_administrative_entity(
                self.administrative_entity
            )
        )
        available_actions = []
        for action in required_statuses_for_actions.keys():
            action_as_set = set(required_statuses_for_actions[action])
            enabled_actions = list(
                action_as_set.intersection(available_statuses_for_administrative_entity)
            )
            if enabled_actions:
                available_actions.append(action)

        distinct_available_actions = list(dict.fromkeys(available_actions))
        return distinct_available_actions

    def is_validation_document_required(self, requirement_type=None):
        filters = {"validation_document": True}

        if requirement_type == "only_for_approval":
            filters[
                "validation_document_required_for"
            ] = Form.VALIDATION_DOCUMENT_REQUIRED_FOR_APPROVAL
        elif requirement_type == "only_for_refusal":
            filters[
                "validation_document_required_for"
            ] = Form.VALIDATION_DOCUMENT_REQUIRED_FOR_REFUSAL
        else:
            filters[
                "validation_document_required_for"
            ] = Form.VALIDATION_DOCUMENT_REQUIRED_FOR_APPROVAL_AND_REFUSAL

        return self.forms.filter(**filters).exists()

    def has_default_validation_texts(self):
        return self.forms.exclude(default_validation_text="").exists()

    def get_default_validation_texts(self):
        return [
            form.default_validation_text
            for form in self.forms.exclude(default_validation_text="")
        ]

    def has_geo_step_help_texts(self):
        return self.forms.exclude(geo_step_help_text="").exists()

    def get_geo_step_help_texts(self):
        return [
            form.geo_step_help_text
            for form in self.forms.exclude(geo_step_help_text="")
        ]

    def can_have_multiple_ranges(self):
        return any(form.can_have_multiple_ranges for form in self.forms.all())

    def get_selected_forms(self):
        return SelectedForm.objects.filter(submission=self)

    def reverse_geocode_and_store_address_geometry(self, to_geocode_addresses):
        # Delete the previous geocoded geometries
        SubmissionGeoTime.objects.filter(
            submission=self, comes_from_automatic_geocoding=True
        ).delete()

        if to_geocode_addresses:
            geoadmin_address_search_api = settings.LOCATIONS_SEARCH_API
            geom = GeometryCollection()
            for address in to_geocode_addresses:
                search_params = {
                    "searchText": address,
                    "limit": 1,
                    "partitionlimit": 1,
                    "type": "locations",
                    "sr": "2056",
                    "lang": "fr",
                    "origins": "address",
                }

                data = urllib.parse.urlencode(search_params)
                url = f"{geoadmin_address_search_api}?{data}"
                # GEOADMIN API might be down and we don't want to block the user
                try:
                    response = requests.get(url, timeout=2)
                except requests.exceptions.RequestException:
                    return None

                if response.status_code == 200 and response.json()["results"]:
                    x = response.json()["results"][0]["attrs"]["x"]
                    y = response.json()["results"][0]["attrs"]["y"]
                    geom.append(MultiPoint(Point(y, x, srid=2056)))
                # If geocoding matches nothing, set the address value on the administrative_entity centroid point
                else:
                    geom.append(MultiPoint(self.administrative_entity.geom.centroid))

            # Save the new ones
            SubmissionGeoTime.objects.create(
                submission=self,
                comes_from_automatic_geocoding=True,
                geom=geom,
            )

    def get_form_for_payment(self):
        """
        For online payments, only one form can exist on a Submission
        """
        if self.forms.count() != 1:
            logger.warning(
                f"Multiple forms in the submission ({self.pk}), in an "
                f"entity set to [single form submission]. Payment feature "
                f"not available."
            )
            return None
        return self.forms.first()

    def requires_online_payment(self):
        form_for_payment = self.get_form_for_payment()
        return form_for_payment and form_for_payment.requires_online_payment

    @property
    def submission_price(self):
        return self.get_submission_price()

    def get_submission_price(self):
        try:
            return SubmissionPrice.objects.get(submission=self)
        except SubmissionPrice.DoesNotExist:
            return None

    def get_transactions(self):
        # TODO: if more payment processors are implemented, change this to add all transaction types to queryset
        if self.submission_price is None:
            return None
        return self.submission_price.get_transactions()

    def get_last_prolongation_transaction(self):
        from .payments.models import Transaction

        if self.get_transactions() is None:
            return None
        return (
            self.get_transactions()
            .filter(transaction_type=Transaction.TYPE_PROLONGATION)
            .order_by("-updated_date")
            .first()
        )

    def set_prolongation_requested_and_notify(self, prolongation_date):
        from . import services

        self.prolongation_status = self.PROLONGATION_STATUS_PENDING
        self.prolongation_date = prolongation_date
        self.save()

        attachments = []
        if self.requires_online_payment() and self.author.userprofile.notify_per_email:
            attachments = self.get_submission_payment_attachments("confirmation")
            data = {
                "subject": "{} ({})".format(
                    _("Votre demande de prolongation"), self.get_forms_names_list()
                ),
                "users_to_notify": [self.author.email],
                "template": "submission_acknowledgment.txt",
                "submission": self,
                "absolute_uri_func": Submission.get_absolute_url,
            }
            services.send_email_notification(data, attachments=attachments)

        data = {
            "subject": "{} ({})".format(
                _("Une demande de prolongation vient d'être soumise"),
                self.get_forms_names_list(),
            ),
            "users_to_notify": self.get_backoffice_email(),
            "template": "submission_prolongation_for_services.txt",
            "submission": self,
            "absolute_uri_func": Submission.get_absolute_url,
        }
        services.send_email_notification(data, attachments=attachments)

    # ServiceFees for submission
    def get_service_fees(self):
        return ServiceFee.objects.filter(submission=self)

    def update_service_fees_total_price(self):
        """
        Sum the monetary total of fees created for this submission
        """
        total = ServiceFee.objects.filter(submission=self.pk).aggregate(
            total=Sum("monetary_amount")
        )["total"]
        self.service_fees_total_price = total
        self.save()

    def get_history(self):
        # Transactions history
        if self.submission_price is None:
            transactions = []
        else:
            transactions = self.submission_price.transactions.all()
        transaction_versions = []
        last_status = ""
        for transaction in transactions:
            versions = []
            for version in transaction.history.all():
                # Include only updates that changed the transaction status
                if last_status != version.status:
                    versions.append(version)
                last_status = version.status
            transaction_versions += versions

        # Merge with Submission (self) history
        history = [
            (event.history_date, event)
            for event in (*self.history.all(), *transaction_versions)
        ]
        history.sort(reverse=True)
        return history

    def get_last_transaction(self):
        if self.get_transactions() is None:
            return None
        return self.get_transactions().order_by("-updated_date").last()

    def get_submission_payment_attachments(self, pdf_type):
        pdf_types = {
            "confirmation": lambda p: p.payment_confirmation_report,
            "refund": lambda p: p.payment_refund_report,
        }
        report_func = pdf_types[pdf_type]
        form = self.get_form_for_payment()

        if not form or (
            not form.payment_settings or not report_func(form.payment_settings)
        ):
            return []
        child_doc_type = None
        for doc_type in report_func(form.payment_settings).document_types.all():
            if doc_type.parent.form.pk == form.pk:
                child_doc_type = doc_type
                break

        if not child_doc_type:
            return []

        comp_doc = (
            self.get_complementary_documents(self.author)
            .filter(document_type=child_doc_type)
            .last()
        )
        if not comp_doc:
            return []
        return [(comp_doc.name, comp_doc.document.file.read())]

    def generate_and_save_pdf(self, pdf_type, transaction):
        pdf_types = {
            "confirmation": (
                lambda t: t.get_confirmation_pdf(),
                lambda p: p.payment_confirmation_report,
                "Facture",
            ),
            "refund": (
                lambda t: t.get_refund_pdf(),
                lambda p: p.payment_refund_report,
                "Remboursement",
            ),
        }
        gen_func, report_func, description = pdf_types[pdf_type]
        file_name, file_bytes = gen_func(transaction)
        complementary_document_attrs = {
            "document": File(
                file_bytes,
                name=file_name,
            )
        }
        form = self.get_form_for_payment()
        child_doc_type = None
        if not form or not form.payment_settings:
            return None
        for doc_type in report_func(form.payment_settings).document_types.all():
            if doc_type.parent.form.pk == form.pk:
                child_doc_type = doc_type
                break

        if child_doc_type is None:
            # Payment settings are not configured correctly, failing silently
            return None
        complementary_document_attrs["document_type"] = child_doc_type

        complementary_document_attrs[
            "description"
        ] = f"{description} {transaction.transaction_id}"
        complementary_document_attrs["owner"] = self.author
        complementary_document_attrs["submission"] = self
        complementary_document_attrs[
            "status"
        ] = SubmissionComplementaryDocument.STATUS_FINALE

        complementary_document_attrs["is_public"] = False
        comp_doc = SubmissionComplementaryDocument.objects.create(
            **complementary_document_attrs
        )

        comp_doc.authorised_departments.set(SubmissionDepartment.objects.all())
        return comp_doc

    def has_any_form_with_exceeded_submissions(self, user_for_bypass=None):
        return any(
            form.has_exceeded_maximum_submissions(user_for_bypass)
            for form in self.forms.all()
        )

    def get_maximum_submissions_message(self):
        for form in self.forms.all():
            return form.max_submissions_message

    def get_id_for_display(self):
        if settings.IS_PERMITS_ENABLED:
            return f"G-{self.pk}"
        return f"#{self.pk}"

    def get_detail_url(self):
        return reverse(
            "submissions:submission_detail",
            kwargs={"submission_id": self.pk},
        )

    def can_edit_submission(self, user):
        from .exceptions import BadSubmissionStatus
        from .permissions import can_always_be_updated, can_edit_submission
        from .shortcuts import get_submission_for_user_or_404

        if can_always_be_updated(user, self):
            allowed_statuses = {
                Submission.STATUS_DRAFT,
                Submission.STATUS_SUBMITTED_FOR_VALIDATION,
                Submission.STATUS_APPROVED,
                Submission.STATUS_PROCESSING,
                Submission.STATUS_AWAITING_SUPPLEMENT,
                Submission.STATUS_AWAITING_VALIDATION,
                Submission.STATUS_REJECTED,
                Submission.STATUS_RECEIVED,
            }
        else:
            allowed_statuses = {
                Submission.STATUS_DRAFT,
                Submission.STATUS_AWAITING_SUPPLEMENT,
                Submission.STATUS_SUBMITTED_FOR_VALIDATION,
            }

        try:
            submission = get_submission_for_user_or_404(
                user,
                self.id,
                statuses=allowed_statuses,
            )

            can_pilot_edit_submission = can_edit_submission(user, submission)

            if (
                submission.status == Submission.STATUS_SUBMITTED_FOR_VALIDATION
                and not can_pilot_edit_submission
            ):
                raise BadSubmissionStatus(
                    submission,
                    [
                        Submission.STATUS_DRAFT,
                        Submission.STATUS_AWAITING_SUPPLEMENT,
                    ],
                )
            return bool(submission)
        except:
            return False

    def can_delete_submission(self, user):
        return self.can_edit_submission(user)


class BaseContact(models.Model):
    first_name = models.CharField(
        _("Prénom"),
        max_length=150,
    )
    last_name = models.CharField(
        _("Nom"),
        max_length=100,
    )
    company_name = models.CharField(_("Entreprise"), max_length=100, blank=True)
    vat_number = models.CharField(_("Numéro TVA"), max_length=19, blank=True)
    address = models.CharField(
        _("Adresse"),
        max_length=100,
    )
    zipcode = models.PositiveIntegerField(
        _("Code postal"),
    )
    city = models.CharField(
        _("Ville"),
        max_length=100,
    )
    country = CountryField(
        _("Pays"),
        null=True,
        blank=False,
    )
    phone = PhoneNumberField(
        _("Téléphone"),
    )
    email = models.EmailField(
        _("Email"),
    )

    class Meta:
        verbose_name = _("Contact")
        abstract = True

    def __str__(self):
        return self.first_name + " " + self.last_name


class Contact(BaseContact):
    history = HistoricalRecords()


class SelectedForm(models.Model):
    """
    This intermediary model represents the selected objects for a permit
    request. Property values will then point to this model.
    """

    submission = models.ForeignKey(
        "Submission", on_delete=models.CASCADE, related_name="selected_forms"
    )
    form = models.ForeignKey(
        Form, on_delete=models.CASCADE, related_name="selected_forms"
    )

    class Meta:
        unique_together = [("submission", "form")]


class ContactForm(models.Model):
    type = models.ForeignKey(
        ContactType,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("type de contact"),
    )
    form_category = models.ForeignKey(
        FormCategory,
        on_delete=models.CASCADE,
        verbose_name=_("type de demande"),
        related_name="contact_forms",
    )
    is_mandatory = models.BooleanField(_("obligatoire"), default=True)
    is_dynamic = models.BooleanField(
        _("Dynamique"),
        help_text=_(
            "Permet à l'utilisateur d'ajouter ce type de contact lors de la saisie, autant de fois que souhaité."
        ),
        default=False,
    )
    order = models.PositiveIntegerField(
        _("ordre"), default=0, blank=False, null=False, db_index=True
    )
    integrator = models.ForeignKey(
        Group,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Groupe intégrateur"),
        limit_choices_to={"submission_department__is_integrator_admin": True},
    )

    class Meta:
        verbose_name = _("1.5 Contact")
        verbose_name_plural = _("1.5 Contacts")
        unique_together = [["type", "form_category"]]
        ordering = ("order",)

    def __str__(self):
        return str(self.type) + " (" + str(self.form_category) + ")"


# Change the app_label in order to regroup models under the same app in admin
class ContactFormForAdminSite(ContactForm):
    class Meta:
        proxy = True
        app_label = "forms"
        verbose_name = _("1.5 Contact")
        verbose_name_plural = _("1.5 Contacts")


class SubmissionContact(models.Model):
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    submission = models.ForeignKey(
        "Submission", on_delete=models.CASCADE, related_name="submission_contacts"
    )
    contact_form = models.ForeignKey(
        ContactType,
        on_delete=models.DO_NOTHING,
        verbose_name=_("type de contact"),
    )

    class Meta:
        verbose_name = _("Relation demande-contact")
        verbose_name_plural = _("Relations demande-contact")

    def __str__(self):
        return "{} - {}".format(str(self.contact), str(self.contact_form.name))


class FieldValue(models.Model):
    """
    Value of a property for a selected object in a submission.
    """

    field = models.ForeignKey(
        Field,
        verbose_name=_("caractéristique"),
        on_delete=models.PROTECT,
        related_name="+",
    )
    selected_form = models.ForeignKey(
        SelectedForm,
        verbose_name=_("objet"),
        on_delete=models.CASCADE,
        related_name="field_values",
    )
    # Storing the value in a JSON field allows to keep the value type
    # (eg. boolean, int) instead of transforming everything to str
    value = JSONField()
    history = HistoricalRecords()

    class Meta:
        unique_together = [("field", "selected_form")]

    def get_value(self):
        value = self.value["val"]
        if self.field.input_type == Field.INPUT_TYPE_DATE:
            return parse_date(value)

        elif self.field.input_type == Field.INPUT_TYPE_FILE:
            private_storage = fields.PrivateFileSystemStorage()
            # TODO: handle missing files! Database pointing empty files should be removed
            try:
                f = private_storage.open(value)
                # The `url` attribute of the file is used to detect if there was already a file set (it is used by
                # `ClearableFileInput` and by the `set_object_property_value` function)
                f.url = reverse(
                    "submissions:submission_media_download",
                    kwargs={"property_value_id": self.pk},
                )
            except IOError:
                f = None

            return f

        return value


class SubmissionGeoTime(models.Model):
    """
    Permit location in space and time
    """

    submission = models.ForeignKey(
        Submission, on_delete=models.CASCADE, related_name="geo_time"
    )
    starts_at = models.DateTimeField(_("Date de début"), blank=True, null=True)
    ends_at = models.DateTimeField(_("Date de fin"), blank=True, null=True)
    comment = models.CharField(
        _("Commentaire"), max_length=1024, blank=True
    )  # TODO: remove in v4 and adapt front component
    external_link = models.URLField(
        _("Lien externe"), blank=True
    )  # TODO: remove in v4 and adapt front component
    comes_from_automatic_geocoding = models.BooleanField(
        _("Géométrie obtenue par géocodage d'adresse"), default=False
    )
    form = models.ForeignKey(
        Form, on_delete=models.CASCADE, related_name="geo_time", null=True
    )
    field = models.ForeignKey(
        Field, on_delete=models.CASCADE, related_name="geo_time", null=True
    )
    geom = geomodels.GeometryCollectionField(_("Localisation"), null=True, srid=2056)
    history = HistoricalRecords()

    class Meta:
        verbose_name = _("3.3 Consultation de l'agenda et de la géométrie")
        verbose_name_plural = _("3.3 Consultation des agenda et géométries")
        indexes = [
            models.Index(fields=["starts_at"]),
            models.Index(fields=["ends_at"]),
        ]


class SubmissionWorkflowStatusQuerySet(models.QuerySet):
    def get_statuses_for_administrative_entity(self, administrative_entity):
        """
        Returns the status availables for an administrative entity
        """
        return self.filter(administrative_entity=administrative_entity).values_list(
            "status", flat=True
        )


class SubmissionWorkflowStatus(models.Model):
    """
    Represents a status in the administrative workflow
    """

    status = models.PositiveSmallIntegerField(
        _("statut"),
        choices=Submission.STATUS_CHOICES,
    )
    administrative_entity = models.ForeignKey(
        AdministrativeEntity,
        on_delete=models.CASCADE,
        related_name="enabled_statuses",
    )

    objects = SubmissionWorkflowStatusQuerySet().as_manager()

    def __str__(self):
        return str(self.get_status_display())

    class Meta:
        verbose_name = _("Status disponible pour l'entité administrative")
        verbose_name_plural = _("Status disponibles pour l'entité administratives")
        unique_together = ("status", "administrative_entity")


class ArchivedSubmissionQuerySet(models.QuerySet):
    def filter_for_user(self, user):
        """
        Return the list of archived requests this user has access to.
        """
        if not user.is_authenticated:
            return self.none()

        if user.is_superuser:
            return self

        qs_filter = Q(archivist=user)
        qs_filter |= Q(
            submission__administrative_entity__in=AdministrativeEntity.objects.associated_to_user(
                user
            )
        )

        return self.filter(qs_filter)


class ArchivedSubmission(models.Model):
    archived_date = models.DateTimeField(auto_now_add=True)
    archivist = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("Personne ayant archivé la demande"),
    )
    submission = models.OneToOneField(
        Submission,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    archive = fields.ArchiveDocumentFileField(_("Archive"))

    objects = ArchivedSubmissionQuerySet().as_manager()

    @property
    def path(self):
        return self.archive.path

    def delete(self, using=None, keep_parents=False):
        # TODO: is this really wanted ?!!
        self.submission.delete()
        ret = super().delete(using, keep_parents)
        # delete the archive file
        self.archive.delete(save=False)
        return ret


class SubmissionInquiry(models.Model):
    start_date = models.DateField()
    end_date = models.DateField()
    documents = models.ManyToManyField(
        "SubmissionComplementaryDocument",
        verbose_name=_("Documents complémentaires"),
        blank=True,
    )
    submission = models.ForeignKey(
        Submission,
        null=False,
        on_delete=models.CASCADE,
        verbose_name=_("Demande"),
        related_name="inquiries",
    )
    submitter = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("Demandeur de l'enquête"),
    )

    class Meta:
        verbose_name = _("2.3 Enquête publique")
        verbose_name_plural = _("2.3 Enquêtes publiques")


class BaseSubmissionDocument(models.Model):

    document = fields.ComplementaryDocumentFileField(_("Document"))

    @property
    def uri(self):
        return self.document.url

    @property
    def name(self):
        return self.document.name

    @property
    def path(self):
        return self.document.path

    def delete(self, using=None, keep_parents=False):
        # delete the uploaded file
        try:
            if os.path.exists(self.document.path):
                os.remove(self.document.path)
                return super().delete(using, keep_parents)
        except OSError as e:
            raise ProtectedError(
                _("Le document {} n'a pas pu être supprimé".format(self)), e
            )

    def __str__(self):
        return self.document.name

    class Meta:
        abstract = True


# Define the post_delete signal handler
def handle_file_deletion(instance, **kwargs):
    if instance.document:
        try:
            if os.path.exists(instance.document.path):
                os.remove(instance.document.path)
        except OSError as e:
            raise ProtectedError(
                _("The document {} could not be deleted".format(instance)), e
            )


# Dynamically connect post_delete to child classes of BaseSubmissionDocument
@receiver(class_prepared)
def connect_post_delete_signal(sender, **kwargs):
    if issubclass(sender, BaseSubmissionDocument) and not sender._meta.abstract:
        post_delete.connect(handle_file_deletion, sender=sender)


class SubmissionComplementaryDocument(BaseSubmissionDocument):
    STATUS_TEMP = 0
    STATUS_FINALE = 1
    STATUS_OTHER = 2
    STATUS_CANCELED = 3

    STATUS_CHOICES = (
        (STATUS_TEMP, _("Provisoire")),
        (STATUS_FINALE, _("Final")),
        (STATUS_OTHER, _("Autre")),
        (STATUS_CANCELED, _("Annulé")),
    )

    description = models.TextField(
        _("Description du document"),
        blank=True,
    )
    owner = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("Propriétaire du document"),
        related_name="complementary_documents",
    )
    submission = models.ForeignKey(
        Submission,
        null=False,
        on_delete=models.CASCADE,
        verbose_name=_("Demande"),
        related_name="complementary_documents",
    )
    status = models.PositiveSmallIntegerField(
        _("Statut du document"),
        choices=STATUS_CHOICES,
    )
    authorised_departments = models.ManyToManyField(
        SubmissionDepartment,
        verbose_name=_("Service(s) autorisé(s) à visualiser le document"),
        related_name="complementary_documents",
    )
    is_public = models.BooleanField(default=False, verbose_name=_("Public"))
    document_type = models.ForeignKey(
        "ComplementaryDocumentType",
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Type du document"),
        related_name="complementary_documents",
    )


class ChildrenFormManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(parent__isnull=False)

    def associated_to_parent(self, parent):
        """
        Get the complementary document types associated the parent
        """
        return self.get_queryset().filter(
            parent=parent,
        )


class ComplementaryDocumentType(models.Model):
    name = models.CharField(_("nom"), max_length=255)
    parent = models.ForeignKey(
        "ComplementaryDocumentType",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name=_("Type parent"),
        related_name="children",
    )
    form = models.ForeignKey(
        Form,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name=_("Formulaires"),
        related_name="document_types",
    )
    integrator = models.ForeignKey(
        Group,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Groupe intégrateur"),
        related_name="document_types",
        limit_choices_to={"submission_department__is_integrator_admin": True},
    )

    # reverse relationship is manually defined on reports.Report so it shows up on both sides in admin
    # Note: theoretically, this should only be allowed on "child" ComplementaryDocumentType, but this will
    # be solved by a future refactoring
    # see https://github.com/yverdon/geocity/issues/526
    reports = models.ManyToManyField(
        "reports.Report",
        blank=True,
        related_name="+",
        through="reports.report_document_types",
    )

    objects = models.Manager()

    # Only children objects
    children_objects = ChildrenFormManager()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=(Q(parent__isnull=False) & Q(form__isnull=True))
                | (Q(parent__isnull=True) & Q(form__isnull=False)),
                name="complementary_document_type_restrict_form_link_to_parents",
            )
        ]
        verbose_name = _("3.2 Catégorie de document")
        verbose_name_plural = _("3.2 Catégories de document")

    def __str__(self):
        return self.name


# Change the app_label in order to regroup models under the same app in admin
class ComplementaryDocumentTypeForAdminSite(ComplementaryDocumentType):
    class Meta:
        proxy = True
        app_label = "reports"
        verbose_name = _("3.2 Type de document")
        verbose_name_plural = _("3.2 Type de document")


class SubmissionAmendField(models.Model):
    name = models.CharField(_("nom"), max_length=255)
    api_name = models.CharField(
        _("Nom dans l'API"),
        max_length=255,
        blank=True,
        help_text=_("Se génère automatiquement lorsque celui-ci est vide."),
    )
    is_mandatory = models.BooleanField(_("obligatoire"), default=False)
    is_visible_by_author = models.BooleanField(
        _("Visible par l'auteur de la demande"), default=False
    )
    is_visible_by_validators = models.BooleanField(
        _("Visible par les validateurs"), default=False
    )
    can_always_update = models.BooleanField(
        _("Editable même après classement de la demande"), default=False
    )
    placeholder = models.CharField(
        _("exemple de donnée à saisir"), max_length=255, blank=True
    )
    help_text = models.CharField(
        _("information complémentaire"), max_length=255, blank=True
    )
    regex_pattern = models.CharField(
        _("regex pattern"),
        max_length=255,
        blank=True,
        help_text=_("Exemple: ^[0-9]{4}$"),
    )
    forms = models.ManyToManyField(
        Form,
        verbose_name=_("formulaires"),
        related_name="amend_fields",
    )
    integrator = models.ForeignKey(
        Group,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Groupe intégrateur"),
        limit_choices_to={"submission_department__is_integrator_admin": True},
    )
    api_light = models.BooleanField(
        _("Visible dans l'API light"),
        default=False,
        help_text=_(
            """Lorsque cette case est cochée, ce champ est affichée dans la version light de l'api (/rest/RESSOURCE) si la demande est rendue publique par le coordinateur.<br>
            Afin de ne pas afficher trop d'informations, le champ est masqué pour améliorer la rapidité de l'API.<br>
            Pour afficher la version normale de l'api, il faut se rendre sur une seule ressource (/rest/RESSOURCE/:ID)."""
        ),
    )

    class Meta:
        verbose_name = _("2.1 Champ de traitement des demandes")
        verbose_name_plural = _("2.1 Champs de traitement des demandes")

    def __str__(self):
        return self.name

    def clean(self):
        if self.api_name:
            if self.api_name != convert_string_to_api_key(self.api_name):
                raise ValidationError(
                    {
                        "api_name": _(
                            f"Celui-ci ne peut pas comporter d'espaces ou de caractères spéciaux"
                        )
                    }
                )
        else:
            self.api_name = convert_string_to_api_key(self.name)


class SubmissionAmendFieldValue(models.Model):
    """
    Value of a property for a selected object to be amended by the Pilot.
    """

    field = models.ForeignKey(
        SubmissionAmendField,
        verbose_name=_("caractéristique"),
        on_delete=models.PROTECT,
        related_name="amend_field_value",
    )
    form = models.ForeignKey(
        SelectedForm,
        verbose_name=_("formulaire"),
        on_delete=models.CASCADE,
        related_name="amend_fields",
    )
    value = models.TextField(_("traitement info"), blank=True)
    history = HistoricalRecords()

    class Meta:
        unique_together = [("field", "form")]


class SubmissionValidation(models.Model):
    STATUS_REQUESTED = 0
    STATUS_APPROVED = 1
    STATUS_REJECTED = 2
    STATUS_ASK_FOR_SUPPLEMENTS = 3
    STATUS_CHOICES = (
        (STATUS_REQUESTED, _("En attente")),
        (STATUS_APPROVED, _("Approuvé")),
        (STATUS_REJECTED, _("Refusé")),
    )

    ALL_STATUS_CHOICES = (
        (STATUS_REQUESTED, _("En attente")),
        (STATUS_APPROVED, _("Approuvé")),
        (STATUS_REJECTED, _("Refusé")),
        (STATUS_ASK_FOR_SUPPLEMENTS, _("Demande de compléments")),
    )

    submission = models.ForeignKey(
        PolymorphicSubmission, on_delete=models.CASCADE, related_name="validations"
    )
    department = models.ForeignKey(
        SubmissionDepartment,
        verbose_name=_("Département"),
        on_delete=models.CASCADE,
        related_name="submission_validations",
    )
    validation_status = models.IntegerField(
        _("Statut de validation"), choices=ALL_STATUS_CHOICES, default=STATUS_REQUESTED
    )
    comment = models.TextField(
        _("Commentaire"),
        blank=True,
    )
    comment_is_visible_by_author = models.BooleanField(
        _("Commentaire visible dans le résumé pour l’auteur·e de la demande"),
        default=True,
    )
    comment_is_visible_in_reports = models.BooleanField(
        _("Commentaire visible dans le modèle d’impression"), default=True
    )
    validated_by = models.ForeignKey(
        User,
        verbose_name=_("Validé par"),
        null=True,
        on_delete=models.PROTECT,
    )
    validated_at = models.DateTimeField(_("Validé le"), null=True)
    history = HistoricalRecords()

    class Meta:
        unique_together = ("submission", "department")
        verbose_name = _("3.5 Consultation de la validation par le service")
        verbose_name_plural = _("3.5 Consultation des validations par les services")

    def is_pending(self):
        return self.validation_status == self.STATUS_REQUESTED

    def has_been_answered(self):
        return self.validation_status in [self.STATUS_APPROVED, self.STATUS_REJECTED]

    def should_display_supplement_button(self):
        return self.validation_status == self.STATUS_ASK_FOR_SUPPLEMENTS

    def get_validation_history(self):
        # Actual history for validation based on comment
        actual_validation_history = self.history.all()

        # First history for every comment
        first_history_subquery = (
            actual_validation_history.filter(
                comment=OuterRef("comment"),
                validation_status=OuterRef("validation_status"),
            )
            .order_by("history_date")
            .values("history_date")[:1]
        )

        # Comment based on the history and ordered from newest to oldest
        if actual_validation_history:
            return actual_validation_history.filter(
                history_date=Subquery(first_history_subquery)
            ).order_by("-history_date")
