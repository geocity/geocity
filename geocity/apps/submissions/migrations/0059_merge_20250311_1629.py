# Generated by Django 4.2.16 on 2025-03-11 15:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("submissions", "0058_checklist_integrator"),
        ("submissions", "0058_merge_20250308_0053"),
    ]

    operations = []
