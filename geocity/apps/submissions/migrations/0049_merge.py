from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("submissions", "0042_helptext_for_shortname_field"),
        ("submissions", "0048_reset_id_sequence"),
    ]

    operations = []
