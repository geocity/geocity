import json
import os.path
from datetime import date, datetime, timezone
from gettext import ngettext

from django import template
from django.forms import modelformset_factory
from django.template.defaultfilters import floatformat
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from geocity.apps.submissions import forms, models, permissions
from geocity.apps.submissions.steps import get_geo_step_name_title

register = template.Library()


@register.inclusion_tag("submissions/_submission_progressbar.html", takes_context=True)
def submission_progressbar(context, steps, active_step):
    return (
        {}
        if context["user"].userprofile.is_temporary
        else {
            "steps": steps,
            "active_step": active_step,
        }
    )


@register.filter
def basename(value):
    return os.path.basename(value)


def get_contacts_summary(submission):
    contacts = [
        (
            models.ContactType.objects.get(id=contact["contact_form"].value()).name,
            [
                (field.label, field.value())
                for field in contact
                if field.name not in {"id", "contact_form"}
                and field.value() is not None
            ],
        )
        for contact in forms.get_submission_contacts_formset_initiated(submission)
        if contact["contact_form"].value()
    ]

    return contacts


@register.inclusion_tag("submissions/_submission_summary.html", takes_context=True)
def submission_summary(context, submission):
    (
        forms_infos,
        amend_properties_visible_by_author,
        amend_properties_visible_by_validators,
    ) = forms.get_submission_forms(submission, for_summary=True)
    contacts = get_contacts_summary(submission)
    requires_payment = submission.requires_payment()
    submission_price = submission.get_submission_price()
    is_validator = permissions.has_permission_to_validate_submission(
        context.request.user, submission
    )

    SubmissionGeoTimeFormSet = modelformset_factory(
        models.SubmissionGeoTime,
        form=forms.SubmissionGeoTimeForm,
        extra=0,
    )

    geo_time_formset = SubmissionGeoTimeFormSet(
        None,
        form_kwargs={
            "submission": submission,
            "disable_fields": True,
            # Hide required asterisks next after fields label
            "empty_permitted": True,
        },
        queryset=submission.geo_time.filter(form=None, field=None).all(),
    )

    creditor = ""
    if requires_payment:
        if submission.creditor_type is not None:
            creditor = submission.creditor_type.name
        elif submission.author and submission.creditor_type is None:
            creditor = (
                _("Auteur de la demande, ")
                + submission.author.first_name
                + " "
                + submission.author.last_name
            )

    selected_price = (
        {
            "text": submission_price.text,
            "amount": submission_price.amount,
            "currency": submission_price.currency,
        }
        if submission_price
        else None
    )

    user_is_author_of_submission = context.request.user == submission.author

    display_amend_properties = (
        amend_properties_visible_by_author
        and user_is_author_of_submission
        or amend_properties_visible_by_validators
        and is_validator
    )

    title_step = get_geo_step_name_title(submission.get_geotime_required_info())

    return {
        "user": context.request.user,
        "author": submission.author,
        "creditor": creditor,
        "contacts": contacts,
        "forms_infos": forms_infos,
        "geo_time_formset": geo_time_formset,
        "requires_payment": requires_payment,
        "selected_price": selected_price,
        "is_validator": is_validator,
        "user_is_author_of_submission": user_is_author_of_submission,
        "display_amend_properties": display_amend_properties,
        "geo_step": title_step["step_name"],
    }


@register.filter(name="json")
def json_(value):
    return json.dumps(value)


@register.filter
def human_field_value(value):
    if isinstance(value, bool):
        return _("Oui") if value else _("Non")
    elif isinstance(value, float):
        return floatformat(value, arg=-2)
    elif isinstance(value, date):
        return value.strftime("%d.%m.%Y")
    elif isinstance(value, list):
        return render_to_string("submissions/_field_value_list.html", {"values": value})
    elif not value:
        return "—"

    return value


@register.simple_tag(takes_context=True)
def is_expired(context):
    poly_submission = context["record"]  # ends_at_max
    submission = poly_submission.get_specific()
    if poly_submission.is_submission:
        if submission.max_validity is not None:
            ends_at_max = poly_submission.ends_at_max
            prolongation_date = submission.prolongation_date
            permit_valid_until = (
                prolongation_date if submission.is_prolonged() else ends_at_max
            )
            if permit_valid_until:
                if (
                    submission.is_prolonged()
                    and datetime.now(timezone.utc) < permit_valid_until
                ):
                    return mark_safe(
                        '<span title="%s">%s</span>'
                        % (
                            _("Demande renouvelée"),
                            render_to_string(
                                "_icon.html",
                                {
                                    "id": "refresh",
                                    "class": "icon--75 text-warning-600",
                                    "title": _("Demande renouvelée"),
                                },
                            ),
                        )
                    )
                elif datetime.now(timezone.utc) > permit_valid_until:
                    return mark_safe(
                        '<span title="%s">%s</span>'
                        % (
                            _("Demande échue"),
                            render_to_string(
                                "_icon.html",
                                {
                                    "id": "circle-alert",
                                    "class": "icon--75 text-danger-600",
                                    "title": _("Demande renouvelée"),
                                },
                            ),
                        )
                    )
    return ""


@register.simple_tag(takes_context=True)
def can_always_be_updated(context):
    if context["record"] is not None:
        record = context["record"].get_specific()
        return permissions.can_always_be_updated(context["request"].user, record)


@register.simple_tag(takes_context=True)
def can_download_archive(context):
    return permissions.can_download_archive(context["user"], context["record"])


@register.simple_tag(takes_context=True)
def can_archive_submissions(context):
    return permissions.can_archive_submissions(context["user"])


@register.simple_tag(takes_context=True)
def can_revert_refund_transaction(context):
    return permissions.can_revert_refund_transaction(context["user"], context["record"])


@register.simple_tag(takes_context=True)
def can_refund_transaction(context):
    return permissions.can_refund_transaction(context["user"], context["record"])


@register.simple_tag()
def get_submission_status_style(poly_submission):
    style = {
        models.Submission.STATUS_DRAFT: ("circle-dashed", "submission-status--neutral"),
        models.Submission.STATUS_SUBMITTED_FOR_VALIDATION: (
            "circle-arrow-up",
            "submission-status--pending",
        ),
        models.Submission.STATUS_AWAITING_SUPPLEMENT: (
            "circle-pause",
            "submission-status--pending",
        ),
        models.Submission.STATUS_PROCESSING: (
            "circle-fading-arrow-up",
            "submission-status--pending",
        ),
        models.Submission.STATUS_AWAITING_VALIDATION: (
            "hourglass",
            "submission-status--pending",
        ),
        models.Submission.STATUS_APPROVED: (
            "check",
            "submission-status--positive",
        ),
        models.Submission.STATUS_REJECTED: ("cross", "submission-status--negative"),
        models.Submission.STATUS_RECEIVED: (
            "check",
            "submission-status--positive",
        ),
        models.Submission.STATUS_INQUIRY_IN_PROGRESS: (
            "eye",
            "submission-status--info",
        ),
        models.Submission.STATUS_ARCHIVED: ("archive", "submission-status--neutral"),
    }

    icon = style[poly_submission.status][0]
    classname = style[poly_submission.status][1]

    required_validations = 0
    remaining_validations = 0
    if poly_submission.is_submission:
        required_validations = poly_submission.required_validations
        remaining_validations = poly_submission.remaining_validations
    elif poly_submission.is_permitsubmission:
        required_validations = poly_submission.required_validations_permits
        remaining_validations = poly_submission.remaining_validations_permits

    # Get specific model
    submission = poly_submission.get_specific()
    label = submission.get_status_display
    if required_validations > 0:
        if remaining_validations > 0:
            count = required_validations
            label = ngettext(
                "%(count)d validation en attente",
                "%(remaining)d/%(count)d validations en attente",
                count,
            ) % {"remaining": remaining_validations, "count": count}
            icon = style[models.Submission.STATUS_AWAITING_VALIDATION][0]
            classname = style[models.Submission.STATUS_AWAITING_VALIDATION][1]
        elif (
            submission.status != 2 and submission.status != 4 and submission.status != 6
        ):
            label = _("Préavis donné par les services")
            icon = "message-square-more"
            classname = "submission-status--pending"

    return {"label": label, "icon": icon, "classname": classname}


@register.inclusion_tag("submissions/_directives_and_legal.html", takes_context=True)
def directives_and_legal(context, submission):
    return {
        "directives": submission.get_submission_directives(),
    }


@register.inclusion_tag(
    "submissions/_directives_and_legal_cta.html", takes_context=True
)
def directives_and_legal_cta(context, submission):

    return {
        "directives": submission.get_submission_directives(),
    }
