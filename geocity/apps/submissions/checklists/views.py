from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.views import View

from geocity.apps.permits.forms import ChecklistTasksForm
from geocity.apps.submissions.checklists.models import SubmissionChecklist
from geocity.apps.submissions.views import get_polymorphic_submission_for_edition


class ChecklistUpdateView(View):
    def post(self, request, submission_id, checklist_id, *args, **kwargs):
        poly_submission = get_polymorphic_submission_for_edition(
            request.user, submission_id
        ).get_specific()

        if not poly_submission.can_edit_submission(request.user):
            raise PermissionDenied

        submission_checklist = SubmissionChecklist.objects.get(pk=checklist_id)
        form = ChecklistTasksForm(submission_checklist, data=request.POST)
        if form.is_valid():
            for task in submission_checklist.tasks.all():
                task.completed = str(task.id) in form.cleaned_data["tasks"]
                task.save()
        return HttpResponse(status=204)
