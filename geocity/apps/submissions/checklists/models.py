from django.contrib.auth.models import Group
from django.db import models
from django.utils.translation import gettext_lazy as _


class Checklist(models.Model):
    """Checklist model"""

    name = models.CharField(max_length=255, null=False, blank=False)
    integrator = models.ForeignKey(
        Group,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("Groupe intégrateur"),
        limit_choices_to={"submission_department__is_integrator_admin": True},
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)


class ChecklistTask(models.Model):
    """Checklist Task model"""

    order = models.PositiveIntegerField(default=0)
    description = models.CharField(max_length=255, null=False, blank=False)
    checklist = models.ForeignKey(
        Checklist, related_name="tasks", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"{self.order}"

    class Meta:
        ordering = ("order",)
        verbose_name = _("Tâche")
        verbose_name_plural = _("Tâches")


class SubmissionChecklist(models.Model):
    submission = models.ForeignKey(
        "submissions.PolymorphicSubmission",
        related_name="checklists",
        on_delete=models.CASCADE,
    )
    checklist = models.ForeignKey(
        Checklist, related_name="submission_checklists", on_delete=models.CASCADE
    )


class SubmissionChecklistTask(models.Model):
    submission_checklist = models.ForeignKey(
        SubmissionChecklist, related_name="tasks", on_delete=models.CASCADE
    )
    order = models.PositiveIntegerField()
    description = models.CharField(blank=True, max_length=255)
    completed = models.BooleanField(default=False)

    class Meta:
        ordering = ("order",)


class ChecklistForAdminSite(Checklist):
    class Meta:
        proxy = True
        app_label = "forms"
        verbose_name = _("1.10 Checklist")
        verbose_name_plural = _("1.10 Checklists")
