from adminsortable2.admin import (
    SortableAdminBase,
    SortableAdminMixin,
    SortableTabularInline,
)
from django import forms
from django.contrib import admin
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from geocity.apps.accounts.admin import IntegratorFilterMixin, filter_for_user
from geocity.apps.forms.admin import get_forms_field
from geocity.apps.forms.models import Form

from ..accounts.models import AdministrativeEntity
from ..permits.steps import StepChecklist
from . import models
from .checklists.models import ChecklistForAdminSite, ChecklistTask


class FormCategoryFilter(admin.SimpleListFilter):
    """
    Filter the select options for the current integrator
    From https://docs.djangoproject.com/en/5.1/ref/contrib/admin/filters/#using-a-simplelistfilter
    """

    title = _("Catégorie de formulaire")
    parameter_name = "formcategory_name"

    def lookups(self, request, model_admin):
        """
        Filter items not belonging to the current integrator
        """
        qs = filter_for_user(
            request.user, models.FormCategory.objects.all().order_by("name")
        ).values_list("name", "name")
        return qs

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value():
            queryset = queryset.filter(form_category__name=self.value())

        return queryset


class SubmissionAmendFieldForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super().__init__(*args, **kwargs)
        self.fields["forms"] = get_forms_field(user)

    class Meta:
        model = models.SubmissionAmendField
        fields = [
            "name",
            "is_mandatory",
            "is_visible_by_author",
            "is_visible_by_validators",
            "can_always_update",
            "placeholder",
            "help_text",
            "regex_pattern",
            "forms",
            "integrator",
            "api_name",
            "api_light",
        ]


@admin.register(models.SubmissionAmendField)
class SubmissionAmendFieldAdmin(IntegratorFilterMixin, admin.ModelAdmin):
    list_display = [
        "sortable_str",
        "is_mandatory",
        "is_visible_by_author",
        "is_visible_by_validators",
        "can_always_update",
    ]
    search_fields = [
        "name",
    ]
    form = SubmissionAmendFieldForm
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "is_mandatory",
                    "is_visible_by_author",
                    "is_visible_by_validators",
                    "can_always_update",
                    "placeholder",
                    "help_text",
                    "regex_pattern",
                    "forms",
                    "integrator",
                )
            },
        ),
        (
            _("API"),
            {
                "fields": (
                    "api_name",
                    "api_light",
                ),
            },
        ),
    )

    def sortable_str(self, obj):
        return str(obj)

    sortable_str.short_description = "Champ de traitement des demandes"
    sortable_str.admin_order_field = "name"

    # Pass the user from ModelAdmin to ModelForm
    def get_form(self, request, obj=None, **kwargs):
        Form = super().get_form(request, obj, **kwargs)

        class RequestForm(Form):
            def __new__(cls, *args, **kwargs):
                kwargs["user"] = request.user
                return Form(*args, **kwargs)

        return RequestForm


@admin.register(models.ContactTypeForAdminSite)
class ContactTypeAdmin(IntegratorFilterMixin, admin.ModelAdmin):
    list_display = [
        "name",
    ]
    search_fields = [
        "name",
    ]

    def sortable_str(self, obj):
        return obj.__str__()

    sortable_str.admin_order_field = "name"
    sortable_str.short_description = _("Types")


@admin.register(models.ContactFormForAdminSite)
class ContactFormAdmin(SortableAdminMixin, IntegratorFilterMixin, admin.ModelAdmin):
    list_display = [
        "sortable_str",
        "type",
        "form_category",
        "is_mandatory",
        "is_dynamic",
    ]
    list_filter = [
        FormCategoryFilter,
        "is_mandatory",
    ]
    search_fields = [
        "name",
    ]

    def sortable_str(self, obj):
        return obj.__str__()

    sortable_str.admin_order_field = "type"
    sortable_str.short_description = _("Contact")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "form_category":
            kwargs["queryset"] = filter_for_user(
                request.user, models.FormCategory.objects.all().order_by("name")
            )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(models.Submission)
class SubmissionAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "created_at",
        "sent_date",
        "status",
        "author",
        "get_forms",
        "administrative_entity",
    ]
    search_fields = [
        "id",
        "author__first_name",
        "author__last_name",
    ]
    list_filter = ("status", "author", "forms", "administrative_entity")

    def get_queryset(self, request):
        return (
            super()
            .get_queryset(request)
            .prefetch_related("forms")
            .select_related("author")
        )

    def has_add_permission(self, request):
        return False

    def get_forms(self, obj):
        return ", ".join(sorted([form.name for form in obj.forms.all()]))

    get_forms.admin_order_field = "forms"
    get_forms.short_description = "Formulaires"


class ComplementaryDocumentTypeAdminForm(forms.ModelForm):
    model = models.ComplementaryDocumentTypeForAdminSite

    def clean_form(self):
        form = self.cleaned_data["form"]
        if not self.instance.pk:
            return form
        payment_settings_confirmation_reports = self.instance.children.exclude(
            reports__confirmation_payment_settings_objects=None
        )
        payment_settings_refund_reports = self.instance.children.exclude(
            reports__refund_payment_settings_objects=None
        )
        error_msg = ""
        if (
            payment_settings_confirmation_reports.exists()
            and not payment_settings_confirmation_reports.filter(
                reports__confirmation_payment_settings_objects__form__in=[form]
            )
        ):
            error_msg = _(
                "Ce type de document est utilisé comme confirmation de paiement dans une configuration de paiement, via un modèle d'impression. Vous devez dé-lier le modèle d'impression de la configuration de paiement afin de pouvoir modifier ce champ."
            )
        if (
            payment_settings_refund_reports.exists()
            and not payment_settings_refund_reports.filter(
                reports__refund_payment_settings_objects__form__in=[form]
            )
        ):
            error_msg = _(
                "Ce type de document est utilisé comme remboursement dans une configuration de paiement, via un modèle d'impression. Vous devez dé-lier le modèle d'impression de la configuration de paiement afin de pouvoir modifier ce champ."
            )
        if error_msg:
            raise forms.ValidationError(error_msg)
        return form


class ComplementaryDocumentTypeInline(admin.TabularInline):
    model = models.ComplementaryDocumentTypeForAdminSite
    form = ComplementaryDocumentTypeAdminForm

    fields = ["name"]

    verbose_name = _("Type de document")
    verbose_name_plural = _("Type de documents")

    # Defines the number of extra forms to by default. Default is set to 3
    # https://docs.djangoproject.com/en/4.1/ref/contrib/admin/#django.contrib.admin.InlineModelAdmin.extra
    extra = 1

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(parent__isnull=False)


@admin.register(models.ComplementaryDocumentTypeForAdminSite)
class ComplementaryDocumentTypeAdmin(IntegratorFilterMixin, admin.ModelAdmin):
    inlines = [
        ComplementaryDocumentTypeInline,
    ]
    form = ComplementaryDocumentTypeAdminForm
    fields = [
        "name",
        "form",
        "integrator",
    ]

    def get_list_display(self, request):
        if request.user.is_superuser:
            list_display = [
                "name",
                "form",
                "integrator",
                "types_",
            ]
        else:
            list_display = [
                "name",
                "form",
                "types_",
            ]
        return list_display

    # Fields used in search_fields and list_filter
    superuser_search_fields = [
        "name",
        "form__name",
        "integrator__name",
    ]
    integrator_search_fields = [
        "name",
        "form__name",
    ]

    superuser_list_search_fields = [
        "name",
        "form",
        "integrator",
    ]

    integrator_list_search_fields = [
        "name",
        "form",
    ]

    def get_search_fields(self, request):
        if request.user.is_superuser:
            search_fields = self.superuser_search_fields
        else:
            search_fields = self.integrator_search_fields
        return search_fields

    def get_list_filter(self, request):
        if request.user.is_superuser:
            list_filter = self.superuser_list_search_fields
        else:
            list_filter = self.integrator_list_search_fields
        return list_filter

    # List types of documents
    def types_(self, obj):
        list_content = format_html_join(
            "",
            "<li>{}</li>",
            [
                [d]
                for d in models.ComplementaryDocumentType.children_objects.associated_to_parent(
                    obj
                ).values_list(
                    "name", flat=True
                )
            ],
        )
        return mark_safe(f"<ul>{list_content}</ul>")

    types_.short_description = _("Type de document")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "form":
            if request.user.is_superuser:
                kwargs["queryset"] = Form.objects.all().order_by("name")
            else:
                kwargs["queryset"] = Form.objects.filter(
                    integrator=request.user.groups.get(
                        submission_department__is_integrator_admin=True
                    )
                ).order_by("name")
        if db_field.name == "parent":
            if request.user.is_superuser:
                kwargs[
                    "queryset"
                ] = models.ComplementaryDocumentType.objects.all().order_by("name")
            else:
                kwargs["queryset"] = models.ComplementaryDocumentType.objects.filter(
                    integrator=request.user.groups.get(
                        submission_department__is_integrator_admin=True
                    )
                ).order_by("name")

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(parent__isnull=True)


@admin.register(models.SubmissionInquiry)
class SubmissionInquiryAdmin(admin.ModelAdmin):
    list_display = ("id", "start_date", "end_date", "submitter", "submission")

    def sortable_str(self, obj):
        return obj.__str__()

    sortable_str.admin_order_field = "name"
    sortable_str.short_description = _("2.3 Enquêtes publiques")


class ChecklistTaskInline(SortableTabularInline):
    model = ChecklistTask
    min_num = 1
    extra = 1

    fields = [
        "description",
    ]

    formfield_overrides = {
        models.CharField: {
            "widget": forms.Textarea(
                attrs={"class": "vLargeTextField", "rows": "2", "cols": "65"}
            )
        },
    }

    class Media:
        css = {"all": ("css/admin/admin.css",)}


class StepChecklistInline(admin.TabularInline):
    model = StepChecklist

    fields = ["administrative_entity", "step", "permit_submission_type"]

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        """enable ordering drop-down alphabetically"""
        if db_field.name == "administrative_entity":
            if not request.user.is_superuser:
                kwargs["queryset"] = AdministrativeEntity.objects.associated_to_user(
                    request.user
                )
            else:
                kwargs["queryset"] = AdministrativeEntity.objects.all().order_by("name")
        return super(StepChecklistInline, self).formfield_for_foreignkey(
            db_field, request, **kwargs
        )

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        field = super().formfield_for_dbfield(db_field, request, **kwargs)
        if db_field.name == "permit_submission_type":
            field.choices[0] = ("", _("Tous les types de permis"))
        return field


@admin.register(ChecklistForAdminSite)
class ChecklistAdmin(SortableAdminBase, IntegratorFilterMixin, admin.ModelAdmin):
    list_display = (
        "name",
        "step_checklist_step",
        "step_checklist_type",
        "step_checklist_entity",
    )
    inlines = [ChecklistTaskInline, StepChecklistInline]
    fields = ["name", "integrator"]

    def step_checklist_step(self, obj):
        return obj.step_checklist.get_step_display()

    def step_checklist_type(self, obj):
        return obj.step_checklist.get_permit_submission_type_display()

    def step_checklist_entity(self, obj):
        return str(obj.step_checklist.administrative_entity)

    step_checklist_entity.short_description = _("Entité administrative")
    step_checklist_step.short_description = _("Étape")
    step_checklist_type.short_description = _("Type de permis")

    step_checklist_entity.admin_order_field = "step_checklist__administrative_entity"
    step_checklist_step.admin_order_field = "step_checklist__step"
    step_checklist_type.admin_order_field = "step_checklist__permit_submission_type"
