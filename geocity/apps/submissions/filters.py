import django_filters
from django.db.models import Max, Min
from django.forms import DateInput
from django.utils.translation import gettext_lazy as _
from django_select2.forms import Select2Widget

from geocity.apps.accounts.models import AdministrativeEntity
from geocity.apps.forms.models import Form, FormCategory

from . import models


def _get_administrative_entities_queryset_for_current_user(request):
    return (
        AdministrativeEntity.objects.filter(
            submissions__in=models.Submission.objects.filter_for_user(request.user)
        )
        .distinct()
        .order_by("name")
    )


def _get_form_categories_queryset_for_current_user(request):
    return (
        FormCategory.objects.filter(
            forms__submissions__in=models.Submission.objects.filter_for_user(
                request.user
            )
        )
        .distinct()
        .order_by("name")
    )


def _get_forms_queryset_for_current_user(request):
    return (
        Form.objects.filter(
            submissions__in=models.Submission.objects.filter_for_user(request.user)
        )
        .distinct()
        .order_by("name")
    )


class BaseSubmissionFilterSet(django_filters.FilterSet):
    id = django_filters.filters.NumberFilter(field_name="id", label=_("N°"))

    status = django_filters.filters.ChoiceFilter(
        label=_("État"),
        widget=Select2Widget(),
    )
    forms__administrative_entities = django_filters.filters.ModelChoiceFilter(
        queryset=_get_administrative_entities_queryset_for_current_user,
        label=_("Entité administrative"),
        widget=Select2Widget(),
    )
    forms__category = django_filters.filters.ModelChoiceFilter(
        queryset=_get_form_categories_queryset_for_current_user,
        label=_("Catégorie"),
        widget=Select2Widget(),
    )
    forms = django_filters.filters.ModelChoiceFilter(
        queryset=_get_forms_queryset_for_current_user,
        label=_("Formulaire"),
        widget=Select2Widget(),
    )
    created_at = django_filters.DateFilter(
        field_name="created_at",
        lookup_expr="date",
        label=_("Date de création"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )
    sent_date = django_filters.DateFilter(
        field_name="sent_date",
        lookup_expr="date",
        label=_("Date d'envoi"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )
    starts_at_min = django_filters.DateFilter(
        field_name="starts_at_min",
        lookup_expr="gte",
        label=_("Début après le"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )
    ends_at_max = django_filters.DateFilter(
        field_name="ends_at_max",
        lookup_expr="lte",
        label=_("Fin avant le"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        status_dict = dict(models.Submission.STATUS_CHOICES)
        available_statuses = (
            models.Submission.objects.filter_for_user(self.request.user)
            .values("status")
            .distinct()
        )

        status_choices = [
            (status["status"], status_dict.get(status["status"]))
            for status in available_statuses
        ]

        status_choices = sorted(status_choices, key=lambda x: x[1])
        self.filters["status"].extra["choices"] = status_choices

    class Meta:
        queryset = models.Submission.objects.annotate(
            starts_at_min=Min("geo_time__starts_at"),
            ends_at_max=Max("geo_time__ends_at"),
        )
        fields = [
            "id",
            "status",
            "administrative_entity",
            "forms__category",
        ]


class OwnSubmissionFilterSet(BaseSubmissionFilterSet):
    pass


class DepartmentSubmissionFilterSet(BaseSubmissionFilterSet):
    author__last_name = django_filters.filters.CharFilter(
        lookup_expr="icontains",
        label=_("Auteur·e de la demande"),
    )
    shortname = django_filters.filters.CharFilter(
        lookup_expr="icontains",
        label=_("Identifiant"),
    )

    class Meta:
        model = models.Submission
        fields = [
            "id",
            "shortname",
            "status",
            "forms__administrative_entities",
            "forms__category",
            "forms",
            "created_at",
            "sent_date",
            "starts_at_min",
            "ends_at_max",
            "author__last_name",
        ]
