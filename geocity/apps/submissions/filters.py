import django_filters
from django.db.models import Max, Min
from django.forms import DateInput
from django.utils.translation import gettext_lazy as _
from django_filters.constants import EMPTY_VALUES
from django_select2.forms import Select2Widget
from unidecode import unidecode

from geocity.apps.accounts.models import AdministrativeEntity
from geocity.apps.forms.models import Form, FormCategory

from ..permits.models import PermitSubmission
from . import models
from .models import PolymorphicSubmission


def _get_administrative_entities_queryset_for_current_user(request):
    return (
        AdministrativeEntity.objects.filter(
            submissions__in=models.PolymorphicSubmission.objects.filter_for_user(
                request.user
            )
        )
        .distinct()
        .order_by("name")
    )


def _get_form_categories_queryset_for_current_user(request):
    return (
        FormCategory.objects.filter(
            forms__submissions__in=models.Submission.objects.filter_for_user(
                request.user
            )
        )
        .distinct()
        .order_by("name")
    )


def _get_forms_queryset_for_current_user(request):
    return (
        Form.objects.filter(
            submissions__in=models.Submission.objects.filter_for_user(request.user)
        )
        .distinct()
        .order_by("name")
    )


class SubmissionHybridChoiceFilter(
    django_filters.filters.QuerySetRequestMixin, django_filters.filters.ChoiceFilter
):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_choices(self, request):
        raise NotImplementedError

    def get_queryset(self, request):
        return None

    @property
    def field(self):
        request = self.get_request()
        choices = self.get_choices(request)
        self.extra["choices"] = choices
        if not hasattr(self, "_field"):
            field_kwargs = self.extra.copy()
            self._field = self.field_class(label=self.label, **field_kwargs)
        return self._field


class SubmissionCategoryFilter(SubmissionHybridChoiceFilter):
    """
    Hybrid polymorphic filter "Type de demande"
    Submission -> submission__forms__categories (for current user)
    PermitSubmission -> has attr permitsubmission  # FIXME Comment
    """

    def get_choices(self, request):
        category_choices = {
            unidecode(category.name.lower()): (category.id, category.name)
            for category in _get_form_categories_queryset_for_current_user(
                request
            ).all()
        }

        # Add "Permis de construire" if the list contains at least one
        if (
            PolymorphicSubmission.objects.instance_of(PermitSubmission)
            .filter_for_user(request.user)
            .exists()
        ):
            category_choices["permis de construire"] = (
                "permit",
                _("Permis de construire"),
            )

        sorted_choices = dict(sorted(category_choices.items()))
        return sorted_choices.values()

    def filter(self, qs, value):
        if value != self.null_value:
            if value == "permit":
                qs = self.get_method(qs)(permitsubmission__isnull=False)
            elif value not in EMPTY_VALUES:
                qs = self.get_method(qs)(
                    submission__isnull=False, submission__forms__category=value
                )
            if self.distinct:
                qs = qs.distinct()
            return qs

        # Null value
        qs = self.get_method(qs)(
            submission__isnull=False, submission__forms__category=None
        )
        return qs.distinct() if self.distinct else qs


class SubmissionObjectsFilter(SubmissionHybridChoiceFilter):
    def get_choices(self, request):
        choices = {
            unidecode(category.name.lower()): (category.id, category.name)
            for category in _get_forms_queryset_for_current_user(request).all()
        }

        # PermitSubmission options
        # FIXME: Make PermitSubmissionTypes a global settings to avoid app dependency
        from geocity.apps.permits.models import PermitSubmissionTypes

        p_submissions = PolymorphicSubmission.objects.instance_of(
            PermitSubmission
        ).filter_for_user(request.user)
        for value, label in PermitSubmissionTypes.choices:
            if p_submissions.filter(permitsubmission__submission_type=value).exists():
                choices[unidecode(label.lower())] = (value, label)

        sorted_choices = dict(sorted(choices.items()))
        return sorted_choices.values()

    def filter(self, qs, value):
        if value != self.null_value:
            from geocity.apps.permits.models import PermitSubmissionTypes

            if value in PermitSubmissionTypes.values:
                qs = self.get_method(qs)(
                    permitsubmission__isnull=False,
                    permitsubmission__submission_type=value,
                )
            elif value not in EMPTY_VALUES:
                qs = self.get_method(qs)(
                    submission__isnull=False, submission__forms=value
                )
            if self.distinct:
                qs = qs.distinct()
            return qs

        # Null value
        qs = self.get_method(qs)(submission__isnull=False, submission__forms=None)
        return qs.distinct() if self.distinct else qs


class BaseSubmissionFilterSet(django_filters.FilterSet):
    id = django_filters.filters.NumberFilter(field_name="id", label=_("N°"))

    status = django_filters.filters.ChoiceFilter(
        label=_("État"),
        widget=Select2Widget(),
    )
    administrative_entity = django_filters.filters.ModelChoiceFilter(
        queryset=_get_administrative_entities_queryset_for_current_user,
        label=_("Entité administrative"),
        widget=Select2Widget(),
    )
    category = SubmissionCategoryFilter(
        label=_("Catégorie"),
        widget=Select2Widget(),
    )
    forms = SubmissionObjectsFilter(
        label=_("Formulaire"),
        widget=Select2Widget(),
    )
    created_at = django_filters.DateFilter(
        field_name="created_at",
        lookup_expr="date",
        label=_("Date de création"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )
    sent_date = django_filters.DateFilter(
        field_name="sent_date",
        lookup_expr="date",
        label=_("Date d'envoi"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )
    starts_at_min = django_filters.DateFilter(
        field_name="starts_at_min",
        lookup_expr="gte",
        label=_("Début après le"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )
    ends_at_max = django_filters.DateFilter(
        field_name="ends_at_max",
        lookup_expr="lte",
        label=_("Fin avant le"),
        widget=DateInput(
            attrs={"type": "date"},
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        status_dict = dict(models.Submission.STATUS_CHOICES)
        available_statuses = (
            models.Submission.objects.filter_for_user(self.request.user)
            .values("status")
            .distinct()
        )

        status_choices = [
            (status["status"], status_dict.get(status["status"]))
            for status in available_statuses
        ]

        status_choices = sorted(status_choices, key=lambda x: x[1])
        self.filters["status"].extra["choices"] = status_choices

    class Meta:
        queryset = models.PolymorphicSubmission.objects.annotate(
            starts_at_min=Min("submission__geo_time__starts_at"),
            ends_at_max=Max("submission__geo_time__ends_at"),
        )
        fields = ["id", "status", "administrative_entity", "category", "forms"]


class OwnSubmissionFilterSet(BaseSubmissionFilterSet):
    pass


class DepartmentSubmissionFilterSet(BaseSubmissionFilterSet):
    requester = django_filters.filters.CharFilter(
        lookup_expr="icontains",
        label=_("Requérant·e"),
    )
    shortname = django_filters.filters.CharFilter(
        lookup_expr="icontains",
        label=_("Identifiant"),
    )

    class Meta:
        model = models.PolymorphicSubmission
        fields = [
            "id",
            "shortname",
            "status",
            "category",
            "forms",
            "sent_date",
            "starts_at_min",
            "ends_at_max",
            "requester",
        ]
