import django_tables2 as tables
from django.utils.translation import gettext_lazy as _

from geocity.apps.permits.models import PermitSubmissionDocument


class PermitSubmissionDocumentsTable(tables.Table):
    description = tables.Column(verbose_name=_("Description"), orderable=False)
    created_at = tables.DateTimeColumn(
        format="d.m.Y H:i", verbose_name=_("Date d'envoi"), orderable=False
    )
    actions = tables.TemplateColumn(
        template_name="permits/tables/_permit_submission_document_table_actions.html",
        verbose_name="",
        orderable=False,
        attrs={"th": {"class": "table-compact-column"}},
    )

    class Meta:
        model = PermitSubmissionDocument
        fields = ("description", "created_at")
