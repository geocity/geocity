from geocity.apps.permits.models import PERMIT_EAST_MIN_COORD, PERMIT_NORTH_MIN_COORD

east_coord_example = PERMIT_EAST_MIN_COORD + 1
north_coord_example = PERMIT_NORTH_MIN_COORD + 1


def base_submission_fields():
    return {
        "submission_type": ["public_enquiry"],
        "camac_number": ["233826"],
        "shortname": ["DemDupont"],
        "object": ["Démolition de la ferme de la famille Dupont"],
        "competency": ["m"],
        "nature": ["demolition_partielle"],
        "transformation_type": [""],
        "work_type": ["0"],
        "construction_type": ["building"],
        "zone_name": ["g"],
        "zone_plan": ["pga"],
        "project_owner_type": ["1"],
        "project_owner-first_name": ["Jean"],
        "project_owner-last_name": ["Luc"],
        "project_owner-company_name": [""],
        "project_owner-address": ["Avenue Jomini 1"],
        "project_owner-email": ["test@example.com"],
        "project_owner-phone": ["0211234567"],
        "project_owner-zipcode": ["1243"],
        "project_owner-city": ["Genève"],
        "project_owner-country": ["CH"],
        "owner-is_owner_same_as_project_owner": ["on"],
        "parcel_numbers": ["123,456"],
        "eca_number": ["123"],
        "east_coordinate": [east_coord_example],
        "north_coordinate": [north_coord_example],
        "is_outside_building_zone": ["True"],
        "is_parcel_outside_building_zone": ["False"],
        "is_work_outside_building_zone": ["True"],
    }


def base_editable_submission_fields():
    return {
        "camac_number": ["233826"],
        "shortname": ["DemDupont"],
        "object": ["Démolition de la ferme de la famille Dupont"],
        "competency": ["m"],
        "nature": ["demolition_partielle"],
        "transformation_type": [""],
        "work_type": ["0"],
        "construction_type": ["building"],
        "zone_name": ["g"],
        "zone_plan": ["pga"],
        "parcel_numbers": ["123,456"],
        "eca_number": ["123"],
        "east_coordinate": [east_coord_example],
        "north_coordinate": [north_coord_example],
        "is_outside_building_zone": ["True"],
        "is_parcel_outside_building_zone": ["False"],
        "is_work_outside_building_zone": ["True"],
    }
