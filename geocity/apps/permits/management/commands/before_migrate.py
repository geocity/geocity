from django.core.management import BaseCommand
from django.db import connection, transaction

# Before running any migrations in this app, we need to ensure that the old
# migrations that were part of the old "permits" app are renamed to old_permits
# to avoid a migration conflict.


def rename_old_permits_migrations():

    sql = """
    UPDATE django_migrations
    SET app = 'old_permits'
    WHERE app = 'permits' AND applied < '2024-10-28 00:00:00';
    """
    with transaction.atomic():
        with connection.cursor() as cursor:
            cursor.execute(sql)


class Command(BaseCommand):
    help = "Runs before migrate"

    def handle(self, *args, **options):
        rename_old_permits_migrations()
