from geocity.apps.accounts.models import AdministrativeEntity
from geocity.apps.submissions.permissions import is_backoffice_of_entity


def has_permission_to_create_permit_submissions_for_entity(user, administrative_entity):
    return (
        user.has_perm("permits.add_permitsubmission")
        and administrative_entity
        in AdministrativeEntity.objects.associated_to_user(user)
        and is_backoffice_of_entity(user, administrative_entity)
    )


def has_permission_to_edit_permit_submissions_for_entity(user, administrative_entity):
    return (
        user.has_perm("permits.change_permitsubmission")
        and administrative_entity
        in AdministrativeEntity.objects.associated_to_user(user)
        and is_backoffice_of_entity(user, administrative_entity)
    )


def has_permission_to_edit_permit_submission(user, permit_submission):
    return has_permission_to_edit_permit_submissions_for_entity(
        user, permit_submission.administrative_entity
    )


def has_permission_to_delete_permit_submissions_for_entity(user, administrative_entity):
    return (
        user.has_perm("permits.delete_permitsubmission")
        and administrative_entity
        in AdministrativeEntity.objects.associated_to_user(user)
        and is_backoffice_of_entity(user, administrative_entity)
    )


def has_permission_to_view_permit_submissions_for_entity(user, administrative_entity):
    return user.has_perm(
        "permits.view_permitsubmission"
    ) and administrative_entity in AdministrativeEntity.objects.associated_to_user(user)
