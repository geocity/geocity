from django.conf import settings
from django.db import transaction
from django.http import Http404
from django.shortcuts import get_object_or_404

from geocity.apps.accounts.models import AdministrativeEntity
from geocity.apps.permits.models import PermitContact, PermitSubmission
from geocity.apps.permits.permissions import (
    has_permission_to_create_permit_submissions_for_entity,
    has_permission_to_edit_permit_submissions_for_entity,
    has_permission_to_view_permit_submissions_for_entity,
)
from geocity.apps.permits.steps import StepChecklist
from geocity.apps.submissions import steps
from geocity.apps.submissions.checklists.models import (
    SubmissionChecklist,
    SubmissionChecklistTask,
)
from geocity.apps.submissions.steps import get_selectable_entities


def get_selectable_entities(user, site, entity_tags=None, action=None):
    if entity_tags is None:
        entity_tags = []
    if user.is_authenticated and settings.IS_PERMITS_ENABLED:
        entities = steps.get_selectable_entities(
            user=user,
            current_site=site,
            submission=None,
            entity_tags=entity_tags,
        )
        entities = entities.filter(is_permits_enabled=True)

        for entity in entities.all():
            if action == "create":
                has_permission = has_permission_to_create_permit_submissions_for_entity(
                    user, entity
                )
            elif action == "edit":
                has_permission = has_permission_to_edit_permit_submissions_for_entity(
                    user, entity
                )
            else:
                raise ValueError("action must be 'create' or 'edit'")
            if not has_permission:
                entities = entities.exclude(pk=entity.pk)

        return entities
    return AdministrativeEntity.objects.none()


def is_permits_enabled_for_user(user, site, entity_tags=None):
    return get_selectable_entities(user, site, entity_tags, "create").exists()


def find_form_by_field_name(forms_by_section, field_name):
    for section, form in forms_by_section.items():
        if field_name in form.fields:
            return section
    return None


def get_default_value_for_permit_submission(field_name):
    return getattr(PermitSubmission(), field_name)


def clear_dependent_fields(submission):
    # Clear values for fields that depend on other fields
    for field in submission._meta.fields:
        if hasattr(field, "parental_field") and hasattr(
            field, "parental_expected_values"
        ):
            value = getattr(submission, field.parental_field)
            if field.parental_expected_values:
                if value not in field.parental_expected_values:
                    setattr(
                        submission,
                        field.name,
                        get_default_value_for_permit_submission(field.name),
                    )
    return submission


def edit_permit_submission(author, submission, data):
    with transaction.atomic():
        data["author"] = author
        for attr, value in data.items():
            setattr(submission, attr, value)

        submission = clear_dependent_fields(submission)
        submission.save()
        return submission


def create_permit_submission(author, data, contacts_cleaned_data):
    with transaction.atomic():
        project_owner = PermitContact.objects.create(
            **contacts_cleaned_data["project_owner"]
        )
        data["project_owner"] = project_owner
        if contacts_cleaned_data["owner"].pop("is_owner_same_as_project_owner", False):
            owner = project_owner
        else:
            owner = PermitContact.objects.create(**contacts_cleaned_data["owner"])

        data["owner"] = owner
        data["author"] = author

        submission = PermitSubmission(**data)
        submission = clear_dependent_fields(submission)
        submission.save()

        for step_checklist in StepChecklist.objects.all():
            if step_checklist.administrative_entity == submission.administrative_entity:
                if (
                    step_checklist.permit_submission_type
                    and step_checklist.permit_submission_type
                    != submission.submission_type
                ):
                    continue
                submission_checklist = SubmissionChecklist.objects.create(
                    submission=submission, checklist=step_checklist.checklist
                )
                for task in step_checklist.checklist.tasks.all():
                    SubmissionChecklistTask.objects.create(
                        submission_checklist=submission_checklist,
                        order=task.order,
                        description=task.description,
                    )

        return submission


def save_permit_submission(author, data, instance, contacts_cleaned_data):
    if instance:
        return edit_permit_submission(author, submission=instance, data=data)
    else:
        return create_permit_submission(author, data, contacts_cleaned_data)


def get_permit_submission_for_user_or_404(user, pk, action):
    permit_submission = get_object_or_404(PermitSubmission, pk=pk)

    if action == "create":
        permission_func = has_permission_to_create_permit_submissions_for_entity
    elif action == "edit":
        permission_func = has_permission_to_edit_permit_submissions_for_entity
    elif action == "view":
        permission_func = has_permission_to_view_permit_submissions_for_entity
    else:
        raise ValueError("action must be 'create', 'edit' or 'view'")

    if permission_func(user, permit_submission.administrative_entity):
        return permit_submission
    raise Http404
