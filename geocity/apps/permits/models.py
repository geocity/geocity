from itertools import chain

from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from geocity.apps.accounts.models import SubmissionDepartment
from geocity.apps.permits.fields import (
    ConditionalBooleanField,
    ConditionalCharField,
    ConditionalChoicesCharField,
    ConditionalIntegerField,
    EmptyBlankChoiceCharField,
    EmptyBlankChoiceSmallIntegerField,
    PermitSubmissionDocumentFileField,
)
from geocity.apps.permits.permissions import (
    has_permission_to_delete_permit_submissions_for_entity,
    has_permission_to_edit_permit_submissions_for_entity,
)
from geocity.apps.permits.validators import parent_field_validator
from geocity.apps.submissions.models import (
    BaseContact,
    BaseSubmissionDocument,
    PolymorphicSubmission,
)

WORK_TYPES = {
    "Agriculture, sylviculture": (
        (0, "Améliorations foncières"),
        (1, "Construction agricole"),
        (2, "Construction sylvicole"),
    ),
    "Approvisionnement en eau / énergie": (
        (3, "Alimentation en eau"),
        (4, "Chauffage à distance"),
        (5, "Installations d'évacuation et de traitement des eaux usées"),
        (6, "Usines à gaz, réseaux et installations chimiques"),
        (7, "Usines d'électricité et réseaux"),
        (8, "Autre"),
    ),
    "Autre transport et communication": (
        (9, "Constructions pour chemins de fer (yc. gare)"),
        (10, "Constructions pour la navigation"),
        (11, "Constructions pour les bus et tramway"),
        (12, "Constructions pour les communications (yc. antenne téléphonie)"),
        (13, "Constructions pour les transports aériens"),
        (14, "Autres constructions vouées aux transports"),
    ),
    "Autres infrastructures": (
        (15, "Aménagement de berges et barrages"),
        (16, "Bâtiment à plusieurs logements pour l'habitation exclusivement"),
        (17, "Bâtiment à usage mixte, principalement à usage d'habitation"),
        (18, "Constructions pour la défense nationale"),
        (19, "Foyer sans soins médicaux et/ou assistance sociale"),
        (20, "Garage, place de parc en rapport avec l'habitation"),
        (21, "Maison individuelle à un logement, isolée"),
        (22, "Maison individuelle à un logement, mitoyenne ou jumelle"),
        (23, "Autre construction en rapport avec l'habitation"),
        (24, "Autres infrastructures"),
    ),
    "Education, recherche, santé, loisirs, culture": (
        (25, "Bâtiments à but culturel, musées, bibliothèques et monuments"),
        (26, "Ecole, système d'éducation (jusqu'au niveau maturité)"),
        (27, "Eglise et bâtiment à but religieux"),
        (28, "Formation supérieure ou recherche"),
        (29, "Foyer avec soins médicaux et/ou assistance sociale"),
        (30, "Hôpital"),
        (31, "Installation de loisirs et de tourisme"),
        (32, "Salles omnisports et salles de sport"),
        (33, "Autre établissement de santé spécialisé"),
    ),
    "Elimination des déchets": (
        (34, "Ordures ménagères"),
        (35, "Autres déchets"),
    ),
    "Industrie, artisanat, commerce, services, administration": (
        (36, "Autres hébergements de courte durée"),
        (37, "Bât. administratif, bureaux"),
        (38, "Bât. commercial, magasin"),
        (
            39,
            "Etablissements tels que hôtel, café-restaurant, café-bar, tea-room, etc...",
        ),
        (40, "Fabrique, usine, atelier"),
        (41, "Halle, dépôt, silo, citerne"),
        (42, "Autre construction destinée à des activités économiques"),
    ),
    "Infrastructure routière, parking": (
        (43, "Parking couvert"),
        (44, "Route, place de stationnement"),
        (45, "Routes cantonales"),
        (46, "Routes communales"),
        (47, "Routes nationales"),
    ),
}

WORK_TYPES_CHOICES = [
    (category, [(value, label) for value, label in options])
    for category, options in WORK_TYPES.items()
]

# Maître d'ouvrage
PROJECT_OWNER_TYPES = (
    (0, "Banque, fonds immobilier, holding financière"),
    (1, "Caisse-maladie, SUVA"),
    (2, "Canton"),
    (3, "CFF"),
    (4, "Chemin de fer privé"),
    (5, "Commune"),
    (6, "Coopérative de logement"),
    (
        7,
        "Département fédéral de la défense, de la protection de la population et des sports",
    ),
    (8, "Entreprise de droit public d'un canton"),
    (
        9,
        "Entreprise de droit public de la commune (transports publics, gaz, eau, électricité, etc.)",
    ),
    (10, "Institution de prévoyance, caisse de pension"),
    (11, "La Poste"),
    (
        12,
        "Office fédéral des constructions et de la logistique (OFCL) ou Domaine des EPF",
    ),
    (13, "Office fédéral des routes (OFROU)"),
    (14, "Organisation internationale, ambassade"),
    (15, "Particulier et hoirie"),
    (16, "Société d'assurances (sans les caisses de pension et les caisses-maladie)"),
    (17, "Société de capitaux, SA, Sàrl (construction, immobilier)"),
    (18, "Société de capitaux, SA, Sàrl (industrie, artisanat, commerce)"),
    (19, "Société individuelle ou de personnes (construction, immobilier)"),
    (20, "Société individuelle ou de personnes (industrie, artisanat, commerce)"),
    (21, "Swisscom"),
    (22, "Usine à gaz privée"),
    (23, "Usine d'électricité privée"),
    (24, "Autre maître d'ouvrage privé (église, fondation, association, etc.)"),
)


def get_owner_type_label(index):
    for i, label in PROJECT_OWNER_TYPES:
        if i == index:
            return label


BOOLEAN_RADIO_CHOICES = ((True, _("Oui")), (False, _("Non")))

# Localisation's coordinates limits
PERMIT_EAST_MIN_COORD = 2490000
PERMIT_EAST_MAX_COORD = 2590000
PERMIT_NORTH_MIN_COORD = 1110000
PERMIT_NORTH_MAX_COORD = 1210000


class PermitSubmissionTypes(models.TextChoices):
    PUBLIC_ENQUIRY = "public_enquiry", _("Enquête publique")
    PRIOR_ANALYSIS = "prior_analysis", _("Analyse préalable")


class PermitCompetencies(models.TextChoices):
    COMPETENCY_ME = "me", _("Municipale + État (ME)")
    COMPETENCY_M = "m", _("Municipale (M)")


class PermitSubmissionNatures(models.TextChoices):
    NATURE_ADJONCTION = "adjonction", _("Adjonction")
    NATURE_AGRANDISSEMENT = "agrandissement", _("Agrandissement")
    NATURE_CHANGEMENT = "changement", _("Changement / nouvelle destination des locaux")
    NATURE_CONSTRUCTION = "construction", _("Construction nouvelle")
    NATURE_DEMOLITION_PARTIELLE = "demolition_partielle", _("Démolition partielle")
    NATURE_DEMOLITION_TOTALE = "demolition_totale", _("Démolition totale")
    NATURE_RECONSTRUCTION_DEMOLITION = "reconstruction_demolition", _(
        "Reconstruction après démolition"
    )
    NATURE_RECONSTRUCTION_INCENDIE = "reconstruction_incendie", _(
        "Reconstruction après incendie"
    )
    NATURE_RENOVATION_TOTALE = "renovation_totale", _("Rénovation totale")
    NATURE_TRANSFORMATION = "transformation", _("Transformation(s)")


class PermitSubmissionTransformationTypes(models.TextChoices):
    TRANSFO_AGGRAND_HEATED = "agrandissement_chauffé", _("Agrandissement chauffé")
    TRANSFO_AGGRAND_NOT_HEATED = "agrandissement_non_chauffé", _(
        "Agrandissement non chauffé"
    )
    TRANSFO_ASSAIN_ENERGETIC = "assainissement_énergétique", _(
        "Assainissement énergétique"
    )
    TRANSFO_ASSAIN_HEATING = "assainissement_chauffage", _(
        "Assainissement système de chauffage"
    )
    TRANSFO_CHANGE_AFFECTATION = "changement_affectation", _("Changement d'affectation")
    TRANSFO_INSTALL_SOLAR_PHOTO = "solaire_photovoltaïque", _(
        "Installation solaire photovoltaïque"
    )
    TRANSFO_INSTALL_SOLAR_THERMIC = "solaire_thermique", _(
        "Installation solaire thermique"
    )
    TRANSFO_INDOR_RENOVATION = "renovations_intérieures", _(
        "Transformations / rénovations intérieures"
    )
    TRANSFO_OTHER = "autres", _("Autres travaux de transformation")


class PermitConstructionTypes(models.TextChoices):
    CONSTRUCTION_BUILDING = "building", _("Bâtiment")
    CONSTRUCTION_OUTDOOR = "outdoor", _("Ouvrage de génie civil, aménagement extérieur")
    CONSTRUCTION_SPECIAL = "special", _("Construction particulière")


class PermitZonePlans(models.TextChoices):
    PLAN_PGA = "pga", _("PGA - Plan général d'affectation")
    PLAN_PPA = "ppa", _("PPA - Plan partiel d'affectation")
    PLAN_PQ_PQCM = "pq_pqcm", _("PQ/PQCM - Plan de quartier")


class PermitContact(BaseContact):
    first_name = models.CharField(
        _("Prénom"),
        max_length=150,
        null=True,
        blank=True,
    )
    last_name = models.CharField(
        _("Nom"),
        max_length=100,
        null=True,
        blank=True,
    )
    company_name = models.CharField(
        _("Raison sociale"),
        max_length=100,
        blank=True,
        help_text=_(
            "Le nom et le prénom, ou la raison sociale, doivent être renseignés."
        ),
    )

    def full_name(self):
        return f"{self.first_name or ''} {self.last_name or ''}".strip()

    def __str__(self):
        return (
            f"{self.first_name} {self.last_name}"
            if self.first_name
            else self.company_name
        )


# WARNING: These are PermitSubmission's model's fields.
# Impacts the model definition & migrations
PERMIT_SUBMISSION_FIELDS = {
    # CLASSIFICATION DATA
    "classification": [
        {
            "name": "submission_type",
            "label": _("Type"),
            "type": models.CharField,
            "args": {
                "choices": PermitSubmissionTypes.choices,
                "default": PermitSubmissionTypes.PUBLIC_ENQUIRY,
                "max_length": 20,
            },
        },
        {
            "name": "shortname",
            "label": _("Identifiant"),
            "type": models.CharField,
            "args": {
                "max_length": 32,
                "help_text": _("Référence communale."),
            },
        },
        {
            "name": "competency",
            "label": _("Compétence"),
            "type": EmptyBlankChoiceCharField,
            "args": {
                "choices": PermitCompetencies.choices,
                "max_length": 20,
                "default": PermitCompetencies.COMPETENCY_ME,
            },
        },
        {
            "name": "camac_number",
            "label": _("N° CAMAC"),
            "type": ConditionalIntegerField,
            "args": {
                "null": True,
                "depends_on": "submission_type",
                "expected_values": [PermitSubmissionTypes.PUBLIC_ENQUIRY],
            },
        },
    ],
    # DONNÉES GÉNÉRALES
    "general_data": [
        {
            "name": "object",
            "label": _("Description"),
            "type": models.CharField,
            "args": {
                "max_length": 180,
            },
        },
        {
            "name": "nature",
            "label": _("Nature"),
            "type": EmptyBlankChoiceCharField,
            "args": {
                "choices": PermitSubmissionNatures.choices,
                "max_length": 40,
            },
        },
        {
            "name": "transformation_type",
            "label": _("Transformation: Type de travaux"),
            "type": ConditionalChoicesCharField,
            "args": {
                "blank": True,
                "max_length": 50,
                "choices": PermitSubmissionTransformationTypes.choices,
                "depends_on": "nature",
                "expected_values": [PermitSubmissionNatures.NATURE_TRANSFORMATION],
            },
        },
        {
            "name": "work_type",
            "label": _("Type d'ouvrage"),
            "type": EmptyBlankChoiceSmallIntegerField,
            "args": {
                "default": None,
                "choices": WORK_TYPES_CHOICES,
            },
        },
        {
            "name": "construction_type",
            "label": _("Genre de construction"),
            "type": EmptyBlankChoiceCharField,
            "args": {
                "choices": PermitConstructionTypes.choices,
                "default": PermitConstructionTypes.CONSTRUCTION_BUILDING,
                "max_length": 40,
            },
        },
    ],
    # ATTRIBUTES (localisation, zone type, etc.)
    "attributes": [
        {
            "name": "parcel_numbers",
            "label": _("N° de parcelle"),
            "type": ArrayField,
            "args": {
                "base_field": models.CharField(
                    max_length=100,
                    validators=[RegexValidator(r"^(\d+|DP \d+)$")],
                ),
                "help_text": _(
                    "Parcelle(s) concernée(s) par la demande (séparez les numéros par des virgules)."
                    "<br>Exemple: 1234, DP 5678."
                ),
            },
        },
        {
            "name": "eca_number",
            "label": _("N° ECA"),
            "type": ConditionalCharField,
            "args": {
                "max_length": 10,
                "depends_on": "nature",
                "expected_values": [
                    PermitSubmissionNatures.NATURE_TRANSFORMATION,
                    PermitSubmissionNatures.NATURE_DEMOLITION_PARTIELLE,
                    PermitSubmissionNatures.NATURE_DEMOLITION_TOTALE,
                    PermitSubmissionNatures.NATURE_RECONSTRUCTION_DEMOLITION,
                ],
            },
        },
        {
            "name": "east_coordinate",
            "label": _("Coordonnée Est"),
            "type": models.IntegerField,
            "args": {
                "validators": [
                    MinValueValidator(PERMIT_EAST_MIN_COORD),
                    MaxValueValidator(PERMIT_EAST_MAX_COORD),
                ],
            },
        },
        {
            "name": "north_coordinate",
            "label": _("Coordonnée Nord"),
            "type": models.IntegerField,
            "args": {
                "validators": [
                    MinValueValidator(PERMIT_NORTH_MIN_COORD),
                    MaxValueValidator(PERMIT_NORTH_MAX_COORD),
                ],
            },
        },
        {
            "name": "is_outside_building_zone",
            "label": _("Travaux situés hors d'une zone à bâtir"),
            "type": models.BooleanField,
            "args": {
                "default": False,
                "choices": BOOLEAN_RADIO_CHOICES,
            },
        },
        {
            "name": "is_parcel_outside_building_zone",
            "label": _(
                "Parcelle(s) située(s) partiellement ou entièrement hors des zones à bâtir"
            ),
            "type": ConditionalBooleanField,
            "args": {
                "help_text": _(
                    "Parcelle(s) située(s) partiellement ou entièrement hors des zones "
                    "à bâtir (zone agricole, viticole, alpestre, aire forestière, zone "
                    "intermédiaire, zone de verdure, zone spéciale art. 50a LATC, zone "
                    "agricole spécialisée art. 52a LATC, zone des hameaux, etc.)."
                ),
                "depends_on": "is_outside_building_zone",
                "expected_values": [True],
                "choices": BOOLEAN_RADIO_CHOICES,
                "default": None,
                "null": True,
            },
        },
        {
            "name": "is_work_outside_building_zone",
            "label": _("Travaux situés hors zone à bâtir"),
            "type": ConditionalBooleanField,
            "args": {
                "help_text": _(
                    "Travaux situés hors zone à bâtir (si constructions, installations "
                    "(y compris de chantier), démolitions, changements d'affectation, "
                    "conduites, fondations, mouvements de terre, accès, dépôts, etc. "
                    "sont prévus, même partiellement, hors zone à bâtir)."
                ),
                "depends_on": "is_outside_building_zone",
                "expected_values": [True],
                "choices": BOOLEAN_RADIO_CHOICES,
                "default": None,
                "null": True,
            },
        },
        {
            "name": "zone_name",
            "label": _("Zone"),
            "type": ConditionalCharField,
            "args": {
                "depends_on": "is_outside_building_zone",
                "expected_values": [False],
                "max_length": 100,
            },
        },
        {
            "name": "zone_plan",
            "label": _("Plan"),
            "type": EmptyBlankChoiceCharField,
            "args": {
                "choices": PermitZonePlans.choices,
                "default": PermitZonePlans.PLAN_PGA,
                "max_length": 40,
            },
        },
    ],
    # INTERVENANTS
    "participants": [
        {
            "name": "project_owner_type",
            "label": _("Maître d'ouvrage"),
            "type": EmptyBlankChoiceSmallIntegerField,
            "args": {
                "choices": PROJECT_OWNER_TYPES,
            },
        },
    ],
    "project_owner": [
        {
            "name": "project_owner",
            "label": _("Maître d'ouvrage (contact)"),
            "type": models.ForeignKey,
            "args": {
                "to": PermitContact,
                "on_delete": models.SET_NULL,
                "related_name": "project_owned_permit_submissions",
                "null": True,
            },
        },
    ],
    "owner": [
        # The form has also a "form only" field : is_owner_same_as_project_owner
        {
            "name": "owner",
            "label": _("Propriétaire"),
            "type": models.ForeignKey,
            "args": {
                "to": PermitContact,
                "on_delete": models.SET_NULL,
                "related_name": "owned_permit_submissions",
                "null": True,
            },
        }
    ],
}


class PermitSubmission(PolymorphicSubmission):
    STATUS_CHOICES = (
        (PolymorphicSubmission.STATUS_DRAFT, _("Brouillon")),  # 0
        (
            PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION,
            _("Envoyée, en attente de traitement"),
        ),  # 1
        (
            PolymorphicSubmission.STATUS_PROCESSING,
            _("En cours de traitement"),
        ),  # 3
        (
            PolymorphicSubmission.STATUS_AWAITING_VALIDATION,
            _("En attente de validation"),
        ),  # 5
    )

    def get_status_choices(cls):
        return cls.STATUS_CHOICES

    status = models.PositiveSmallIntegerField(
        _("état"),
        choices=STATUS_CHOICES,
        default=PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION,
    )
    author = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("auteur"),
        related_name="permit_submissions",
    )
    formal_examination_report = models.TextField(
        verbose_name=_("Compte rendu de l’examen formel"), blank=True
    )

    @staticmethod
    def _get_flat_fields():
        """
        Return the list of all fields without section.
        """
        return [
            field for field in chain.from_iterable(PERMIT_SUBMISSION_FIELDS.values())
        ]

    # Set model fields based on PERMIT_SUBMISSION_FIELDS
    for field in _get_flat_fields():
        field_type = field["type"]
        field_args = field.get("args", {})
        field_name = field["name"]

        if field_name in locals():
            raise RuntimeError(f"Field name '{field_name}' already exists in locals()")

        locals()[field_name] = field_type(**field_args, verbose_name=field["label"])

    def get_specific(self):
        return self

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.validate_conditions()
        super().save(force_insert, force_update, using, update_fields)

    def validate_conditions(self):
        for field in self._meta.fields:
            parental_field = getattr(field, "parental_field", None)
            if parental_field:
                value = getattr(self, field.attname)
                parent_field_validator(
                    field.attname, parental_field, field.parental_expected_values
                )(value, self)

    def get_id_for_display(self):
        return f"P-{self.pk}"

    def get_coordinates_display(self):
        return f"{self.east_coordinate:_} / {self.north_coordinate:_}".replace("_", "'")

    @property
    def submission_type_label(self):
        return dict(PermitSubmissionTypes.choices)[self.submission_type]

    def can_edit_submission(self, user):
        return has_permission_to_edit_permit_submissions_for_entity(
            user, self.administrative_entity
        )

    def can_delete_submission(self, user):
        return has_permission_to_delete_permit_submissions_for_entity(
            user, self.administrative_entity
        )

    def get_checklist_for_step(self, step):
        queryset = self.checklists.filter(
            checklist__step_checklist__administrative_entity=self.administrative_entity,
            checklist__step_checklist__step=step.value,
        )

        return queryset.first()

    def get_forms_names_list(self):
        return self.get_submission_type_display()

    def can_be_sent_for_validation(self):
        return (
            self.status == PolymorphicSubmission.STATUS_SUBMITTED_FOR_VALIDATION
            and not self.progress.technical_examination
        )

    def can_be_seen_by_validators(self):
        return self.status in [
            PolymorphicSubmission.STATUS_AWAITING_VALIDATION,
            PolymorphicSubmission.STATUS_PROCESSING,
        ]

    def get_validators(self):
        validations = self.validations.all()
        queryset = User.objects.none()
        for validation in validations:
            queryset = queryset.union(validation.department.group.user_set.all())
        return queryset

    def get_detail_url(self):
        return reverse(
            "permits:permit_submission_detail:current",
            kwargs={"pk": self.pk},
        )

    def get_documents(self, user):
        qs = self.documents.all().order_by("pk").distinct()

        return qs.filter(
            Q(owner=user) | Q(authorised_departments__group__in=user.groups.all()),
        )


class PermitSubmissionProgress(models.Model):
    permit_submission = models.OneToOneField(
        "PermitSubmission", on_delete=models.CASCADE, related_name="progress"
    )
    formal_examination = models.BooleanField(default=False)
    technical_examination = models.BooleanField(default=False)
    validation = models.BooleanField(default=False)


@receiver(post_save, sender=PermitSubmission)
def create_permit_submission_progress(sender, instance, created, **kwargs):
    if created:
        PermitSubmissionProgress.objects.create(permit_submission=instance)


class PermitSubmissionDocument(BaseSubmissionDocument):
    document = PermitSubmissionDocumentFileField(
        _("Document"),
        upload_to="permit_documents/",
    )
    description = models.CharField(
        _("Description du document"),
        max_length=80,
        blank=False,
        null=False,
    )
    created_at = models.DateTimeField(
        _("Date de création"),
        auto_now_add=True,
    )
    owner = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
        verbose_name=_("Propriétaire du document"),
        related_name="permit_documents",
    )
    permit_submission = models.ForeignKey(
        PermitSubmission,
        null=False,
        on_delete=models.CASCADE,
        verbose_name=_("Permis"),
        related_name="documents",
    )
    authorised_departments = models.ManyToManyField(
        SubmissionDepartment,
        verbose_name=_("Service(s) autorisé(s) à visualiser le document"),
        related_name="permit_documents",
    )

    def get_download_url(self):
        return reverse(
            "permits:permit_submission_download_document",
            kwargs={
                "pk": self.permit_submission.pk,
                "document_pk": self.pk,
                "path": self.document.name.split("/")[-1],
            },
        )

    def get_remove_url(self):
        return reverse(
            "permits:permit_submission_remove_document",
            kwargs={"pk": self.permit_submission.pk, "document_pk": self.pk},
        )
