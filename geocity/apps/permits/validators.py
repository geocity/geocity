from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def parent_field_validator(field_name, parent_field_name, expected_values):
    """
    Validators using "instance" are not serializable =>
    Can't be added to field definitions & migrations.
    Use parent_field_validator only in clean methods.
    """

    def validator(value, instance):
        parent_value = getattr(instance, parent_field_name, None)
        value = getattr(instance, field_name, None)
        expected_values_str = ", ".join([str(ev) for ev in expected_values])

        if parent_value in expected_values and value is None:
            raise ValidationError(
                {
                    field_name: _(
                        f"Ce champ ne peut pas être vide quand '{parent_field_name}' est une de ces valeurs: '{expected_values_str}'"
                    )
                }
            )

        if parent_value not in expected_values and value is not False and bool(value):
            raise ValidationError(
                {
                    field_name: _(
                        f"Ce champ doit être vide quand '{parent_field_name}' est une de ces valeurs: '{expected_values_str}'"
                    )
                }
            )

    return validator
