from geocity.apps.permits.models import (
    PERMIT_EAST_MIN_COORD,
    PERMIT_NORTH_MIN_COORD,
    PermitCompetencies,
    PermitConstructionTypes,
    PermitSubmissionNatures,
    PermitSubmissionTypes,
)
from geocity.apps.permits.steps import PermitStepType, StepChecklist
from geocity.apps.permits.utils import create_permit_submission
from geocity.apps.submissions.checklists.models import Checklist, ChecklistTask
from geocity.apps.submissions.management.fixturize_data.generic import *  # noqa: F403

# Variables to change the result of the fixturize
entities = {
    "mercure.localhost": "mercure",
    "venus.localhost": "venus",
}


def get_checklist_data():
    public_enquiry_lists = [
        {
            "name": "Enquête publique: Contrôle / Examen",
            "tasks": [
                {
                    "order": 0,
                    "description": "Documents complets",
                },
                {
                    "order": 1,
                    "description": "Lois et règlements cantonaux et fédéraux",
                },
                {
                    "order": 2,
                    "description": "Conformité avec le règlement communal sur les constructions",
                },
                {
                    "order": 3,
                    "description": "Demande de compléments si nécessaire",
                },
            ],
            "step": PermitStepType.VALIDATION.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
        {
            "name": "Enquête publique: Enquête publique",
            "tasks": [
                {
                    "order": 0,
                    "description": "Vérifier le(s) propriétaire(s) et noms et adresses. Si entreprise, vérifier dénomination au RC",
                },
                {
                    "order": 1,
                    "description": "Questionnaire signé par le constructeur?",
                },
                {
                    "order": 2,
                    "description": "Déterminer date de parution dans la presse et période d'enquête",
                },
                {
                    "order": 3,
                    "description": "Envoi des avis d'enquête en pdf pour publication, copies en interne",
                },
                {
                    "order": 4,
                    "description": "Télécharger le dossier sur le SIT pour consultation en ligne",
                },
                {
                    "order": 5,
                    "description": "Préparer dossiers physiques pour circulation interne et consultation publique sur place",
                },
            ],
            "step": PermitStepType.PUBLIC_INQUIRY.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
        {
            "name": "Enquête publique: Décision",
            "tasks": [
                {
                    "order": 0,
                    "description": "Réception de la synthèse CAMAC",
                },
                {
                    "order": 1,
                    "description": "Si opposition(s), transmission groupée après enquête au mandataire",
                },
                {
                    "order": 2,
                    "description": "Edition proposition municipale avec éventuelles levées d'oppositions, réponses aux observations et droits de recours",
                },
            ],
            "step": PermitStepType.DECISION.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
        {
            "name": "Enquête publique: Envoi décision",
            "tasks": [
                {
                    "order": 0,
                    "description": "Envoi le même jour: levées d'oppositions, réponses aux observations, permis au mandataire",
                },
            ],
            "step": PermitStepType.DECISION_DISPATCH.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
        {
            "name": "Enquête publique: Travaux",
            "tasks": [
                {
                    "order": 0,
                    "description": "Réception de l'annonce de début des travaux et information de l'inspectorat des chantiers",
                },
            ],
            "step": PermitStepType.CONSTRUCTION.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
        {
            "name": "Enquête publique: Visites",
            "tasks": [
                {
                    "order": 0,
                    "description": "Conditions permis de construire",
                },
                {
                    "order": 1,
                    "description": "Respect des directives AEAI",
                },
            ],
            "step": PermitStepType.VISITS.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
        {
            "name": "Enquête publique: Délivrance du permis",
            "tasks": [
                {
                    "order": 0,
                    "description": "Visite de fin de chantier",
                },
                {
                    "order": 1,
                    "description": "Délivrance du permis",
                },
            ],
            "step": PermitStepType.PERMIT.value,
            "permit_submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
        },
    ]

    prior_analysis_lists = [
        {
            "name": "Analyse préalable: Contrôle / Examen",
            "tasks": [
                {
                    "order": 0,
                    "description": "Descriptif du projet",
                },
                {
                    "order": 1,
                    "description": "Plans généraux",
                },
                {
                    "order": 2,
                    "description": "Lois et règlements cantonaux et fédéraux",
                },
                {
                    "order": 3,
                    "description": "Conformité avec le règlement communal sur les constructions",
                },
            ],
            "step": PermitStepType.VALIDATION.value,
            "permit_submission_type": PermitSubmissionTypes.PRIOR_ANALYSIS,
        },
        {
            "name": "Analyse préalable: Envoi décision",
            "tasks": [
                {
                    "order": 0,
                    "description": "Envoi de la réponse",
                },
            ],
            "step": PermitStepType.DECISION_DISPATCH.value,
            "permit_submission_type": PermitSubmissionTypes.PRIOR_ANALYSIS,
        },
    ]

    return public_enquiry_lists + prior_analysis_lists


def get_permit_submission_data(user):
    return [
        {
            "administrative_entity": None,
            "submission_type": PermitSubmissionTypes.PUBLIC_ENQUIRY,
            "camac_number": "233826",
            "shortname": "DemDupont",
            "object": "Démolition de la ferme de la famille Dupont",
            "competency": PermitCompetencies.COMPETENCY_ME,
            "nature": PermitSubmissionNatures.NATURE_DEMOLITION_PARTIELLE,
            "transformation_type": "",
            "work_type": 1,
            "construction_type": PermitConstructionTypes.CONSTRUCTION_OUTDOOR,
            "zone_name": "g",
            "zone_plan": "pga",
            "parcel_numbers": ["1234"],
            "eca_number": "123",
            "east_coordinate": PERMIT_EAST_MIN_COORD + 1,
            "north_coordinate": PERMIT_NORTH_MIN_COORD + 1,
            "is_outside_building_zone": True,
            "is_parcel_outside_building_zone": False,
            "is_work_outside_building_zone": True,
            "project_owner_type": 2,
            "author": user,
            "contact_data": {
                "project_owner": {
                    "first_name": "David",
                    "last_name": "Bahon",
                    "company_name": "",
                    "address": "boulevard Broquet 240",
                    "email": "davidbahon@example.org",
                    "phone": "+41211234567",
                    "zipcode": "1400",
                    "city": "Yverdon-les-Bains",
                    "country": "CH",
                },
                "owner": {
                    "is_owner_same_as_project_owner": True,
                },
            },
        },
        {
            "administrative_entity": None,
            "submission_type": PermitSubmissionTypes.PRIOR_ANALYSIS,
            "camac_number": None,
            "shortname": "A-42",
            "object": "Construction d'un four à pain communal",
            "competency": PermitCompetencies.COMPETENCY_M,
            "nature": PermitSubmissionNatures.NATURE_CONSTRUCTION,
            "transformation_type": "",
            "work_type": 25,
            "construction_type": PermitConstructionTypes.CONSTRUCTION_SPECIAL,
            "zone_name": "g",
            "zone_plan": "pga",
            "parcel_numbers": ["1234"],
            "eca_number": "123",
            "east_coordinate": PERMIT_EAST_MIN_COORD + 1,
            "north_coordinate": PERMIT_NORTH_MIN_COORD + 1,
            "is_outside_building_zone": True,
            "is_parcel_outside_building_zone": False,
            "is_work_outside_building_zone": True,
            "project_owner_type": 1,
            "author": user,
            "contact_data": {
                "project_owner": {
                    "first_name": "Monique",
                    "last_name": "Pouly",
                    "company_name": "ABCD AG",
                    "address": "Rue de la Corniche 42",
                    "email": "monique.p@example.org",
                    "phone": "+41211234567",
                    "zipcode": "2013",
                    "city": "Boudry",
                    "country": "CH",
                },
                "owner": {
                    "is_owner_same_as_project_owner": True,
                },
            },
        },
    ]


def extra_setup():
    from django.contrib.auth.models import Group

    from geocity.apps.accounts.models import AdministrativeEntity

    first = AdministrativeEntity.objects.first()
    first.is_permits_enabled = True
    first.save()

    user = Group.objects.get(name="Mercure coordinators").user_set.first()

    integrator = None

    for dep in first.departments.all():
        if not dep.integrator:
            continue
        integrator = Group.objects.get(pk=dep.integrator)
        break

    # Create checklists
    for checklist in get_checklist_data():
        checklist_obj = Checklist.objects.create(
            name=checklist["name"], integrator=integrator
        )
        for task in checklist["tasks"]:
            ChecklistTask.objects.create(
                order=task["order"],
                description=task["description"],
                checklist=checklist_obj,
            )
        if "step" in checklist:
            StepChecklist.objects.create(
                administrative_entity=first,
                checklist=checklist_obj,
                step=checklist["step"],
                permit_submission_type=checklist.get("permit_submission_type", None),
            )

    # Create a PermitSubmission
    for data in get_permit_submission_data(user):
        contact_data = data.pop("contact_data")
        data["administrative_entity"] = first
        create_permit_submission(user, data, contact_data)
