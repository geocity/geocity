from django.apps import AppConfig


class PermitsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "geocity.apps.permits"
    verbose_name = "Permis de construire"
