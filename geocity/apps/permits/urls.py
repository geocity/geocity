from django.urls import include, path

from . import views

app_name = "permits"

submission_detail_urlpatterns = [
    path(
        "",
        views.PermitSubmissionDetailView.as_view(),
        name="current",
    ),
    path(
        "validation/",
        views.PermitSubmissionDetailValidationStepView.as_view(),
        name="validation",
    ),
    path(
        "public-inquiry/",
        views.PermitSubmissionDetailPublicInquiryStepView.as_view(),
        name="public-inquiry",
    ),
    path(
        "decision/",
        views.PermitSubmissionDetailDecisionStepView.as_view(),
        name="decision",
    ),
    path(
        "decision-dispatch/",
        views.PermitSubmissionDetailDecisionDispatchStepView.as_view(),
        name="decision-dispatch",
    ),
    path(
        "construction/",
        views.PermitSubmissionDetailConstructionStepView.as_view(),
        name="construction",
    ),
    path(
        "visits/", views.PermitSubmissionDetailVisitsStepView.as_view(), name="visits"
    ),
    path(
        "permit/", views.PermitSubmissionDetailPermitStepView.as_view(), name="permit"
    ),
    path("edit/", views.PermitSubmissionEditView.as_view(), name="edit"),
]

urlpatterns = [
    path(
        "create",
        views.PermitSubmissionCreateView.as_view(),
        name="permit_submission_create",
    ),
    path(
        "<int:pk>/",
        include(
            (submission_detail_urlpatterns, app_name),
            namespace="permit_submission_detail",
        ),
    ),
    path(
        "<int:pk>/upload",
        views.PermitSubmissionDocumentUploadView.as_view(),
        name="permit_submission_upload",
    ),
    path(
        "<int:pk>/download/<int:document_pk>/<path:path>",
        views.PermitSubmissionDocumentDownloadView.as_view(),
        name="permit_submission_download_document",
    ),
    path(
        "<int:pk>/remove/<int:document_pk>",
        views.PermitSubmissionDocumentRemoveView.as_view(),
        name="permit_submission_remove_document",
    ),
]
