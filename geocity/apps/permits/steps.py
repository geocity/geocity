import dataclasses
import enum

from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from geocity.apps.permits.models import PermitSubmissionTypes
from geocity.apps.submissions.checklists.models import Checklist


@dataclasses.dataclass
class PermitSubStep:
    name: str
    completed: bool = False


@dataclasses.dataclass
class PermitStep:
    name: str
    url: str = None
    completed: bool = False
    sub_steps: list[PermitSubStep] = dataclasses.field(default_factory=list)


class PermitStepType(enum.Enum):
    VALIDATION = "validation"
    PUBLIC_INQUIRY = "public-inquiry"
    DECISION = "decision"
    DECISION_DISPATCH = "decision-dispatch"
    CONSTRUCTION = "construction"
    VISITS = "visits"
    PERMIT = "permit"


PERMIT_STEP_NAME = {
    PermitStepType.VALIDATION: _("Contrôle / Examen"),
    PermitStepType.PUBLIC_INQUIRY: _("Enquête publique"),
    PermitStepType.DECISION: _("Décision"),
    PermitStepType.DECISION_DISPATCH: _("Envoi décision"),
    PermitStepType.CONSTRUCTION: _("Travaux"),
    PermitStepType.VISITS: _("Visites"),
    PermitStepType.PERMIT: _("Délivrance du permis"),
}


def get_steps_for_stepper(request, submission, user):
    steps = {
        PermitStepType.VALIDATION: get_validation_step(submission),
        PermitStepType.PUBLIC_INQUIRY: get_public_inquiry_step(submission),
        PermitStepType.DECISION: get_decision_step(submission),
        PermitStepType.DECISION_DISPATCH: get_decision_dispatch_step(submission),
        PermitStepType.CONSTRUCTION: get_construction_step(submission),
        PermitStepType.VISITS: get_visits_step(submission),
        PermitStepType.PERMIT: get_permit_step(submission),
    }

    return {step_type: step for step_type, step in steps.items() if step is not None}


def get_validation_step(submission):
    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.VALIDATION],
        completed=submission.progress.validation,
        url=reverse(
            "permits:permit_submission_detail:validation", kwargs={"pk": submission.pk}
        ),
        sub_steps=[
            PermitSubStep(
                name=_("Examen formel"),
                completed=submission.progress.formal_examination,
            ),
            PermitSubStep(
                name=_("Examen technique"),
                completed=submission.progress.technical_examination,
            ),
        ],
    )


def get_public_inquiry_step(submission):
    if submission.submission_type == PermitSubmissionTypes.PRIOR_ANALYSIS:
        return None

    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.PUBLIC_INQUIRY],
        url=reverse(
            "permits:permit_submission_detail:public-inquiry",
            kwargs={"pk": submission.pk},
        ),
    )


def get_decision_step(submission):
    if submission.submission_type == PermitSubmissionTypes.PRIOR_ANALYSIS:
        return None

    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.DECISION],
        url=reverse(
            "permits:permit_submission_detail:decision", kwargs={"pk": submission.pk}
        ),
    )


def get_decision_dispatch_step(submission):
    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.DECISION_DISPATCH],
        url=reverse(
            "permits:permit_submission_detail:decision-dispatch",
            kwargs={"pk": submission.pk},
        ),
    )


def get_construction_step(submission):
    if submission.submission_type == PermitSubmissionTypes.PRIOR_ANALYSIS:
        return None

    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.CONSTRUCTION],
        url=reverse(
            "permits:permit_submission_detail:construction",
            kwargs={"pk": submission.pk},
        ),
    )


def get_visits_step(submission):
    if submission.submission_type == PermitSubmissionTypes.PRIOR_ANALYSIS:
        return None

    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.VISITS],
        url=reverse(
            "permits:permit_submission_detail:visits", kwargs={"pk": submission.pk}
        ),
    )


def get_permit_step(submission):
    if submission.submission_type == PermitSubmissionTypes.PRIOR_ANALYSIS:
        return None

    return PermitStep(
        name=PERMIT_STEP_NAME[PermitStepType.PERMIT],
        url=reverse(
            "permits:permit_submission_detail:permit", kwargs={"pk": submission.pk}
        ),
    )


STEP_FUNCTIONS = {
    PermitSubmissionTypes.PRIOR_ANALYSIS: {
        PermitStepType.VALIDATION: get_validation_step,
        PermitStepType.DECISION_DISPATCH: get_decision_dispatch_step,
    },
    PermitSubmissionTypes.PUBLIC_ENQUIRY: {
        PermitStepType.VALIDATION: get_validation_step,
        PermitStepType.PUBLIC_INQUIRY: get_public_inquiry_step,
        PermitStepType.DECISION: get_decision_step,
        PermitStepType.DECISION_DISPATCH: get_decision_dispatch_step,
        PermitStepType.CONSTRUCTION: get_construction_step,
        PermitStepType.VISITS: get_visits_step,
        PermitStepType.PERMIT: get_permit_step,
    },
}


def get_step_object_by_name_and_type(submission, step_name, step_type):
    return STEP_FUNCTIONS.get(step_type).get(step_name)(submission)


class StepChecklist(models.Model):
    administrative_entity = models.ForeignKey(
        "accounts.AdministrativeEntity",
        related_name="step_checklists",
        on_delete=models.CASCADE,
        verbose_name=_("Entité administrative"),
    )
    checklist = models.OneToOneField(
        Checklist, related_name="step_checklist", on_delete=models.CASCADE
    )
    step = models.CharField(
        _("Etape"),
        blank=False,
        null=False,
        max_length=255,
        choices=[(key.value, value) for key, value in PERMIT_STEP_NAME.items()],
    )
    permit_submission_type = models.CharField(
        _("Type de permis de construire"),
        max_length=255,
        choices=PermitSubmissionTypes.choices,
        null=True,
        blank=True,
    )

    class Meta:
        unique_together = ("administrative_entity", "step", "permit_submission_type")
        verbose_name = _("Checklist d'étape")
        verbose_name_plural = _("Checklists d'étape")

    def __str__(self):
        permit_submission_type = self.get_permit_submission_type_display()
        if not permit_submission_type:
            permit_submission_type = _("Tous les types de permis")
        return f"{self.get_step_display()} - {permit_submission_type} - {self.administrative_entity}"

    def clean(self):
        checklist_same_type = StepChecklist.objects.filter(
            permit_submission_type__isnull=True,
            step=self.step,
            administrative_entity=self.administrative_entity,
        )
        if self.permit_submission_type and checklist_same_type.exists():
            raise ValidationError(
                {
                    "step": _(
                        f"Cette étape est déjà associée à une checklist: {checklist_same_type.first().checklist.name}"
                    )
                }
            )
        checklist_all_types = StepChecklist.objects.filter(
            step=self.step,
            administrative_entity=self.administrative_entity,
            permit_submission_type__isnull=False,
        )
        if self.permit_submission_type is None and checklist_all_types.exists():
            raise ValidationError(
                {
                    "step": _(
                        f"Cette étape est déjà associée à une checklist: {checklist_all_types.first().checklist.name}"
                    )
                }
            )
        return super().clean()
