# Generated by Django 4.2.16 on 2024-12-12 14:44

from django.db import migrations

import geocity.apps.permits.fields


class Migration(migrations.Migration):

    dependencies = [
        ("permits", "0011_permitsubmission_author"),
    ]

    operations = [
        migrations.AlterField(
            model_name="permitsubmission",
            name="is_work_outside_building_zone",
            field=geocity.apps.permits.fields.ConditionalBooleanField(
                choices=[(True, "Oui"), (False, "Non")],
                default=None,
                help_text="Travaux situés hors zone à bâtir (si constructions, installations (y compris de chantier), démolitions, changements d'affectation, conduites, fondations, mouvements de terre, accès, dépôts, etc. sont prévus, même partiellement, hors zone à bâtir).",
                null=True,
                verbose_name="Travaux situés hors zone à bâtir",
            ),
        ),
    ]
