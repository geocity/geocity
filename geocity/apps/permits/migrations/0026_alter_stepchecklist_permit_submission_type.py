# Generated by Django 4.2.16 on 2025-03-11 11:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("permits", "0025_merge_20250306_0916"),
    ]

    operations = [
        migrations.AlterField(
            model_name="stepchecklist",
            name="permit_submission_type",
            field=models.CharField(
                blank=True,
                choices=[
                    ("public_enquiry", "Enquête publique"),
                    ("prior_analysis", "Analyse préalable"),
                ],
                max_length=255,
                null=True,
                verbose_name="Type de permis de construire",
            ),
        ),
    ]
