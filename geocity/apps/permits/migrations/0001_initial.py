# Generated by Django 4.2.16 on 2024-11-09 20:05

import django.contrib.postgres.fields
import django.core.validators
import django.db.models.deletion
import django_countries.fields
from django.db import migrations, models

import geocity.apps.permits.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("accounts", "0024_alter_administrativeentity_is_polco_enabled"),
    ]

    operations = [
        migrations.CreateModel(
            name="PolcoContact",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "vat_number",
                    models.CharField(
                        blank=True, max_length=19, verbose_name="Numéro TVA"
                    ),
                ),
                ("address", models.CharField(max_length=100, verbose_name="Adresse")),
                (
                    "country",
                    django_countries.fields.CountryField(
                        max_length=2, null=True, verbose_name="Pays"
                    ),
                ),
                ("phone", models.CharField(max_length=20, verbose_name="Téléphone")),
                ("email", models.EmailField(max_length=254, verbose_name="Email")),
                (
                    "first_name",
                    models.CharField(
                        blank=True, max_length=150, null=True, verbose_name="Prénom"
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        blank=True, max_length=100, null=True, verbose_name="Nom"
                    ),
                ),
                (
                    "company_name",
                    models.CharField(
                        blank=True, max_length=100, verbose_name="Raison sociale"
                    ),
                ),
                (
                    "zipcode",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="Code postal"
                    ),
                ),
                (
                    "city",
                    models.CharField(
                        blank=True, max_length=100, null=True, verbose_name="Ville"
                    ),
                ),
            ],
            options={
                "verbose_name": "Contact",
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="PolcoSubmission",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "submission_type",
                    models.CharField(
                        choices=[
                            ("prior_analysis", "Analyse préalable"),
                            ("public_enquiry", "Enquête publique"),
                        ],
                        default=None,
                        max_length=20,
                        verbose_name="Type de demande",
                    ),
                ),
                (
                    "camac_number",
                    geocity.apps.permits.fields.ConditionalIntegerField(
                        help_text="Identifiant donné par la CAMAC vaudoise",
                        null=True,
                        verbose_name="N° CAMAC",
                    ),
                ),
                (
                    "shortname",
                    models.CharField(max_length=15, verbose_name="Identifiant"),
                ),
                (
                    "object",
                    models.CharField(
                        help_text="Résumé succinct du dossier",
                        max_length=180,
                        verbose_name="Objet",
                    ),
                ),
                (
                    "competency",
                    geocity.apps.permits.fields.EmptyBlankChoiceCharField(
                        choices=[
                            ("m", "Municipale (M)"),
                            ("me", "Municipale + État (ME)"),
                        ],
                        default=None,
                        help_text="Responsabilité ou pouvoir d'agir dans un domaine spécifique",
                        max_length=20,
                        verbose_name="Compétence",
                    ),
                ),
                (
                    "nature",
                    geocity.apps.permits.fields.EmptyBlankChoiceCharField(
                        choices=[
                            ("adjonction", "Adjonction"),
                            ("agrandissement", "Agrandissement"),
                            (
                                "changement",
                                "Changement / nouvelle destination des locaux",
                            ),
                            ("construction", "Construction nouvelle"),
                            ("demolition_partielle", "Démolition partielle"),
                            ("demolition_totale", "Démolition totale"),
                            (
                                "reconstruction_demolition",
                                "Reconstruction après démolition",
                            ),
                            (
                                "reconstruction_incendie",
                                "Reconstruction après incendie",
                            ),
                            ("renovation_totale", "Rénovation totale"),
                            ("transformation", "Transformation(s)"),
                        ],
                        help_text="Nature générale de la demande",
                        max_length=40,
                        verbose_name="Nature",
                    ),
                ),
                (
                    "transformation_description",
                    geocity.apps.permits.fields.ConditionalCharField(
                        blank=True,
                        max_length=50,
                        verbose_name="Transformation: description des travaux",
                    ),
                ),
                (
                    "transformation_type",
                    geocity.apps.permits.fields.ConditionalChoicesCharField(
                        blank=True,
                        choices=[
                            ("agrandissement_chauffé", "Agrandissement chauffé"),
                            (
                                "agrandissement_non_chauffé",
                                "Agrandissement non chauffé",
                            ),
                            (
                                "assainissement_énergétique",
                                "Assainissement énergétique",
                            ),
                            (
                                "assainissement_chauffage",
                                "Assainissement système de chauffage",
                            ),
                            ("changement_affectation", "Changement d'affectation"),
                            (
                                "solaire_photovoltaïque",
                                "Installation solaire photovoltaïque",
                            ),
                            ("solaire_thermique", "Installation solaire thermique"),
                            (
                                "renovations_intérieures",
                                "Transformations / rénovations intérieures",
                            ),
                            ("autres", "Autres travaux de transformation"),
                        ],
                        max_length=50,
                        verbose_name="Transformation: Type de travaux",
                    ),
                ),
                (
                    "work_type",
                    geocity.apps.permits.fields.MultipleChoiceCharField(
                        help_text="Identification plus précise des charactériques du dossier",
                        max_length=200,
                        verbose_name="Type d'ouvrage",
                    ),
                ),
                (
                    "construction_type",
                    geocity.apps.permits.fields.EmptyBlankChoiceCharField(
                        choices=[
                            ("building", "Bâtiment"),
                            (
                                "outdoor",
                                "Ouvrage de génie civil, aménagement extérieur",
                            ),
                            ("special", "Construction particulière"),
                        ],
                        default="building",
                        help_text="Nature générale de la demande",
                        max_length=40,
                        verbose_name="Genre de construction",
                    ),
                ),
                (
                    "zone_name",
                    models.CharField(
                        help_text="Nom faisant référence à une dénomination spécifique dans un plan d'urbanisme.",
                        max_length=100,
                        verbose_name="Nom et/ou No de la zone",
                    ),
                ),
                (
                    "zone_plan",
                    geocity.apps.permits.fields.EmptyBlankChoiceCharField(
                        choices=[
                            ("pga", "PGA - Plan Général d'Affectation"),
                            ("ppa", "PPA - Plan Partiel d'Affectation"),
                            ("pq_pqcm", "PQ/PQCM - Plan de Quartier"),
                        ],
                        default="pga",
                        help_text="Sert à organiser l'aménagement du territoire, prévenir les risques et définir des orientations spécifiques pour le développement local.",
                        max_length=40,
                        verbose_name="Plan",
                    ),
                ),
                (
                    "project_owner_type",
                    geocity.apps.permits.fields.EmptyBlankChoiceSmallIntegerField(
                        choices=[
                            (0, "Banque, fond immobilier, holding financière"),
                            (1, "Caisse maladie, SUVA"),
                            (2, "Canton"),
                            (3, "CFF"),
                            (4, "Chemin de fer privé"),
                            (5, "Commune"),
                            (6, "Coopérative de logement"),
                            (
                                7,
                                "Département fédéral de la défense, de la protection de la population et des sports",
                            ),
                            (8, "Entreprise de droit public d'un canton"),
                            (
                                9,
                                "Entreprise de droit public de la commune (transports publics, gaz, eau, électricité, etc.)",
                            ),
                            (10, "Institution de prévoyance, caisse de pension"),
                            (11, "La Poste"),
                            (
                                12,
                                "Office fédéral des constructions et de la logistique (OFCL) ou Domaine des EPF",
                            ),
                            (13, "Office fédéral des routes (OFROU)"),
                            (14, "Organisation internationale, ambassade"),
                            (15, "Particulier et hoirie"),
                            (
                                16,
                                "Société d'assurance (sans les caisses de pension et les caisses maladies)",
                            ),
                            (
                                17,
                                "Société de capitaux, SA, Sàrl (construction, immobilier)",
                            ),
                            (
                                18,
                                "Société de capitaux, SA, Sàrl (industrie, artisanat, commerce)",
                            ),
                            (
                                19,
                                "Société individuelle ou de personnes (construction, immobilier)",
                            ),
                            (
                                20,
                                "Société individuelle ou de personnes (industrie, artisanat, commerce)",
                            ),
                            (21, "Swisscom"),
                            (22, "Usine à gaz privée"),
                            (23, "Usine d'électricité privée"),
                            (
                                24,
                                "Autre maître d'ouvrage privé (église, fondation, association, etc.)",
                            ),
                        ],
                        verbose_name="Maître d'ouvrage",
                    ),
                ),
                (
                    "parcel_numbers",
                    django.contrib.postgres.fields.ArrayField(
                        base_field=models.CharField(
                            max_length=100,
                            validators=[
                                django.core.validators.RegexValidator(
                                    "^(\\d+|\\(\\d+\\)|DP \\d+)$"
                                )
                            ],
                        ),
                        help_text="N° de parcelles concernées par la demande, séparées par des virgules. Exemple: 1234, DP 5678, (345)",
                        size=None,
                        verbose_name="N° de parcelles",
                    ),
                ),
                (
                    "eca_number",
                    geocity.apps.permits.fields.ConditionalCharField(
                        max_length=10, verbose_name="N° ECA"
                    ),
                ),
                ("east_coordinate", models.IntegerField(verbose_name="Coordonnée Est")),
                (
                    "north_coordinate",
                    models.IntegerField(verbose_name="Coordonnée Nord"),
                ),
                (
                    "is_outside_building_zone",
                    models.BooleanField(
                        choices=[(True, "Oui"), (False, "Non")],
                        default=False,
                        verbose_name="Travaux situés hors d'une zone à bâtir",
                    ),
                ),
                (
                    "is_parcel_outside_building_zone",
                    geocity.apps.permits.fields.ConditionalBooleanField(
                        choices=[(True, "Oui"), (False, "Non")],
                        default=None,
                        help_text="Parcelle(s) située(s) partiellement ou entièrement hors des zones à bâtir (zone agricole, viticole, alpestre, aire forestière, zone intermédiaire, zone de verdure, zone spéciale art. 50a LATC, zone agricole spécialisée art. 52a LATC, zone des hameaux, etc.)",
                        null=True,
                        verbose_name="Parcelle(s) située(s) partiellement ou entièrement hors des zones à bâtir",
                    ),
                ),
                (
                    "is_work_outside_building_zone",
                    geocity.apps.permits.fields.ConditionalBooleanField(
                        choices=[(True, "Oui"), (False, "Non")],
                        default=None,
                        help_text="Travaux situés hors zone à bâtir (si constructions, installations (y compris de chantier), démolitions, changements d'affectation, conduites, fondations, mouvements de terre, accès, dépôts, etc. sont prévus, même partiellement, hors zone à bâtir)",
                        null=True,
                        verbose_name="Parcelle(s) située(s) partiellement ou entièrement hors des zones à bâtir",
                    ),
                ),
                (
                    "administrative_entity",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="polco_submissions",
                        to="accounts.administrativeentity",
                        verbose_name="entité administrative",
                    ),
                ),
                (
                    "owner",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="owned_polco_submissions",
                        to="permits.polcocontact",
                        verbose_name="Propriétaire",
                    ),
                ),
                (
                    "project_owner",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="project_owned_polco_submissions",
                        to="permits.polcocontact",
                        verbose_name="Maître d'ouvrage (contact)",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
