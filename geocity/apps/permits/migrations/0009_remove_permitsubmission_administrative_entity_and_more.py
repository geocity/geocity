# Generated by Django 4.2.16 on 2024-12-09 14:36

import django.db.models.deletion
from django.db import migrations, models

import geocity.apps.permits.fields


class Migration(migrations.Migration):

    dependencies = [
        ("submissions", "0045_apply_polymorphism"),
        ("permits", "0008_alter_permitsubmission_camac_number_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="permitsubmission",
            name="administrative_entity",
        ),
        migrations.RemoveField(
            model_name="permitsubmission",
            name="id",
        ),
        migrations.AddField(
            model_name="permitsubmission",
            name="polymorphicsubmission_ptr",
            field=models.OneToOneField(
                auto_created=True,
                default=None,
                on_delete=django.db.models.deletion.CASCADE,
                parent_link=True,
                primary_key=True,
                serialize=False,
                to="submissions.polymorphicsubmission",
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="permitsubmission",
            name="status",
            field=models.PositiveSmallIntegerField(
                choices=[(0, "Brouillon"), (1, "Envoyée, en attente de traitement")],
                default=1,
                verbose_name="état",
            ),
        ),
        migrations.AlterField(
            model_name="permitsubmission",
            name="object",
            field=models.CharField(max_length=180, verbose_name="Description"),
        ),
        migrations.AlterField(
            model_name="permitsubmission",
            name="submission_type",
            field=models.CharField(
                choices=[
                    ("public_enquiry", "Enquête publique"),
                    ("prior_analysis", "Analyse préalable"),
                ],
                default="public_enquiry",
                max_length=20,
                verbose_name="Objet de la demande",
            ),
        ),
        migrations.AlterField(
            model_name="permitsubmission",
            name="work_type",
            field=geocity.apps.permits.fields.EmptyBlankChoiceSmallIntegerField(
                choices=[
                    (
                        "Agriculture, sylviculture",
                        [
                            (0, "Améliorations foncières"),
                            (1, "Construction agricole"),
                            (2, "Construction sylvicole"),
                        ],
                    ),
                    (
                        "Approvisionnement en eau / énergie",
                        [
                            (3, "Alimentation en eau"),
                            (4, "Chauffage à distance"),
                            (
                                5,
                                "Installations d'évacuation et de traitement des eaux usées",
                            ),
                            (6, "Usines à gaz, réseaux et installations chimiques"),
                            (7, "Usines d'électricité et réseaux"),
                            (8, "Autre"),
                        ],
                    ),
                    (
                        "Autre transport et communication",
                        [
                            (9, "Constructions pour chemins de fer (yc. gare)"),
                            (10, "Constructions pour la navigation"),
                            (11, "Constructions pour les bus et tramway"),
                            (
                                12,
                                "Constructions pour les communications (yc. antenne téléphonie)",
                            ),
                            (13, "Constructions pour les transports aériens"),
                            (14, "Autres constructions vouées aux transports"),
                        ],
                    ),
                    (
                        "Autres infrastructures",
                        [
                            (15, "Aménagement de berges et barrages"),
                            (
                                16,
                                "Bâtiment à plusieurs logements pour l'habitation exclusivement",
                            ),
                            (
                                17,
                                "Bâtiment à usage mixte, principalement à usage d'habitation",
                            ),
                            (18, "Constructions pour la défense nationale"),
                            (19, "Foyer sans soins médicaux et/ou assistance sociale"),
                            (20, "Garage, place de parc en rapport avec l'habitation"),
                            (21, "Maison individuelle à un logement, isolée"),
                            (
                                22,
                                "Maison individuelle à un logement, mitoyenne ou jumelle",
                            ),
                            (23, "Autre construction en rapport avec l'habitation"),
                            (24, "Autres infrastructures"),
                        ],
                    ),
                    (
                        "Education, recherche, santé, loisirs, culture",
                        [
                            (
                                25,
                                "Bâtiments à but culturel, musées, bibliothèques et monuments",
                            ),
                            (
                                26,
                                "Ecole, système d'éducation (jusqu'au niveau maturité)",
                            ),
                            (27, "Eglise et bâtiment à but religieux"),
                            (28, "Formation supérieure ou recherche"),
                            (29, "Foyer avec soins médicaux et/ou assistance sociale"),
                            (30, "Hôpital"),
                            (31, "Installation de loisirs et de tourisme"),
                            (32, "Salles omnisports et salles de sport"),
                            (33, "Autre établissement de santé spécialisé"),
                        ],
                    ),
                    (
                        "Elimination des déchets",
                        [(34, "Ordures ménagères"), (35, "Autres déchets")],
                    ),
                    (
                        "Industrie, artisanat, commerce, services, administration",
                        [
                            (36, "Autres hébergements de courte durée"),
                            (37, "Bât. administratif, bureaux"),
                            (38, "Bât. commercial, magasin"),
                            (
                                39,
                                "Etablissements tels que hôtel, café-restaurant, café-bar, tea-room, etc...",
                            ),
                            (40, "Fabrique, usine, atelier"),
                            (41, "Halle, dépôt, silo, citerne"),
                            (
                                42,
                                "Autre construction destinée à des activités économiques",
                            ),
                        ],
                    ),
                    (
                        "Infrastructure routière, parking",
                        [
                            (43, "Parking couvert"),
                            (44, "Route, place de stationnement"),
                            (45, "Routes cantonales"),
                            (46, "Routes communales"),
                            (47, "Routes nationales"),
                        ],
                    ),
                ],
                default=None,
                verbose_name="Type d'ouvrage",
            ),
        ),
    ]
