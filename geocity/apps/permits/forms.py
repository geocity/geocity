import mimetypes

from constance import config
from django import forms
from django.forms import ClearableFileInput
from django.utils import timezone
from django.utils.functional import lazy
from django.utils.translation import gettext_lazy as _

from ...fields import AddressWidget
from ..accounts.models import AdministrativeEntityQuerySet, SubmissionDepartment
from ..accounts.users import get_departments
from ..submissions import services
from ..submissions.forms import (
    BaseSubmissionValidationForm,
    SubmissionValidationDepartmentSelectionForm,
    disable_form,
)
from ..submissions.models import SubmissionValidation
from . import models


class PermitContactForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["address"].widget = AddressWidget(
            autocomplete_options={
                "zipcodeField": f"{self.prefix}-zipcode",
                "cityField": f"{self.prefix}-city",
            }
        )
        self.fields["country"].initial = "CH"

    class Meta:
        model = models.PermitContact
        fields = (
            "first_name",
            "last_name",
            "company_name",
            "address",
            "zipcode",
            "city",
            "country",
            "email",
            "phone",
        )

    def clean(self):
        cleaned_data = super().clean()
        if not all(
            [bool(cleaned_data.get("first_name")), bool(cleaned_data.get("last_name"))]
        ):
            if not cleaned_data["company_name"]:
                raise forms.ValidationError(
                    _("Le nom et prénom ou la raison sociale doivent être renseignés.")
                )
        return cleaned_data


class PermitProjectOwnerContactForm(PermitContactForm):
    section = "project_owner"
    prefix = "project_owner"
    template_name = "django/forms/horizontal.html"


class PermitOwnerContactForm(PermitContactForm):
    section = "owner"
    prefix = "owner"
    template_name = "permits/_permit_submission_owner_form.html"

    is_owner_same_as_project_owner = forms.BooleanField(
        label=_("Le propriétaire et le maître d'ouvrage sont les mêmes acteurs"),
        initial=True,
        required=False,
    )

    def __init__(self, *args, is_saving=False, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if self.data and (
                not is_saving
                or self.data.get(self.prefix + "-is_owner_same_as_project_owner")
                == "on"
            ):
                field.required = False

    def contact_fields(self):
        """
        Return a list of BoundField objects that aren't hidden fields.
        The opposite of the hidden_fields() method.
        """
        return [
            field
            for field in self
            if not field.is_hidden and field.name != "is_owner_same_as_project_owner"
        ]

    def clean(self):
        cleaned_data = super(PermitContactForm, self).clean()
        # Clean fields only if not same as project owner
        if not cleaned_data.get("is_owner_same_as_project_owner"):
            return super().clean()

    def should_hide_contact_fields(self):
        return (
            not self.data
            or self.data.get("owner-is_owner_same_as_project_owner") == "on"
        )

    class Meta:
        model = models.PermitContact
        fields = ("is_owner_same_as_project_owner",) + PermitContactForm.Meta.fields


class PermitSubmissionForm(forms.ModelForm):
    template_name = "django/forms/horizontal.html"

    class Meta:
        model = models.PermitSubmission
        fields = "__all__"

    def __init__(self, *args, is_saving=False, **kwargs):
        self.readonly_fields = kwargs.pop("readonly_fields", [])

        super().__init__(*args, **kwargs)

        for field in self.readonly_fields:
            if field in self.fields:
                self.fields[field].widget.attrs["readonly"] = True
                self.fields[field].widget.attrs["disabled"] = True
                self.fields[field].required = False
                self.fields[field].disabled = True

        if not getattr(self, "section"):
            raise RuntimeError(
                "The section attribute must be set on PermitSubmissionForm child class."
            )

        if is_saving:
            self.disable_required_on_unmet_conditional_fields()

    def _set_required_on_unmet_conditional_fields(self, required):
        for field_name, field in self.fields.items():
            parent_field = field.widget.attrs.get("data-depends-on")
            if parent_field:
                value = self.data.get(parent_field)
                if value not in field.widget.attrs.get("data-expected-values").split(
                    ","
                ):
                    field.required = required

    def disable_required_on_unmet_conditional_fields(self):
        self._set_required_on_unmet_conditional_fields(False)

    def enable_required_on_met_conditional_fields(self):
        self._set_required_on_unmet_conditional_fields(True)

    def visible_fields(self):
        fields = []
        for field in super().visible_fields():
            attrs = field.field.widget.attrs
            classes = attrs.get("class", [])

            field_def = next(
                (
                    f
                    for f in models.PermitSubmission._get_flat_fields()
                    if f.get("name") == field.name
                ),
                {},
            )

            args = field_def.get("args", {})
            expected_values = args.get("expected_values")
            depends_on = args.get("depends_on")

            if "conditional-field" in classes and depends_on and expected_values:
                value = self.data.get(depends_on)
                initial_value = self.fields.get(depends_on)
                if initial_value:
                    initial_value = initial_value.initial

                if value is None:
                    # Edit mode, existing value hides
                    existing_value = getattr(self.instance, depends_on, None)
                    if existing_value:
                        if existing_value not in expected_values:
                            field.extra_classes = ("hidden",)
                    # Create mode, initial value hides
                    else:
                        if initial_value not in expected_values:
                            field.extra_classes = ("hidden",)
                else:
                    # POSTING (edit/create), posted value hides
                    if value not in expected_values:
                        field.extra_classes = ("hidden",)

            fields.append(field)

        return fields


class PermitSubmissionParticipantsForm(PermitSubmissionForm):
    section = "participants"

    class Meta(PermitSubmissionForm.Meta):
        fields = list(
            field["name"]
            for field in models.PERMIT_SUBMISSION_FIELDS.get("participants")
        )


class PermitSubmissionAttributesForm(PermitSubmissionForm):
    section = "attributes"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["north_coordinate"].widget.attrs.update(
            {
                "min": models.PERMIT_NORTH_MIN_COORD,
                "max": models.PERMIT_NORTH_MAX_COORD,
            }
        )
        self.fields["east_coordinate"].widget.attrs.update(
            {
                "min": models.PERMIT_EAST_MIN_COORD,
                "max": models.PERMIT_EAST_MAX_COORD,
            }
        )

    class Meta(PermitSubmissionForm.Meta):
        fields = list(
            field["name"] for field in models.PERMIT_SUBMISSION_FIELDS.get("attributes")
        )

        widgets = {
            "is_outside_building_zone": forms.RadioSelect,
            "is_parcel_outside_building_zone": forms.RadioSelect,
            "is_work_outside_building_zone": forms.RadioSelect,
            "zone_plan": forms.RadioSelect,
        }


class PermitSubmissionClassificationForm(PermitSubmissionForm):
    section = "classification"

    def __init__(
        self,
        *args,
        administrative_entities: AdministrativeEntityQuerySet = None,
        is_saving=False,
        **kwargs,
    ):
        kwargs.update(is_saving=is_saving)
        super().__init__(*args, **kwargs)
        if administrative_entities:
            self.fields["administrative_entity"].queryset = administrative_entities

            if administrative_entities.count() == 1:
                self.fields[
                    "administrative_entity"
                ].initial = administrative_entities.get()

    class Meta(PermitSubmissionForm.Meta):
        widgets = {
            "administrative_entity": forms.RadioSelect,
            "submission_type": forms.RadioSelect,
            "competency": forms.RadioSelect,
        }

        fields = ("administrative_entity",) + tuple(
            field["name"]
            for field in models.PERMIT_SUBMISSION_FIELDS.get("classification")
        )


class PermitSubmissionGeneralForm(PermitSubmissionForm):
    section = "general_data"

    def __init__(
        self,
        *args,
        is_saving=False,
        **kwargs,
    ):
        kwargs.update(is_saving=is_saving)
        super().__init__(*args, **kwargs)

    class Meta(PermitSubmissionForm.Meta):
        widgets = {
            "work_type": forms.RadioSelect,
            "construction_type": forms.RadioSelect,
            "object": forms.Textarea(
                attrs={
                    "rows": 3,
                }
            ),
        }

        fields = tuple(
            field["name"]
            for field in models.PERMIT_SUBMISSION_FIELDS.get("general_data")
        )


def get_allowed_mimetypes():

    file_extensions = config.ALLOWED_FILE_EXTENSIONS.split(",")
    allowed_mimetypes = [
        mimetypes.types_map[f".{item.strip()}"] for item in file_extensions
    ]
    return ", ".join(allowed_mimetypes)


class PermitSubmissionDocumentsForm(forms.ModelForm):
    authorised_departments = forms.ModelMultipleChoiceField(
        label=_("Départements autorisés"),
        queryset=None,
        widget=forms.CheckboxSelectMultiple,
        required=True,
    )

    class Meta:
        model = models.PermitSubmissionDocument
        fields = ("document", "description", "authorised_departments")
        # The allowed mimetypes have to be loaded lazily. If not, Constance triggers an error
        # because the database is not ready at the time this module is loaded.
        widgets = {
            "document": ClearableFileInput(
                attrs={"accept": lazy(get_allowed_mimetypes, str), "required": True}
            ),
            "description": forms.TextInput(attrs={"required": True}),
        }

    def __init__(self, user, permit_submission, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.empty_permitted = False
        self.fields[
            "authorised_departments"
        ].queryset = SubmissionDepartment.objects.filter(
            administrative_entity=permit_submission.administrative_entity
        ).all()

        self.fields["authorised_departments"].initial = get_departments(user)


class PermitSubmissionFormalExaminationForm(forms.ModelForm):
    formal_examination_report = forms.CharField(
        label=_("Compte rendu"),
        widget=forms.Textarea(attrs={"rows": 3}),
        required=False,
        # As we auto-save the changes in the field and refresh the DOM when
        # the requests comes back, it’s important to keep whitespaces so the
        # user cursor doesn’t move unexpectedly
        strip=False,
    )
    formal_examination_completed = forms.BooleanField(
        label=_("Examen formel terminé"),
        required=False,
    )

    class Meta:
        model = models.PermitSubmission
        fields = ["formal_examination_report", "formal_examination_completed"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and self.instance.progress:
            self.fields[
                "formal_examination_report"
            ].disabled = self.instance.progress.formal_examination
            self.fields[
                "formal_examination_completed"
            ].initial = self.instance.progress.formal_examination

    def save(self, commit=True):
        permit_submission = super().save(commit=False)

        if commit:
            permit_submission.save()
            progress = models.PermitSubmissionProgress.objects.get(
                permit_submission=permit_submission
            )
            progress.formal_examination = self.cleaned_data[
                "formal_examination_completed"
            ]
            progress.save()

        return permit_submission


class PermitSubmissionTechnicalExaminationForm(forms.ModelForm):
    technical_examination = forms.BooleanField(
        label=_("Examen technique terminé"),
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            # Disable the field
            if not all(
                val.has_been_answered()
                for val in self.instance.permit_submission.validations.all()
            ):
                self.fields["technical_examination"].disabled = True
                self.fields["technical_examination"].help_text = _(
                    "Vous ne pouvez pas terminer l'examen technique tant que des validations sont en cours."
                )

    class Meta:
        model = models.PermitSubmissionProgress
        fields = ["technical_examination"]


class PermitSubmissionValidationDepartmentSelectionForm(
    SubmissionValidationDepartmentSelectionForm
):
    def __init__(self, *args, request=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request
        if not self.submission.can_be_sent_for_validation():
            disable_form(self)

    def save(self):
        services.request_submission_validation(
            self.submission,
            self.cleaned_data["departments"],
            self.request.build_absolute_uri,
        )
        if (
            self.submission.status
            == models.PermitSubmission.STATUS_SUBMITTED_FOR_VALIDATION
        ):
            self.submission.status = models.PermitSubmission.STATUS_AWAITING_VALIDATION
            self.save()


class PermitSubmissionValidationForm(BaseSubmissionValidationForm):
    def __init__(self, user, submission, request, *args, **kwargs):
        super().__init__(user, submission, *args, **kwargs)
        self.user = user
        self.submission = submission
        self.request = request
        self.fields["validation_status"].choices = [
            (
                value,
                label,
            )
            for value, label in self.fields["validation_status"].choices
            if value not in [SubmissionValidation.STATUS_REQUESTED]
        ]
        if self.submission.progress.technical_examination:
            disable_form(self)

    def save(self, commit=True):
        validation_object = SubmissionValidation.objects.filter(
            submission_id=self.submission.id,
            validated_by_id=self.user.id,
        )
        initial_validation_status = (
            (validation_object.first().validation_status)
            if validation_object.exists()
            else SubmissionValidation.STATUS_REQUESTED
        )
        self.instance.comment_is_visible_by_author = False
        self.instance.comment_is_visible_in_reports = False
        self.instance.validated_at = timezone.now()
        self.instance.validated_by = self.user
        validation = super().save(commit)

        if validation.validation_status == SubmissionValidation.STATUS_APPROVED:
            validation_message = _("Le préavis positif a été enregistré.")
        elif validation.validation_status == SubmissionValidation.STATUS_REJECTED:
            validation_message = _("Le préavis négatif a été enregistré.")
        else:
            validation_message = _("Le commentaire a été enregistré.")

        if not self.submission.get_pending_validations():
            initial_permit_status = self.submission.status
            self.submission.status = models.PermitSubmission.STATUS_PROCESSING
            self.submission.save()

            if (
                initial_permit_status
                is models.PermitSubmission.STATUS_AWAITING_VALIDATION
                or (
                    initial_permit_status is models.PermitSubmission.STATUS_PROCESSING
                    and initial_validation_status is not self.instance.validation_status
                )
            ):
                if (
                    self.instance.validation_status
                    == SubmissionValidation.STATUS_ASK_FOR_SUPPLEMENTS
                ):
                    subject = _(
                        "Les services chargés de la validation d’une demande ont demandé des compléments"
                    )
                    template = "submission_validated_complements.txt"
                else:
                    subject = _(
                        "Les services chargés de la validation d’une demande ont donné leur préavis"
                    )
                    template = "submission_validated.txt"
                data = {
                    "subject": "{} ({})".format(
                        subject,
                        self.submission.get_forms_names_list(),
                    ),
                    "users_to_notify": self.submission.get_backoffice_email(),
                    "template": template,
                    "submission": self.submission,
                    "absolute_uri_func": self.request.build_absolute_uri,
                }
                services.send_email_notification(data)
            else:
                self.submission.status = (
                    models.PermitSubmission.STATUS_AWAITING_VALIDATION
                )
                self.submission.save()

        return validation_message

    class Meta:
        model = SubmissionValidation
        fields = [
            "validation_status",
            "comment",
        ]
        widgets = {
            "validation_status": forms.RadioSelect(),
            "comment": forms.Textarea(attrs={"rows": 3}),
        }


class ChecklistTasksForm(forms.Form):
    def __init__(self, submission_checklist, *args, **kwargs):
        super().__init__(*args, **kwargs)
        choices = [(t.id, t.description) for t in submission_checklist.tasks.all()]
        self.fields["tasks"] = forms.MultipleChoiceField(
            label="",
            widget=forms.CheckboxSelectMultiple,
            choices=choices,
            required=False,
        )
        self.fields["tasks"].initial = [
            task.id for task in submission_checklist.tasks.all() if task.completed
        ]
