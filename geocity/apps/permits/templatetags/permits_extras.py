from django import template
from django.contrib.sites.shortcuts import get_current_site

from geocity.apps.permits import utils

register = template.Library()


@register.simple_tag(takes_context=True)
def is_permits_enabled_for_user(context, user):
    """
    If "Police des Construction" is available for user,
    display a call to action to create a new "PermitSubmission".
    """
    request = context["request"]
    entity_tags = request.GET.getlist("entityfilter")
    site = get_current_site(request)
    return utils.is_permits_enabled_for_user(user, site, entity_tags)


@register.filter
def get_attr(obj, name):
    return getattr(obj, name, None)


@register.filter
def can_edit_permit_submission(permit_submission, user):
    return permit_submission.can_edit_submission(user)
