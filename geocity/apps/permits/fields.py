from constance import config
from django.core.validators import FileExtensionValidator
from django.db import models
from django.db.models import BLANK_CHOICE_DASH
from django.db.models.fields.files import FieldFile
from django.forms import CheckboxSelectMultiple, MultipleChoiceField
from django.urls import reverse

from geocity.fields import PrivateFileSystemStorage


class EmptyBlankChoiceCharField(models.CharField):
    def get_choices(
        self,
        include_blank=True,
        blank_choice=BLANK_CHOICE_DASH,
        limit_choices_to=None,
        ordering=(),
    ):
        return super().get_choices(
            include_blank=include_blank,
            blank_choice=[("", "")],
            limit_choices_to=limit_choices_to,
            ordering=ordering,
        )


class EmptyBlankChoiceSmallIntegerField(models.SmallIntegerField):
    def get_choices(
        self,
        include_blank=True,
        blank_choice=BLANK_CHOICE_DASH,
        limit_choices_to=None,
        ordering=(),
    ):
        return super().get_choices(
            include_blank=include_blank,
            blank_choice=[("", "")],
            limit_choices_to=limit_choices_to,
            ordering=ordering,
        )


class CheckboxMultipleChoiceField(models.Field):
    def __init__(self, *args, multiple_choice_options=None, **kwargs):
        self.multiple_choice_options = multiple_choice_options
        super().__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        char_field = super().formfield(**kwargs)
        choices = [
            (category, [(value, label) for value, label in options])
            for category, options in self.multiple_choice_options.items()
        ]
        field_kwargs = {
            "choices": choices,
            "widget": CheckboxSelectMultiple,
        }

        field_kwargs.update(
            {
                "label": char_field.label,
                "help_text": char_field.help_text,
                "initial": char_field.initial,
            }
        )
        return MultipleChoiceField(**field_kwargs)


class MultipleChoiceCharField(CheckboxMultipleChoiceField, models.CharField):
    pass


class ConditionalField(models.Field):
    def __init__(self, *args, depends_on=None, expected_values=None, **kwargs):
        self.parental_field = depends_on
        self.parental_expected_values = expected_values
        super().__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        """
        Update widget
        """
        field = super().formfield(**kwargs)
        field.required = True
        field.widget.attrs.update(
            {
                "data-depends-on": self.parental_field,
                "data-expected-values": ",".join(
                    [str(ev) for ev in self.parental_expected_values]
                ),
                "class": "conditional-field",
            }
        )
        return field


class ConditionalIntegerField(ConditionalField, models.IntegerField):
    pass


class ConditionalCharField(ConditionalField, models.CharField):
    pass


class ConditionalChoicesCharField(ConditionalField, EmptyBlankChoiceCharField):
    pass


class ConditionalBooleanField(ConditionalField, models.BooleanField):
    pass


class PermitSubmissionDocumentFieldFile(FieldFile):
    @property
    def url(self):
        return reverse(
            "permits:documents_download",
            kwargs={"path": self.name},
        )


class PermitSubmissionDocumentFileExtensionValidator(FileExtensionValidator):
    def _get_allowed_extensions(self):
        if not hasattr(self, "_allowed_extensions"):
            self._allowed_extensions = [
                extension.strip()
                for extension in config.ALLOWED_FILE_EXTENSIONS.split(",")
            ]
        return self._allowed_extensions

    def __call__(self, value):
        # Because the config is loaded from constance,
        # we need to load it at call time, and not when the validator is initialized.
        # If not, constance triggers an error because the database is not ready.
        self.allowed_extensions = self._get_allowed_extensions()

        return super().__call__(value)


class PermitSubmissionDocumentFileField(models.FileField):
    """
    FileField storing information in a private media root.
    """

    attr_class = PermitSubmissionDocumentFieldFile

    def __init__(self, verbose_name=None, name=None, **kwargs):
        kwargs["storage"] = PrivateFileSystemStorage()
        super().__init__(verbose_name, name, **kwargs)

        self.validators = [PermitSubmissionDocumentFileExtensionValidator()]
