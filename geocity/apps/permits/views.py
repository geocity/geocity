from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import BadRequest, ValidationError
from django.forms import formset_factory
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, TemplateView

from geocity.apps.accounts.decorators import (
    check_mandatory_2FA,
    permanent_user_required,
)
from geocity.apps.permits.forms import (
    ChecklistTasksForm,
    PermitOwnerContactForm,
    PermitProjectOwnerContactForm,
    PermitSubmissionAttributesForm,
    PermitSubmissionClassificationForm,
    PermitSubmissionDocumentsForm,
    PermitSubmissionFormalExaminationForm,
    PermitSubmissionGeneralForm,
    PermitSubmissionParticipantsForm,
    PermitSubmissionTechnicalExaminationForm,
    PermitSubmissionValidationDepartmentSelectionForm,
    PermitSubmissionValidationForm,
    get_allowed_mimetypes,
)
from geocity.apps.permits.steps import PermitStepType, get_steps_for_stepper
from geocity.apps.permits.utils import (
    find_form_by_field_name,
    get_permit_submission_for_user_or_404,
    get_selectable_entities,
    save_permit_submission,
)

from ..accounts.users import get_departments
from ..submissions import services
from .models import (
    PERMIT_SUBMISSION_FIELDS,
    PROJECT_OWNER_TYPES,
    PermitSubmission,
    PermitSubmissionDocument,
    PermitSubmissionTypes,
)
from .permissions import (
    has_permission_to_create_permit_submissions_for_entity,
    has_permission_to_edit_permit_submission,
    has_permission_to_edit_permit_submissions_for_entity,
)
from .tables import PermitSubmissionDocumentsTable


class PermitSubmissionFormViewMixin:
    action = None

    def dispatch(self, request, *args, **kwargs):
        current_site = get_current_site(request)
        entity_tags = request.GET.getlist("entityfilter")

        self._administrative_entities = get_selectable_entities(
            user=request.user,
            site=current_site,
            entity_tags=entity_tags,
            action=self.action,
        )
        if self.action != "create":
            pk = kwargs.get("pk", None)
            self.permit_submission = get_permit_submission_for_user_or_404(
                request.user, pk, action=self.action
            )
        else:
            self.permit_submission = None
            if not any(
                has_permission_to_create_permit_submissions_for_entity(
                    request.user, adm
                )
                for adm in self._administrative_entities.all()
            ):
                raise Http404

        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        # Set in dispatch for edition (None on creation)
        permit_submission = self.permit_submission

        # Initialize the forms
        ctx = {
            "permit_submission": permit_submission,
            "forms": {
                "classification": PermitSubmissionClassificationForm(
                    administrative_entities=self._administrative_entities,
                    instance=permit_submission,
                    readonly_fields=(
                        ["administrative_entity", "submission_type"]
                        if permit_submission
                        else []
                    ),
                ),
                "general_data": PermitSubmissionGeneralForm(instance=permit_submission),
                "attributes": PermitSubmissionAttributesForm(
                    instance=permit_submission
                ),
            },
        }

        # Only for creation
        if not permit_submission:
            ctx["forms"].update(
                {
                    "participants": PermitSubmissionParticipantsForm(),
                    "project_owner": PermitProjectOwnerContactForm(),
                    "owner": PermitOwnerContactForm(),
                }
            )

        context.update(ctx)

        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context.update({"permit_submission": self.permit_submission, "forms": {}})
        data = request.POST

        if data:
            forms = self.get_forms(data)
            cleaned_data = {}
            is_all_valid = True

            for form_id, form in forms.items():
                context["forms"][form_id] = form
                if form.is_valid():
                    cleaned_data.update(form.cleaned_data)
                else:
                    is_all_valid = False
                form.enable_required_on_met_conditional_fields()

            # On creation only
            if self.permit_submission:
                contacts_cleaned_data = None
            else:
                contacts_cleaned_data = {}
                contact_forms = self.get_contact_forms(data)
                for form_id, form in contact_forms.items():
                    context["forms"][form_id] = form
                    if form.is_valid():
                        contacts_cleaned_data[form_id] = form.cleaned_data
                    else:
                        is_all_valid = False

            if is_all_valid:
                try:
                    submission = save_permit_submission(
                        author=request.user,
                        data=cleaned_data,
                        contacts_cleaned_data=contacts_cleaned_data,
                        instance=self.permit_submission,
                    )
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        _("La demande de permis a été créée avec succès."),
                    )
                    return redirect(
                        "permits:permit_submission_detail:current", pk=submission.pk
                    )
                except ValidationError as e:
                    if hasattr(e, "error_dict"):
                        for field, errors in e.error_dict.items():
                            form_id = find_form_by_field_name(forms, field)
                            context["forms"][form_id].add_error(field, errors)
                    else:
                        messages.add_message(request, messages.ERROR, e.message)

            context.update({"forms_have_errors": True})

        return self.render_to_response(context)


@method_decorator(login_required, name="dispatch")
@method_decorator(permanent_user_required, name="dispatch")
@method_decorator(check_mandatory_2FA, name="dispatch")
class PermitSubmissionCreateView(PermitSubmissionFormViewMixin, TemplateView):
    template_name = "permits/permit_submission_form.html"
    action = "create"

    def get_forms(self, data):
        return {
            PermitSubmissionClassificationForm.section: PermitSubmissionClassificationForm(
                administrative_entities=self._administrative_entities,
                data=data,
                is_saving=True,
            ),
            PermitSubmissionGeneralForm.section: PermitSubmissionGeneralForm(
                data=data,
                is_saving=True,
            ),
            PermitSubmissionParticipantsForm.section: PermitSubmissionParticipantsForm(
                data=data,
                is_saving=True,
            ),
            PermitSubmissionAttributesForm.section: PermitSubmissionAttributesForm(
                data=data,
                is_saving=True,
            ),
        }

    def get_contact_forms(self, data):
        return {
            PermitProjectOwnerContactForm.section: PermitProjectOwnerContactForm(
                data=data
            ),
            PermitOwnerContactForm.section: PermitOwnerContactForm(
                data=data, is_saving=True
            ),
        }


@method_decorator(login_required, name="dispatch")
@method_decorator(permanent_user_required, name="dispatch")
@method_decorator(check_mandatory_2FA, name="dispatch")
class PermitSubmissionEditView(PermitSubmissionFormViewMixin, TemplateView):
    template_name = "permits/permit_submission_form.html"
    action = "edit"

    def get_forms(self, data):
        forms = {
            PermitSubmissionClassificationForm.section: PermitSubmissionClassificationForm(
                administrative_entities=self._administrative_entities,
                data=data,
                is_saving=True,
                instance=self.permit_submission,
                readonly_fields=("administrative_entity", "submission_type"),
            ),
            PermitSubmissionGeneralForm.section: PermitSubmissionGeneralForm(
                data=data,
                is_saving=True,
                instance=self.permit_submission,
            ),
            PermitSubmissionAttributesForm.section: PermitSubmissionAttributesForm(
                data=data,
                is_saving=True,
                instance=self.permit_submission,
            ),
        }
        return forms


def get_documents_formset_class():
    return formset_factory(form=PermitSubmissionDocumentsForm, extra=1)


@method_decorator(login_required, name="dispatch")
@method_decorator(permanent_user_required, name="dispatch")
@method_decorator(check_mandatory_2FA, name="dispatch")
class PermitSubmissionDetailViewMixin(DetailView):
    template_name = "permits/permit_submission_detail.html"
    model = PermitSubmission

    def is_validator(self, user, submission):
        return (
            user in submission.get_validators().all()
            and not has_permission_to_edit_permit_submission(user, submission)
        )

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.select_related("progress")
        return super().get_object(queryset=queryset)

    def get_active_step(self, context):
        raise NotImplementedError

    def add_checklist_to_context(self, context):
        active_step = self.get_active_step(context)
        if active_step:
            checklist = self.object.get_checklist_for_step(active_step)
            if checklist:
                form = ChecklistTasksForm(checklist)
                context["checklist_form"] = form
                context["checklist_id"] = checklist.id

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        validations = self.object.validations.all()

        context.update(
            {
                "is_readonly_for_validators": self.is_validator(
                    self.request.user, self.object
                ),
                "submission": self.object,
                "fields_by_sections": PERMIT_SUBMISSION_FIELDS.items(),
                "project_owner_types": PROJECT_OWNER_TYPES,
                "steps": get_steps_for_stepper(
                    self.request,
                    self.object,
                    self.request.user,
                ),
                "documents_formset": get_documents_formset_class()(
                    form_kwargs={
                        "user": self.request.user,
                        "permit_submission": self.object,
                    }
                ),
                "documents_table": PermitSubmissionDocumentsTable(
                    data=self.object.get_documents(self.request.user) or []
                ),
                "documents_allowed_mimetypes": get_allowed_mimetypes(),
                "validations": self.object.validations.all(),
                "validation_history": [v.get_validation_history() for v in validations],
                "has_permission_to_ask_for_validation": has_permission_to_edit_permit_submission(
                    self.request.user, self.object
                ),
                "has_permission_to_classify": has_permission_to_edit_permit_submission(
                    self.request.user, self.object
                ),
                "should_display_comment_visibility": False,
            }
        )

        self.add_checklist_to_context(context)
        context["active_step"] = self.get_active_step(context)

        return context

    def dispatch(self, request, *args, **kwargs):
        # Checking that the user has access to the permit submission
        submission = get_permit_submission_for_user_or_404(
            request.user, kwargs.get("pk"), action="view"
        )

        if (
            not has_permission_to_edit_permit_submission(request.user, submission)
            and not submission.can_be_seen_by_validators()
        ):
            raise Http404

        return super().dispatch(request, *args, **kwargs)


class PermitSubmissionDetailView(PermitSubmissionDetailViewMixin):
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        context = self.get_context_data(object=self.object)
        return redirect(
            reverse(
                f"permits:permit_submission_detail:{self.get_active_step(context).value}",
                kwargs={"pk": self.object.pk},
            )
        )

    def get_active_step(self, context):
        active_step = PermitStepType.VALIDATION
        for stepKey, step in context["steps"].items():
            if step.completed is False:
                active_step = stepKey
                break
        return active_step


class PermitSubmissionDocumentUploadView(PermitSubmissionDetailViewMixin):
    def get_active_step(self, context):
        return None

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        documents_formset = get_documents_formset_class()(
            data=request.POST,
            files=request.FILES,
            form_kwargs={"permit_submission": self.object, "user": self.request.user},
        )

        if documents_formset.is_valid():
            for form in documents_formset:
                document = form.save(commit=False)
                document.owner = request.user
                document.permit_submission = PermitSubmission.objects.get(
                    pk=kwargs.get("pk")
                )

                document.save()
                form.save_m2m()
            return redirect(
                "permits:permit_submission_detail:current", pk=kwargs.get("pk")
            )
        else:
            context["documents_formset"] = documents_formset
            return self.render_to_response(context)


class PermitSubmissionDocumentDownloadView(PermitSubmissionDetailViewMixin):
    model = PermitSubmission

    def get(self, request, *args, **kwargs):
        document = get_object_or_404(
            PermitSubmissionDocument, pk=kwargs.get("document_pk")
        )
        return services.download_file(document.document.name)


class PermitSubmissionDocumentRemoveView(PermitSubmissionDetailViewMixin):
    model = PermitSubmission

    def get(self, request, *args, **kwargs):
        document = get_object_or_404(
            PermitSubmissionDocument, pk=kwargs.get("document_pk")
        )
        document.delete()
        return redirect("permits:permit_submission_detail:current", pk=kwargs.get("pk"))


class PublicInquiryStepOnlyMixin:
    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)

        submission = self.object

        if submission.submission_type == PermitSubmissionTypes.PRIOR_ANALYSIS:
            messages.add_message(
                request,
                messages.INFO,
                _("Cette étape n’est pas disponible pour les analyses préalables."),
            )
            return redirect(
                reverse(
                    "permits:permit_submission_detail:current",
                    kwargs={"pk": submission.pk},
                )
            )

        return response


class PermitSubmissionStepViewMixin(PermitSubmissionDetailViewMixin):

    active_step = None

    def get_active_step(self, context):
        return self.active_step


class PermitSubmissionDetailValidationStepView(PermitSubmissionStepViewMixin):
    template_name = "permits/permit_submission_detail_validation.html"
    active_step = PermitStepType.VALIDATION

    def get_validation(self, user, submission, data=None):
        departments = get_departments(user)

        try:
            validation, *rest = list(
                submission.validations.filter(department__in=departments)
            )
        # User is not part of the requested departments
        except ValueError:
            return None

        if rest:
            return None

        return validation

    def post(self, request, *args, **kwargs):
        submission = self.get_object()
        form_name = request.POST.get("form")

        has_permission = has_permission_to_edit_permit_submissions_for_entity(
            request.user, submission.administrative_entity
        )

        if form_name == "validation_response":
            has_permission = self.is_validator(request.user, submission)

        if not has_permission:
            messages.add_message(
                request,
                messages.ERROR,
                _("Vous n’avez pas la permission d’effectuer cette action."),
            )
            return self.get(request)

        if form_name == "formal_examination":
            form = PermitSubmissionFormalExaminationForm(
                request.POST, instance=submission
            )
        elif form_name == "technical_examination":
            form = PermitSubmissionTechnicalExaminationForm(
                request.POST, instance=submission.progress
            )
            if not all(val.has_been_answered() for val in submission.validations.all()):
                raise BadRequest
        elif form_name == "validation_department_selection":
            form = PermitSubmissionValidationDepartmentSelectionForm(
                data=request.POST, instance=submission, request=request
            )
            self.template_name = "permits/_permit_submission_detail_validation_department_submission.html"
        elif form_name == "validation_response":
            validation = self.get_validation(request.user, submission, request.POST)

            form = PermitSubmissionValidationForm(
                instance=validation,
                user=request.user,
                submission=submission,
                request=request,
                data=request.POST,
            )
        else:
            raise BadRequest

        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                _(
                    "La demande de validation a été transmise aux services sélectionnés."
                ),
            )

        return self.get(request)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context.update(
            {
                "active_step": PermitStepType.VALIDATION,
                "formal_examination_form": PermitSubmissionFormalExaminationForm(
                    instance=self.object
                ),
                "technical_examination_form": PermitSubmissionTechnicalExaminationForm(
                    instance=self.object.progress
                ),
            }
        )

        if has_permission_to_edit_permit_submissions_for_entity(
            self.request.user, self.object.administrative_entity
        ):
            context.update(
                {
                    "validation_department_selection_form": PermitSubmissionValidationDepartmentSelectionForm(
                        instance=self.object,
                    )
                }
            )

        if self.is_validator(self.request.user, self.object):
            validation = self.get_validation(
                self.request.user, self.object, data=self.request.POST
            )
            validation_form = PermitSubmissionValidationForm(
                instance=validation,
                user=self.request.user,
                request=self.request,
                submission=self.object,
            )
            if validation_form:
                context.update(
                    {
                        "validation_response_form": validation_form,
                    }
                )

        return context


class PermitSubmissionDetailPublicInquiryStepView(
    PublicInquiryStepOnlyMixin, PermitSubmissionStepViewMixin
):
    template_name = "permits/permit_submission_detail_public_inquiry.html"
    active_step = PermitStepType.PUBLIC_INQUIRY


class PermitSubmissionDetailDecisionStepView(
    PublicInquiryStepOnlyMixin, PermitSubmissionStepViewMixin
):
    template_name = "permits/permit_submission_detail_decision.html"
    active_step = PermitStepType.DECISION


class PermitSubmissionDetailDecisionDispatchStepView(PermitSubmissionStepViewMixin):
    template_name = "permits/permit_submission_detail_decision_dispatch.html"
    active_step = PermitStepType.DECISION_DISPATCH


class PermitSubmissionDetailConstructionStepView(
    PublicInquiryStepOnlyMixin, PermitSubmissionStepViewMixin
):
    template_name = "permits/permit_submission_detail_construction.html"
    active_step = PermitStepType.CONSTRUCTION


class PermitSubmissionDetailVisitsStepView(
    PublicInquiryStepOnlyMixin, PermitSubmissionStepViewMixin
):
    template_name = "permits/permit_submission_detail_visits.html"
    active_step = PermitStepType.VISITS


class PermitSubmissionDetailPermitStepView(
    PublicInquiryStepOnlyMixin, PermitSubmissionStepViewMixin
):
    template_name = "permits/permit_submission_detail_permit.html"
    active_step = PermitStepType.PERMIT
