from django.contrib.auth.models import Group, User
from django.test import TestCase
from django.urls import reverse

from geocity.tests import factories
from geocity.tests.utils import get_parser


class AccountAdminTestCase(TestCase):
    def setUp(self):

        self.superuser = factories.SuperUserFactory()
        self.integrator = factories.IntegratorUserFactory()
        self.pilot = factories.PilotUserFactory()
        self.validator = factories.ValidatorUserFactory()
        # Create 3 integrators
        factories.IntegratorUserFactory.create_batch(3)
        # Create 3 pilots
        factories.PilotUserFactory.create_batch(3)
        # Create 3 validators
        factories.ValidatorUserFactory.create_batch(3)

        # Get emails from DB as expected values for tests
        integrator_groups = Group.objects.filter(
            submission_department__is_integrator_admin=True
        )
        self.integrator_users_mails = ";".join(
            User.objects.filter(groups__in=integrator_groups)
            .values_list("email", flat=True)
            .order_by("email")
        )

        pilot_groups = Group.objects.filter(submission_department__is_backoffice=True)

        self.pilot_users_mails = ";".join(
            User.objects.filter(groups__in=pilot_groups)
            .values_list("email", flat=True)
            .order_by("email")
        )

        validator_groups = Group.objects.filter(
            submission_department__is_validator=True
        )

        self.validator_users_mails = ";".join(
            User.objects.filter(groups__in=validator_groups)
            .values_list("email", flat=True)
            .order_by("email")
        )

    def test_superuser_can_see_mailinglist(self):

        self.client.login(username=self.superuser.username, password="password")

        response = self.client.get(reverse("admin:mailinglist"))

        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertInHTML(self.integrator_users_mails, content)
        self.assertInHTML(self.pilot_users_mails, content)
        self.assertInHTML(self.validator_users_mails, content)

    def test_integrator_cannot_see_mailinglist(self):

        self.client.login(username=self.integrator.username, password="password")

        response = self.client.get(reverse("admin:mailinglist"))
        parser = get_parser(response.content)
        self.assertEqual(len(parser.select("#integratorUsersMails")), 0)

    def test_pilot_cannot_see_mailinglist(self):

        self.client.login(username=self.pilot.username, password="password")

        response = self.client.get(reverse("admin:mailinglist"))
        parser = get_parser(response.content)
        self.assertEqual(len(parser.select("#integratorUsersMails")), 0)

    def test_validator_cannot_see_mailinglist(self):

        self.client.login(username=self.validator.username, password="password")

        response = self.client.get(reverse("admin:mailinglist"))
        parser = get_parser(response.content)
        self.assertEqual(len(parser.select("#integratorUsersMails")), 0)

    def test_mailinglist_is_not_public(self):

        response = self.client.get(reverse("admin:mailinglist"))
        parser = get_parser(response.content)
        self.assertEqual(len(parser.select("#integratorUsersMails")), 0)
