from django.shortcuts import resolve_url
from django.test import TestCase
from django.urls import reverse

from geocity.apps.permits.models import PermitSubmissionTypes
from geocity.tests.permits.factories import PermitSubmissionFactory
from geocity.tests.utils import LoggedInPilotMixin, get_parser


class PermitSubmissionDetailViewTest(LoggedInPilotMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.administrative_entity.is_permits_enabled = True
        self.administrative_entity.save()
        self.permit_submission = PermitSubmissionFactory(
            submission_type=PermitSubmissionTypes.PUBLIC_ENQUIRY,
            administrative_entity=self.administrative_entity,
            shortname="Custom-id",
            author=self.user,
        )

    def test_redirects_to_the_first_incomplete_step(self):
        self.permit_submission.progress.validation = True
        self.permit_submission.progress.save()

        response = self.client.get(
            reverse(
                "permits:permit_submission_detail:current",
                kwargs={"pk": self.permit_submission.pk},
            ),
            follow=True,
        )

        self.assertRedirects(
            response,
            resolve_url(
                "permits:permit_submission_detail:public-inquiry",
                pk=self.permit_submission.pk,
            ),
        )

    def test_follow_up_displays_all_steps_for_public_inquiry_type(self):
        response = self.client.get(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
        )

        parser = get_parser(response.content)
        steps = parser.select(
            "#permit-submission-follow-up-steps .follow-up-step__label"
        )

        actual_labels = [step.get_text(strip=True) for step in steps]
        expected_labels = [
            "Contrôle / Examen",
            "Enquête publique",
            "Décision",
            "Envoi décision",
            "Travaux",
            "Visites",
            "Délivrance du permis",
        ]

        self.assertEqual(actual_labels, expected_labels)

    def test_follow_up_displays_selected_steps_for_public_inquiry_type(self):
        self.permit_submission = PermitSubmissionFactory(
            submission_type=PermitSubmissionTypes.PRIOR_ANALYSIS,
            camac_number=None,
            administrative_entity=self.administrative_entity,
            shortname="Custom-id",
            author=self.user,
        )

        response = self.client.get(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
        )

        parser = get_parser(response.content)
        steps = parser.select(
            "#permit-submission-follow-up-steps .follow-up-step__label"
        )

        actual_labels = [step.get_text(strip=True) for step in steps]
        expected_labels = [
            "Contrôle / Examen",
            "Envoi décision",
        ]

        self.assertEqual(actual_labels, expected_labels)

    def test_follow_up_displays_current_step_substeps(self):
        response = self.client.get(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
        )

        parser = get_parser(response.content)
        sub_steps = parser.select(
            "#permit-submission-follow-up-steps .checklist__label"
        )

        actual_labels = [step.get_text(strip=True) for step in sub_steps]
        expected_labels = [
            "Examen formel",
            "Examen technique",
        ]

        self.assertEqual(actual_labels, expected_labels)
