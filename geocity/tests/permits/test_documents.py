import os

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from geocity.apps.accounts.users import get_departments
from geocity.apps.permits.models import PermitSubmissionTypes
from geocity.apps.permits.tests.utils import base_submission_fields
from geocity.tests.factories import FormFactory
from geocity.tests.permits.factories import PermitSubmissionFactory
from geocity.tests.utils import LoggedInPilotMixin


class PermitSubmissionDocumentsTestCase(LoggedInPilotMixin, TestCase):
    def _base_fields(self):
        base_fields = base_submission_fields()
        base_fields["administrative_entity"] = [self.administrative_entity.pk]
        return base_fields

    def setUp(self):
        super().setUp()
        self.administrative_entity.is_permits_enabled = True
        self.administrative_entity.save()
        FormFactory(
            administrative_entities=[self.administrative_entity], is_public=True
        )
        self.permit_submission = PermitSubmissionFactory(
            submission_type=PermitSubmissionTypes.PUBLIC_ENQUIRY,
            administrative_entity=self.administrative_entity,
            shortname="Custom-id",
            author=self.user,
        )

    def _formset_data(self, user):
        management_form = {
            "form-TOTAL_FORMS": ["1"],
            "form-INITIAL_FORMS": ["0"],
            "form-MIN_NUM_FORMS": ["0"],
            "form-MAX_NUM_FORMS": ["1000"],
            "action": "documents",
            "save_continue": "",
        }
        with open("geocity/tests/files/real_pdf.pdf", "rb") as file:
            uploaded_file = SimpleUploadedFile(
                file.name, file.read(), content_type="application/pdf"
            )

            management_form.update(
                {
                    "form-0-description": ["Single document upload"],
                    "form-0-authorised_departments": [
                        str(get_departments(user).first().pk)
                    ],
                    "form-0-document": [uploaded_file],
                }
            )

        return management_form

    def test_permit_submission_document_is_created(self):
        response = self.client.post(
            reverse(
                "permits:permit_submission_upload",
                kwargs={"pk": self.permit_submission.pk},
            ),
            data=self._formset_data(self.user),
            follow=True,
        )
        assert self.permit_submission.documents.count() == 1
        assert (
            self.permit_submission.documents.first().get_download_url()
            in response.content.decode()
        )

    def test_permit_submission_document_is_removed(self):
        self.client.post(
            reverse(
                "permits:permit_submission_upload",
                kwargs={"pk": self.permit_submission.pk},
            ),
            data=self._formset_data(self.user),
            follow=True,
        )
        assert self.permit_submission.documents.count() == 1

        self.client.get(
            reverse(
                "permits:permit_submission_remove_document",
                kwargs={
                    "pk": self.permit_submission.pk,
                    "document_pk": self.permit_submission.documents.first().pk,
                },
            ),
            follow=True,
        )
        assert self.permit_submission.documents.count() == 0

    def test_permit_submission_delete_removes_related_documents(self):

        self.client.post(
            reverse(
                "permits:permit_submission_upload",
                kwargs={"pk": self.permit_submission.pk},
            ),
            data=self._formset_data(self.user),
            follow=True,
        )
        assert self.permit_submission.documents.count() == 1

        # Store the document path, to later check that it is removed:
        document_path = self.permit_submission.documents.first().document.path
        self.client.post(
            reverse(
                "submissions:submission_delete",
                kwargs={
                    "submission_id": self.permit_submission.pk,
                },
            ),
            follow=True,
        )

        # use document_path to check that it is removed
        assert not os.path.exists(document_path)
