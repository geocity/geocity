from django.test import TestCase
from django.urls import reverse

from geocity.apps.permits.models import PermitSubmission, PermitSubmissionTypes
from geocity.apps.permits.tests.utils import (
    base_editable_submission_fields,
    base_submission_fields,
    east_coord_example,
    north_coord_example,
)
from geocity.tests.factories import (
    AdministrativeEntityFactory,
    ChecklistFactory,
    FormFactory,
)
from geocity.tests.permits.factories import (
    PermitSubmissionFactory,
    StepChecklistFactory,
)
from geocity.tests.utils import LoggedInPilotMixin, LoggedInUserMixin


class PermitSubmissionUserWithoutPermissionTest(LoggedInUserMixin, TestCase):
    def setUp(self):
        super().setUp()

    def test_user_doesnt_have_permit_submission_permission(self):
        response = self.client.post(
            reverse("permits:permit_submission_create"),
            data={},
            follow=True,
        )
        assert response.status_code == 404

    def test_user_cant_edit_permit_submission(self):
        permit_submission = PermitSubmissionFactory()
        response = self.client.post(
            reverse(
                "permits:permit_submission_detail:edit",
                kwargs={"pk": permit_submission.pk},
            ),
            data={},
            follow=True,
        )
        assert response.status_code == 404


class PermitSubmissionFormTestCase(LoggedInPilotMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.administrative_entity.is_permits_enabled = True
        self.administrative_entity.save()
        FormFactory(
            administrative_entities=[self.administrative_entity], is_public=True
        )


class PermitSubmissionCreationTestCase(PermitSubmissionFormTestCase):
    def _base_fields(self):
        base_fields = base_submission_fields()
        base_fields["administrative_entity"] = [self.administrative_entity.pk]
        return base_fields

    def test_permit_submission_is_created(self):
        response = self.client.post(
            reverse("permits:permit_submission_create"),
            data=self._base_fields(),
        )
        # Will be 200 if any form has an error
        assert response.status_code == 302
        assert PermitSubmission.objects.count() == 1
        permit_submission = PermitSubmission.objects.first()

        assert (
            permit_submission.administrative_entity.pk == self.administrative_entity.pk
        )
        assert permit_submission.submission_type == "public_enquiry"
        assert permit_submission.camac_number == 233826
        assert permit_submission.shortname == "DemDupont"
        assert permit_submission.object == "Démolition de la ferme de la famille Dupont"
        assert permit_submission.competency == "m"
        assert permit_submission.nature == "demolition_partielle"
        assert permit_submission.transformation_type == ""
        assert permit_submission.work_type == 0
        assert permit_submission.construction_type == "building"
        assert permit_submission.zone_plan == "pga"
        assert permit_submission.parcel_numbers == ["123", "456"]
        assert permit_submission.eca_number == "123"
        assert permit_submission.east_coordinate == east_coord_example
        assert permit_submission.north_coordinate == north_coord_example
        assert permit_submission.is_outside_building_zone is True
        assert permit_submission.is_parcel_outside_building_zone is False
        assert permit_submission.is_work_outside_building_zone is True

        assert permit_submission.project_owner.first_name == "Jean"
        assert permit_submission.project_owner.last_name == "Luc"
        assert permit_submission.project_owner.company_name == ""
        assert permit_submission.project_owner.address == "Avenue Jomini 1"
        assert permit_submission.project_owner.email == "test@example.com"
        assert permit_submission.project_owner.phone == "0211234567"
        assert permit_submission.project_owner.zipcode == 1243
        assert permit_submission.project_owner.city == "Genève"

        assert permit_submission.owner.pk == permit_submission.project_owner.pk

    def test_permit_submission_triggers_condition_error(self):
        fields = self._base_fields()
        fields["eca_number"] = [""]
        response = self.client.post(
            reverse("permits:permit_submission_create"),
            data=fields,
            follow=True,
        )
        self.assertInHTML(
            "Ce champ est obligatoire.",
            response.content.decode(),
        )

    def test_permit_submission_triggers_error_if_no_name_for_project_owner(self):
        fields = self._base_fields()
        fields["project_owner-first_name"] = [""]
        fields["project_owner-last_name"] = [""]

        response = self.client.post(
            reverse("permits:permit_submission_create"),
            data=fields,
            follow=True,
        )
        self.assertInHTML(
            "Le nom et prénom ou la raison sociale doivent être renseignés.",
            response.content.decode(),
        )

    def test_permit_submission_created_for_different_owner_project_owner(self):
        fields = self._base_fields()
        fields.update(
            {
                "owner-is_owner_same_as_project_owner": ["False"],
                "owner-first_name": [""],
                "owner-last_name": [""],
                "owner-company_name": ["Demoman"],
                "owner-address": ["Rue du Carnil 2"],
                "owner-email": ["test2@example.com"],
                "owner-phone": ["0271234567"],
                "owner-zipcode": ["2000"],
                "owner-city": ["Neuchâtel"],
                "owner-country": ["CH"],
            }
        )

        self.client.post(
            reverse("permits:permit_submission_create"),
            data=fields,
            follow=True,
        )

        permit_submission = PermitSubmission.objects.first()
        assert PermitSubmission.objects.count() == 1

        assert permit_submission.project_owner.first_name == "Jean"
        assert permit_submission.project_owner.last_name == "Luc"
        assert permit_submission.project_owner.company_name == ""
        assert permit_submission.project_owner.address == "Avenue Jomini 1"
        assert permit_submission.project_owner.email == "test@example.com"
        assert permit_submission.project_owner.phone == "0211234567"
        assert permit_submission.project_owner.zipcode == 1243
        assert permit_submission.project_owner.city == "Genève"
        assert permit_submission.owner.first_name is None
        assert permit_submission.owner.last_name is None
        assert permit_submission.owner.company_name == "Demoman"
        assert permit_submission.owner.address == "Rue du Carnil 2"
        assert permit_submission.owner.email == "test2@example.com"
        assert permit_submission.owner.phone == "0271234567"
        assert permit_submission.owner.zipcode == 2000
        assert permit_submission.owner.city == "Neuchâtel"
        assert permit_submission.owner.country == "CH"

    def test_permit_submission_errors_with_wrong_coordinate(self):
        fields = self._base_fields()
        fields["east_coordinate"] = ["45204150"]

        response = self.client.post(
            reverse("permits:permit_submission_create"),
            data=fields,
            follow=True,
        )
        assert (
            "Assurez-vous que cette valeur est inférieure ou égale à 2590000"
            in response.content.decode()
        )


class PermitSubmissionEditTestCase(PermitSubmissionFormTestCase):
    def _base_fields(self):
        base_fields = base_editable_submission_fields()
        return base_fields

    def test_permit_submission_is_edited(self):
        permit_submission = PermitSubmissionFactory(
            administrative_entity=self.administrative_entity, camac_number="12"
        )

        response = self.client.post(
            reverse(
                "permits:permit_submission_detail:edit",
                kwargs={"pk": permit_submission.pk},
            ),
            data=self._base_fields(),
        )

        # Will be 200 if any form has an error
        assert response.status_code == 302
        assert PermitSubmission.objects.count() == 1

        permit_submission.refresh_from_db()
        assert permit_submission.camac_number == 233826

    def test_permit_submission_cant_edit_disabled_fields(self):
        permit_submission = PermitSubmissionFactory(
            administrative_entity=self.administrative_entity,
        )

        data = self._base_fields()
        data.update(
            {
                "administrative_entity": [str(AdministrativeEntityFactory().id)],
                "submission_type": ["prior_analysis"],
            }
        )

        response = self.client.post(
            reverse(
                "permits:permit_submission_detail:edit",
                kwargs={"pk": permit_submission.pk},
            ),
            data=data,
        )

        # Disabled fields are silently not considered
        assert response.status_code == 302
        assert PermitSubmission.objects.count() == 1

        permit_submission.refresh_from_db()
        assert permit_submission.administrative_entity == self.administrative_entity
        assert permit_submission.submission_type == PermitSubmissionTypes.PUBLIC_ENQUIRY


class PermitSubmissionChecklists(LoggedInPilotMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.administrative_entity.is_permits_enabled = True
        self.administrative_entity.save()
        FormFactory(
            administrative_entities=[self.administrative_entity], is_public=True
        )

    def test_checklist_is_present_in_submission(self):
        checklist = ChecklistFactory()
        StepChecklistFactory(
            administrative_entity=self.administrative_entity,
            checklist=checklist,
            permit_submission_type="public_enquiry",
            step="validation",
        )

        base_fields = base_submission_fields()
        base_fields["administrative_entity"] = [self.administrative_entity.pk]
        self.client.post(
            reverse("permits:permit_submission_create"),
            data=base_fields,
        )

        permit_submission = PermitSubmission.objects.last()
        assert permit_submission.checklists.filter(checklist__pk=checklist.pk).exists()

    def test_pilot_can_update_checklist_task(self):
        checklist = ChecklistFactory()
        StepChecklistFactory(
            administrative_entity=self.administrative_entity,
            checklist=checklist,
            permit_submission_type=PermitSubmissionTypes.PUBLIC_ENQUIRY,
            step="validation",
        )

        base_fields = base_submission_fields()
        base_fields["administrative_entity"] = [self.administrative_entity.pk]
        self.client.post(
            reverse("permits:permit_submission_create"),
            data=base_fields,
        )
        permit_submission = PermitSubmission.objects.last()
        checklist = permit_submission.checklists.last()

        self.client.post(
            reverse(
                "submissions:checklist_update",
                kwargs={
                    "submission_id": permit_submission.pk,
                    "checklist_id": checklist.pk,
                },
            ),
            data={
                "tasks": [
                    str(checklist.tasks.first().pk),
                    str(checklist.tasks.last().pk),
                ]
            },
        )

        assert checklist.tasks.filter(completed=True).count() == 2

    def test_checklist_is_not_present_in_submission_of_different_type(self):
        checklist = ChecklistFactory()
        StepChecklistFactory(
            administrative_entity=self.administrative_entity,
            checklist=checklist,
            permit_submission_type=PermitSubmissionTypes.PRIOR_ANALYSIS,
            step="validation",
        )

        base_fields = base_submission_fields()
        base_fields["administrative_entity"] = [self.administrative_entity.pk]
        self.client.post(
            reverse("permits:permit_submission_create"),
            data=base_fields,
        )

        permit_submission = PermitSubmission.objects.last()
        assert not permit_submission.checklists.filter(pk=checklist.pk).exists()
