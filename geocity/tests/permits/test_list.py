from django.test import TestCase
from django.urls import reverse

from geocity.apps.permits.models import PermitSubmissionTypes
from geocity.apps.submissions.models import Submission
from geocity.tests import factories
from geocity.tests.permits.factories import PermitSubmissionFactory
from geocity.tests.utils import LoggedInPilotMixin


class PermitSubmissionListTestCase(LoggedInPilotMixin, TestCase):
    def setUp(self):
        super().setUp()

        # add second user
        self.user2 = factories.PilotUserFactory.create()
        # Enable POLCO
        self.administrative_entity.is_permits_enabled = True
        self.administrative_entity.is_single_form_submissions = True
        self.administrative_entity.save()

    def _test_list_submission(
        self, permit_author, permit_status=Submission.STATUS_SUBMITTED_FOR_VALIDATION
    ):
        # Create Submission
        # Create classical submission
        submission = factories.SubmissionFactory(
            author=self.user,
            status=Submission.STATUS_APPROVED,
            shortname="sub-x1",
            administrative_entity=self.administrative_entity,
        )

        # Create PermitSubmission
        p_submission = PermitSubmissionFactory(
            author=permit_author,
            submission_type=PermitSubmissionTypes.PUBLIC_ENQUIRY,
            administrative_entity=self.administrative_entity,
            shortname="Custom-id",
            status=permit_status,
        )

        # GET List
        response = self.client.get(reverse("submissions:submissions_list"))

        objs = response.context["polymorphicsubmission_list"]

        self.assertEqual(len(objs), 2)
        self._check_permit_submission(objs[0], p_submission)
        self._check_submission(objs[1], submission)

    def _check_permit_submission(self, obj, p_submission):
        self.assertEqual(obj.id_for_display, f"P-{p_submission.id}")
        self.assertEqual(obj.shortname, "Custom-id")
        self.assertEqual(obj.status, p_submission.status)
        self.assertEqual(obj.sent_date, p_submission.created_at)
        self.assertEqual(obj.administrative_entity, p_submission.administrative_entity)

    def _check_submission(self, obj, submission):
        self.assertEqual(obj.id_for_display, f"G-{submission.id}")
        self.assertEqual(obj.shortname, "sub-x1")
        self.assertEqual(obj.status, submission.status)
        self.assertEqual(obj.sent_date, submission.sent_date)
        self.assertEqual(obj.administrative_entity, submission.administrative_entity)

    def test_my_list_shows_permits_and_standard_submissions(self):
        self._test_list_submission(self.user)

    def test_my_list_shows_awaiting_permits(self):
        self._test_list_submission(self.user2, Submission.STATUS_AWAITING_VALIDATION)

    def test_my_list_shows_processing_permits(self):
        self._test_list_submission(self.user2, Submission.STATUS_PROCESSING)
