from django.test import TestCase
from django.urls import reverse

from geocity.apps.permits.models import PermitSubmissionTypes
from geocity.tests.permits.factories import PermitSubmissionFactory
from geocity.tests.utils import LoggedInPilotMixin, get_parser


class PermitSubmissionDetailValidationViewTest(LoggedInPilotMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.administrative_entity.is_permits_enabled = True
        self.administrative_entity.save()
        self.permit_submission = PermitSubmissionFactory(
            submission_type=PermitSubmissionTypes.PUBLIC_ENQUIRY,
            administrative_entity=self.administrative_entity,
            shortname="Custom-id",
            author=self.user,
        )

    def test_displays_formal_examination_form(self):
        response = self.client.get(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
        )

        parser = get_parser(response.content)

        formal_examination_report_textarea = parser.select_one(
            'textarea[name="formal_examination_report"]'
        )
        self.assertEqual(
            formal_examination_report_textarea.text.strip(),
            "",
        )
        self.assertEqual(formal_examination_report_textarea.get("disabled"), None)

        self.assertEqual(
            parser.select_one('[name="formal_examination_completed"]').get("checked"),
            None,
        )

    def test_update_formal_examination_form(self):
        response = self.client.post(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
            data={
                "form": "formal_examination",
                "formal_examination_report": "Hello",
                "formal_examination_completed": "on",
            },
        )

        parser = get_parser(response.content)

        formal_examination_report_textarea = parser.select_one(
            'textarea[name="formal_examination_report"]'
        )
        self.assertEqual(
            formal_examination_report_textarea.text.strip(),
            "Hello",
        )
        self.assertEqual(formal_examination_report_textarea.get("disabled"), "")

        self.assertEqual(
            parser.select_one('[name="formal_examination_completed"]').get("checked"),
            "",
        )

    def test_displays_technical_examination_form(self):
        response = self.client.get(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
        )

        parser = get_parser(response.content)

        self.assertEqual(
            parser.select_one('[name="technical_examination"]').get("checked"),
            None,
        )

    def test_update_technical_examination_form(self):
        response = self.client.post(
            reverse(
                "permits:permit_submission_detail:validation",
                kwargs={"pk": self.permit_submission.pk},
            ),
            data={
                "form": "technical_examination",
                "technical_examination": "on",
            },
        )

        parser = get_parser(response.content)

        self.assertEqual(
            parser.select_one('[name="technical_examination"]').get("checked"),
            "",
        )
