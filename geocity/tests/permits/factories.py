import random

import factory
from django.utils.text import Truncator

from geocity.apps.permits import models
from geocity.apps.permits.steps import PERMIT_STEP_NAME, StepChecklist
from geocity.tests.factories import AdministrativeEntityFactory, ChecklistFactory


class PermitContactFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    address = factory.Faker("word")
    zipcode = factory.Faker("zipcode")
    city = factory.Faker("city")
    phone = Truncator(factory.Faker("phone_number")).chars(19)

    class Meta:
        model = models.PermitContact


class PermitSubmissionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.PermitSubmission

    administrative_entity = factory.SubFactory(AdministrativeEntityFactory)
    camac_number = factory.LazyAttribute(lambda _: random.randint(100, 9999))
    project_owner = factory.SubFactory(PermitContactFactory)

    project_owner_type = factory.LazyAttribute(
        lambda _: random.choice(tuple(key for key, label in models.PROJECT_OWNER_TYPES))
    )
    east_coordinate = models.PERMIT_EAST_MIN_COORD + 1
    north_coordinate = models.PERMIT_NORTH_MIN_COORD + 1
    parcel_numbers = factory.LazyAttribute(
        lambda _: "{%s}" % ",".join(map(str, random.sample(range(100, 999), 2)))
    )
    # First value of first item of first category
    work_type = models.WORK_TYPES_CHOICES[0][1][0][0]

    @factory.post_generation
    def owner(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            self.owner = extracted
        else:
            # Set owner "same as project owner" if not specified
            self.owner = self.project_owner


class PermitSubmissionProgressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.PermitSubmissionProgress

    permit_submission = factory.SubFactory(PermitSubmissionFactory)
    formal_examination = factory.Faker("boolean")
    technical_examination = factory.Faker("boolean")
    validation = factory.Faker("boolean")


class StepChecklistFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = StepChecklist

    administrative_entity = factory.SubFactory(AdministrativeEntityFactory)
    checklist = factory.SubFactory(ChecklistFactory)
    step = factory.Faker(
        "random_element", elements=[key for key, label in PERMIT_STEP_NAME.items()]
    )
    permit_submission_type = factory.Faker(
        "random_element",
        elements=[key for key, label in models.PermitSubmissionTypes.choices],
    )
