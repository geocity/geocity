from django.test import TestCase
from django.urls import reverse

from geocity.apps.forms import models as forms_models
from geocity.tests import factories
from geocity.tests.utils import LoggedInUserMixin, get_parser


def extract_nav_items(content):
    nav_items = get_parser(content).select(".stepper-step .step-name")
    return [nav_item.text.strip() for nav_item in nav_items]


class SubmissionProgressBarTestCase(LoggedInUserMixin, TestCase):
    def create_submission(self):
        submission = factories.SubmissionFactory(author=self.user)
        return submission

    def test_contacts_step_does_not_appear_when_no_contacts_required(self):
        submission = self.create_submission()
        form = factories.FormFactory()
        submission.administrative_entity.forms.set([form])
        submission.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_select_administrative_entity",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertNotIn("Contacts", nav_items)

    def test_contacts_step_appears_when_contacts_required(self):
        submission = self.create_submission()
        form = factories.FormFactory()
        submission.administrative_entity.forms.set([form])
        submission.forms.set([form])
        factories.ContactFormFactory(form_category=form.category)

        response = self.client.get(
            reverse(
                "submissions:submission_select_administrative_entity",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertIn("Contacts", nav_items)

    def test_appendices_step_does_not_appear_when_no_appendices_required(self):
        submission = self.create_submission()
        form = factories.FormFactory()
        submission.administrative_entity.forms.set([form])
        submission.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_select_administrative_entity",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertNotIn("Documents", nav_items)

    def test_appendices_step_appears_when_appendices_required(self):
        submission = self.create_submission()
        form = factories.FormFactory()
        submission.administrative_entity.forms.set([form])
        submission.forms.set([form])
        field = factories.FieldFactory(
            input_type=forms_models.Field.INPUT_TYPE_FILE,
        )
        field.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_select_administrative_entity",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertIn("Documents", nav_items)

    def test_geotime_step_does_not_appear_when_no_date_nor_geometry_types_are_required(
        self,
    ):
        submission = self.create_submission()
        form = factories.FormWithoutGeometryFactory(
            needs_date=False,
        )
        submission.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertNotIn("Localisation et planning", nav_items)
        self.assertNotIn("Planning", nav_items)
        self.assertNotIn("Localisation", nav_items)

    def test_geotime_step_appears_when_date_and_geometry_types_are_required(self):
        submission = self.create_submission()
        form = factories.FormFactory(
            has_geometry_point=True,
            has_geometry_line=True,
            has_geometry_polygon=True,
            needs_date=True,
        )
        submission.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertIn("Localisation et planning", nav_items)

    def test_geotime_step_appears_when_only_date_is_required(self):
        submission = self.create_submission()
        form = factories.FormWithoutGeometryFactory(
            needs_date=True,
        )
        submission.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertIn("Planning", nav_items)
        self.assertNotIn("Localisation et planning", nav_items)
        self.assertNotIn("Localisation", nav_items)

    def test_geotime_step_appears_when_only_geometry_types_are_required(self):
        submission = self.create_submission()
        form = factories.FormFactory(
            has_geometry_point=True,
            has_geometry_line=True,
            has_geometry_polygon=True,
            needs_date=False,
        )
        submission.forms.set([form])

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": submission.pk},
            )
        )

        self.assertEqual(response.status_code, 200)
        nav_items = extract_nav_items(response.content)
        self.assertIn("Localisation", nav_items)
        self.assertNotIn("Localisation et planning", nav_items)
        self.assertNotIn("Planning", nav_items)

    def test_step_display_no_subtitle_when_no_value_exists(self):
        submission = self.create_submission()

        response = self.client.get(
            reverse(
                "submissions:submission_select_forms",
                kwargs={"submission_id": submission.pk},
            )
        )

        subtitles = get_parser(response.content).select(".stepper-step .step-subtitle")

        self.assertEqual(len(subtitles), 0)

    def test_administrative_entity_step_display_selected_value_as_subtitle(self):
        submission = self.create_submission()
        form1 = factories.FormFactory()
        form2 = factories.FormFactory()
        submission.administrative_entity.forms.set([form1, form2])
        submission.forms.set([form1, form2])

        response = self.client.get(
            reverse(
                "submissions:submission_select_forms",
                kwargs={"submission_id": submission.pk},
            )
        )

        url = reverse(
            "submissions:submission_select_administrative_entity",
            kwargs={"submission_id": submission.pk},
        )
        subtitle = get_parser(response.content).select_one(
            f".stepper-step a[href='{url}'] .step-subtitle"
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(submission.administrative_entity.name, subtitle.text.strip())

    def test_select_forms_step_display_selected_values_as_subtitle(self):
        submission = self.create_submission()
        form1 = factories.FormFactory()
        form2 = factories.FormFactory()
        submission.administrative_entity.forms.set([form1, form2])
        submission.forms.set([form1, form2])

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": submission.pk},
            )
        )

        url = reverse(
            "submissions:submission_select_forms",
            kwargs={"submission_id": submission.pk},
        )
        subtitle = get_parser(response.content).select_one(
            f".stepper-step a[href='{url}'] .step-subtitle"
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            ", ".join([form.name for form in submission.forms.all()]),
            subtitle.text.strip(),
        )
