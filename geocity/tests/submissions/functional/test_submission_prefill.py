from django.test import TestCase
from django.urls import reverse

from geocity.tests import factories
from geocity.tests.utils import LoggedInUserMixin, get_parser


class SubmissionPrefillTestCase(LoggedInUserMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.submission = factories.SubmissionFactory(author=self.user)
        factories.SelectedFormFactory.create_batch(
            3,
            submission=self.submission,
            form__category=factories.FormCategoryFactory(),
        )
        self.submission.administrative_entity.forms.set(self.submission.forms.all())

    def test_forms_step_preselects_forms_for_existing_submission(self):
        response = self.client.get(
            reverse(
                "submissions:submission_select_forms",
                kwargs={"submission_id": self.submission.pk},
            )
        )

        parser = get_parser(response.content)

        for i, form in enumerate(self.submission.forms.order_by("order")):
            self.assertEqual(
                len(
                    parser.select(f'input[type="checkbox"][value="{form.pk}"]:checked')
                ),
                1,
            )

    def test_fields_step_prefills_fields_for_existing_submission(self):
        selected_form = self.submission.get_selected_forms().first()
        field = factories.FieldFactory()
        field.forms.add(selected_form.form)
        field_value = factories.FieldValueFactory(
            selected_form=selected_form, field=field
        )
        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": self.submission.pk},
            )
        )

        parser = get_parser(response.content)

        textarea = parser.select(
            'textarea[name="fields-{form_id}_{field_id}"]'.format(
                form_id=selected_form.form.pk,
                field_id=field.pk,
            )
        )

        self.assertEqual(len(textarea), 1)
        self.assertEqual(textarea[0].text.strip(), field_value.value["val"])
        self.assertEqual(textarea[0]["placeholder"], f"ex: {field.placeholder}")

        help_text = parser.select_one(
            "#id_fields-{form_id}_{field_id}-help".format(
                form_id=selected_form.form.pk,
                field_id=field.pk,
            )
        )

        self.assertEqual(help_text.text.strip(), field.help_text)

    def test_fields_step_shows_title_with_help_text(self):
        selected_form = self.submission.get_selected_forms().first()

        field_title = factories.FieldFactoryTypeTitle()
        field_title.forms.add(selected_form.form)

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": self.submission.pk},
            )
        )

        parser = get_parser(response.content)
        title = parser.select_one('h3[data-role="property-title"]')
        title_help = parser.select_one('[data-role="property-title-help"]')

        self.assertEqual(title.text.strip(), field_title.name)
        self.assertEqual(title_help.text.strip(), field_title.help_text)

    def test_fields_step_order_fields_for_existing_submission(self):
        selected_form = self.submission.get_selected_forms().first()

        field_1 = factories.FormFieldFactory(form=selected_form.form)
        field_2 = factories.FormFieldFactory(form=selected_form.form)

        # Force the order explicitly after creation, since new fields are added at the end by default
        field_1.order = 10
        field_2.order = 2
        field_1.save()
        field_2.save()

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": self.submission.pk},
            )
        )
        content = response.content.decode()
        position_1 = content.find(field_1.field.name)
        position_2 = content.find(field_2.field.name)
        self.assertGreater(position_1, position_2)

    def test_fields_step_shows_downloadable_file(self):
        selected_form = self.submission.get_selected_forms().first()

        field_file = factories.FieldFactoryTypeFileDownload()
        field_file.forms.add(selected_form.form)

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": self.submission.pk},
            )
        )

        expected_href = f"/submissions/form-files/{field_file.file_download.name}"
        parser = get_parser(response.content)
        file_links = parser.find_all("a", href=expected_href)
        self.assertEqual(1, len(file_links))
        self.assertIn(expected_href, response.content.decode())

    def test_fields_step_shows_downloadable_files_more_than_once(self):
        selected_forms = self.submission.get_selected_forms()
        selected_form_first = selected_forms.first()
        selected_form_last = selected_forms.last()

        field_file = factories.FieldFactoryTypeFileDownload()
        field_file.forms.add(selected_form_first.form)
        field_file.forms.add(selected_form_last.form)

        response = self.client.get(
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": self.submission.pk},
            )
        )

        expected_href = f"/submissions/form-files/{field_file.file_download.name}"
        parser = get_parser(response.content)
        file_links = parser.find_all("a", href=expected_href)

        self.assertEqual(2, len(file_links))
        self.assertIn(expected_href, response.content.decode())
