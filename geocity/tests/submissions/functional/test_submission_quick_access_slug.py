from django.test import TestCase
from django.urls import reverse

from geocity.apps.submissions.models import Submission
from geocity.tests import factories


class SubmissionQuickAccessSlugTestCase(TestCase):
    def setUp(self) -> None:
        super().setUp()

        entity = factories.AdministrativeEntityFactory()
        self.user = factories.UserFactory()
        self.form = factories.FormFactory()

        # Use one admin entity and add a field so that we don't skip the screen.
        self.form.administrative_entities.set([entity])
        factories.FormFieldFactory(form=self.form)

    def test_quick_access_slug_redirects(self):
        self.client.login(username=self.user, password="password")

        response = self.client.get(f"/?form={self.form.quick_access_slug}", follow=True)

        lastest_submission = Submission.objects.filter_for_user(self.user).order_by(
            "-id"
        )[0]

        self.assertRedirects(
            response,
            reverse(
                "submissions:submission_fields",
                kwargs={"submission_id": lastest_submission.pk},
            ),
        )
