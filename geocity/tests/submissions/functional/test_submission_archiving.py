import re
from datetime import datetime

from django.urls import reverse

from geocity.apps.submissions import models as submissions_models
from geocity.tests import factories
from geocity.tests.geocity_test_case import GeocityTestCase
from geocity.tests.utils import get_parser


class TestSubmissionArchiving(GeocityTestCase):
    def setUp(self):
        super(TestSubmissionArchiving, self).setUp()

        self.login(email="user@test.com")
        self.parent_type = factories.ParentComplementaryDocumentTypeFactory()

        self.submission = factories.SubmissionFactory(
            author=self.user,
            status=submissions_models.Submission.STATUS_RECEIVED,
            administrative_entity=self.administrative_entity,
        )

    def test_submissions_list_display_archive_button_and_checkboxes_for_pilot(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)

        submission_list_response = self.client.get(
            reverse("submissions:submissions_list")
        )
        parser = get_parser(submission_list_response.content)

        archive_buttons = parser.select('[data-role="archive-submissions"]')
        self.assertEquals(len(archive_buttons), 1)

        archive_checkboxes = parser.select(
            'input[type="checkbox"][name="archive_checkbox[]"]'
        )
        self.assertEquals(len(archive_checkboxes), 1)

    def test_submissions_list_doesnt_display_archive_button_and_checkboxes_for_regular_users(
        self,
    ):
        self.login(email="user@test.com")

        submission_list_response = self.client.get(
            reverse("submissions:submissions_list")
        )
        parser = get_parser(submission_list_response.content)

        archive_buttons = parser.select('[data-role="archive-submissions"]')
        self.assertEquals(len(archive_buttons), 0)

        archive_checkboxes = parser.select(
            'input[type="checkbox"][name="archive_checkbox[]"]'
        )
        self.assertEquals(len(archive_checkboxes), 0)

    def test_submissions_list_dont_display_archive_button_and_checkboxes_for_validators(
        self,
    ):
        self.login(email="validator@test.com", group=self.VALIDATOR)

        submission_list_response = self.client.get(
            reverse("submissions:submissions_list")
        )
        parser = get_parser(submission_list_response.content)

        archive_buttons = parser.select('[data-role="archive-submissions"]')
        self.assertEquals(len(archive_buttons), 0)

        archive_checkboxes = parser.select(
            'input[type="checkbox"][name="archive_checkbox[]"]'
        )
        self.assertEquals(len(archive_checkboxes), 0)

    def test_submission_is_archived_if_its_status_allows_it(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)

        statuses = (
            submissions_models.Submission.STATUS_DRAFT,
            submissions_models.Submission.STATUS_SUBMITTED_FOR_VALIDATION,
            submissions_models.Submission.STATUS_APPROVED,
            submissions_models.Submission.STATUS_PROCESSING,
            submissions_models.Submission.STATUS_AWAITING_SUPPLEMENT,
            submissions_models.Submission.STATUS_AWAITING_VALIDATION,
            submissions_models.Submission.STATUS_REJECTED,
            submissions_models.Submission.STATUS_RECEIVED,
            submissions_models.Submission.STATUS_INQUIRY_IN_PROGRESS,
        )
        allowed_statuses = (
            submissions_models.Submission.STATUS_APPROVED,
            submissions_models.Submission.STATUS_REJECTED,
            submissions_models.Submission.STATUS_RECEIVED,
        )

        for status in statuses:
            submission = factories.SubmissionFactory(
                author=self.user,
                status=status,
                administrative_entity=self.administrative_entity,
            )

            response = self.client.post(
                reverse("submissions:archived_submission_list"),
                data={
                    "to_archive[]": submission.id,
                    "action": "archive-requests",
                },
            )
            submission.refresh_from_db()

            if status in allowed_statuses:
                self.assertEquals(
                    submission.status,
                    submissions_models.Submission.STATUS_ARCHIVED,
                )
                self.assertTrue(
                    submissions_models.ArchivedSubmission.objects.filter(
                        submission=submission
                    ).exists()
                )
            else:
                self.assertEqual(response.status_code, 403)

    def test_non_pilot_users_cannot_archive_requests(self):
        self.login(email="validator@test.com", group=self.VALIDATOR)

        response = self.client.post(
            reverse("submissions:archived_submission_list"),
            data={
                "to_archive[]": self.submission.id,
                "action": "archive-requests",
            },
        )
        self.submission.refresh_from_db()
        self.assertNotEquals(
            self.submission.status, submissions_models.Submission.STATUS_ARCHIVED
        )
        self.assertFalse(
            submissions_models.ArchivedSubmission.objects.filter(
                submission=self.submission
            ).exists()
        )
        self.assertEqual(response.status_code, 403)

    def test_single_download_archive(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)
        self.submission.archive(self.user)
        file_response = self.client.get(
            reverse(
                "submissions:archived_submission_download",
                kwargs={
                    "pk": self.submission.pk,
                },
            ),
        )

        # Do not compare seconds as they can have a small delta and sometimes make the tests fail
        expected_filename_regex = (
            f"Archive_{datetime.today().strftime('%d.%m.%Y.%H.%M')}.[0-9][0-9]"
        )

        self.assertEqual(file_response.get("Content-Type"), "application/zip")
        self.assertRegex(
            file_response.get("Content-Disposition"),
            re.compile(rf'inline; filename="{expected_filename_regex}.zip"'),
        )

    def test_bulk_download(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)
        self.submission.archive(self.user)
        archive = submissions_models.ArchivedSubmission.objects.filter(
            submission=self.submission
        ).first()
        file_response = self.client.get(
            reverse(
                "submissions:archived_submission_bulk_download",
            ),
            data={"to_download": self.submission.pk},
        )

        # Do not compare seconds as they can have a small delta and sometimes make the tests fail
        expected_filename_regex = (
            f"Archive_{datetime.today().strftime('%d.%m.%Y.%H.%M')}.[0-9][0-9]"
        )

        self.assertEqual(file_response.get("Content-Type"), "application/zip")
        self.assertRegex(
            file_response.get("Content-Disposition"),
            re.compile(rf'inline; filename="{expected_filename_regex}.zip"'),
        )

    def test_users_not_in_submission_entity_group_cannot_single_download(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)
        archive = factories.ArchivedSubmissionFactory(
            submission=self.submission, archivist=self.user
        )
        self.login(email="validator@test.com", group=self.VALIDATOR)

        response = self.client.get(
            reverse(
                "submissions:archived_submission_download",
                kwargs={
                    "pk": archive.submission.pk,
                },
            ),
            follow=True,
        )
        self.assertNotEqual(response.get("Content-Type"), "application/zip")
        self.assertEqual(response.status_code, 403)

    def test_users_not_in_submission_entity_group_cannot_bulk_download(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)
        self.submission.archive(self.user)
        archive = submissions_models.ArchivedSubmission.objects.filter(
            submission=self.submission
        ).first()
        self.login(email="validator@test.com", group=self.VALIDATOR)
        response = self.client.get(
            reverse(
                "submissions:archived_submission_bulk_download",
            ),
            data={"to_download": archive.submission.pk},
            follow=True,
        )
        self.assertNotEqual(response.get("Content-Type"), "application/zip")
        self.assertEqual(response.status_code, 403)

    def test_coordinator_can_delete_archive(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)
        self.submission.archive(self.user)
        archive = submissions_models.ArchivedSubmission.objects.filter(
            submission=self.submission
        ).first()

        self.client.post(
            reverse(
                "submissions:archived_submission_delete",
                kwargs={"pk": archive.submission.pk},
            ),
        )

        self.assertFalse(
            submissions_models.ArchivedSubmission.objects.filter(
                submission=archive.submission
            ).exists()
        )
        self.assertFalse(
            submissions_models.Submission.objects.filter(pk=self.submission.pk).exists()
        )

    def test_user_without_correct_permissions_cannot_delete_archive(self):
        self.login(email="pilot@test.com", group=self.SECRETARIAT)
        self.submission.archive(self.user)
        archive = submissions_models.ArchivedSubmission.objects.filter(
            submission=self.submission
        ).first()
        self.login(email="validator@test.com", group=self.VALIDATOR)

        response = self.client.post(
            reverse(
                "submissions:archived_submission_delete",
                kwargs={"pk": archive.submission.pk},
            ),
            follow=True,
        )

        self.assertEqual(response.status_code, 403)
        self.assertTrue(
            submissions_models.ArchivedSubmission.objects.filter(
                submission=archive.submission
            ).exists()
        )
        self.assertTrue(
            submissions_models.Submission.objects.filter(pk=self.submission.pk).exists()
        )
