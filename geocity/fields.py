import json

from django import forms
from django.conf import settings
from django.contrib.gis import forms as geoforms
from django.core.files.storage import FileSystemStorage
from django.utils.translation import gettext


class PrivateFileSystemStorage(FileSystemStorage):
    """
    Storage for files that MUST NOT get directly exposed by the web server.
    """

    def __init__(self):
        super().__init__(location=settings.PRIVATE_MEDIA_ROOT, base_url=None)


class PublicFileSystemStorage(FileSystemStorage):
    """
    Storage for files that CAN ALLWAYS be directly exposed by the web server.
    """

    def __init__(self):
        super().__init__(location=settings.PUBLIC_MEDIA_ROOT, base_url=None)


# extend django gis osm openlayers widget
class GeometryWidget(geoforms.OSMWidget):
    template_name = "geometrywidget/geometrywidget.html"
    map_srid = 2056

    @property
    def media(self):
        return forms.Media(
            css={
                "all": (
                    "libs/js/openlayers6/ol.css",
                    "libs/js/jquery-ui-custom/jquery-ui.min.css",
                    "css/geotime.css",
                )
            },
            js=(
                "libs/js/openlayers6/ol.js",
                "libs/js/proj4js/proj4-src.js",
                "customWidgets/GeometryWidget/geometrywidget.js",
                "libs/js/jquery-ui-custom/jquery-ui.min.js",
            ),
        )


class GeometryWidgetAdvanced(geoforms.OpenLayersWidget):
    template_name = "advancedgeometrywidget/advancedgeometrywidget.html"
    map_srid = 2056

    @property
    def media(self):
        return forms.Media()


class AddressWidget(forms.TextInput):
    template_name = "submissions/widgets/address.html"

    def __init__(self, attrs=None, autocomplete_options=None):
        autocomplete_options = {
            "apiurl": settings.LOCATIONS_SEARCH_API,
            "apiurlDetail": settings.LOCATIONS_SEARCH_API_DETAILS,
            "origins": "address",
            "zipcodeField": "zipcode",
            "cityField": "city",
            "placeholder": gettext("ex: Place Pestalozzi 2, 1400 Yverdon"),
            "singleContact": True,
            "singleAddressField": False,
            **(autocomplete_options or {}),
        }
        super().__init__(
            {
                **(attrs or {}),
                "data-address-autocomplete": json.dumps(autocomplete_options),
            }
        )

    def build_attrs(self, base_attrs, extra_attrs=None):
        # Create a datalist linked with the input to be populated client-side
        if "id" in extra_attrs:
            extra_attrs.update({"list": f"{extra_attrs['id']}-datalist"})
        return {**base_attrs, **extra_attrs, **{"autocomplete": "off"}}
