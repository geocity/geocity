### Base image, either for prod, or for dev

# Mandatory to be passed when building the local / prod image
ARG GDAL_VERSION
FROM ghcr.io/osgeo/gdal:ubuntu-small-${GDAL_VERSION}

RUN set -e; \
    apt-get -y update ; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends \
       build-essential \
       gettext \
       python3-pip \
       libcairo2-dev \
       poppler-utils \
       python3-dev \
       python3-setuptools \
       python3-wheel \
       python3-cffi \
       libcairo2 \
       libpango-1.0-0 \
       libpangocairo-1.0-0 \
       libgdk-pixbuf2.0-0 \
       libffi-dev \
       shared-mime-info \
       tzdata \
       ; \
  ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime ; \
  dpkg-reconfigure -f noninteractive tzdata ; \
  rm -rf /var/lib/apt/lists/* ; \
  :

# Use our entrypoint script
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

# Update C env vars so compiler can find gdal
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal
ENV PYTHONUNBUFFERED 1

WORKDIR /code
COPY requirements_dev.txt requirements_dev.txt
COPY requirements.txt requirements.txt

ARG ENV
RUN set -e; \
    # For later collectstatic
    mkdir -p /var/www/project_statics/static ; \
    mkdir -p /var/www/project_statics/external ; \
    mkdir -p /var/www/project_statics/vite ; \
    REQUIREMENTS_TXT=requirements.txt ; \
    if [ "$ENV" = "DEV" ] ; then \
      REQUIREMENTS_TXT=requirements_dev.txt ; \
    fi ; \
    echo "####" ; \
    echo "# Install $ENV dependencies from ${REQUIREMENTS_TXT}" ; \
    echo "####" ; \
    pip3 install -r $REQUIREMENTS_TXT; \
    echo "####" ; \
    echo "# Installed $ENV dependencies from ${REQUIREMENTS_TXT}" ; \
    echo "####" ; \
    :
