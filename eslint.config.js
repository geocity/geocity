import globals from 'globals';
import pluginJs from '@eslint/js';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';

export default [
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        // Django JavaScript Catalog functions
        gettext: false,
        ngettext: false,
        interpolate: false,
        get_format: false,
        gettext_noop: false,
        pgettext: false,
        npgettext: false,
        pluralidx: false,
      },
    },
  },
  {
    files: ['*.config.js'],
    languageOptions: { globals: globals.node },
  },
  pluginJs.configs.recommended,
  eslintPluginPrettierRecommended,
];
