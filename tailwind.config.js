import colors from 'tailwindcss/colors';
import typography from '@tailwindcss/typography';

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./geocity/**/*.html', './geocity/**/*.py', './assets/js/**/*.js'],
  theme: {
    extend: {
      colors: {
        grey: {
          50: 'var(--color-grey-50)',
          100: 'var(--color-grey-100)',
          200: 'var(--color-grey-200)',
          300: 'var(--color-grey-300)',
          400: 'var(--color-grey-400)',
          500: 'var(--color-grey-500)',
          600: 'var(--color-grey-600)',
          700: 'var(--color-grey-700)',
          800: 'var(--color-grey-800)',
          900: 'var(--color-grey-900)',
        },
        primary: {
          50: 'var(--color-primary-50)',
          100: 'var(--color-primary-100)',
          200: 'var(--color-primary-200)',
          300: 'var(--color-primary-300)',
          400: 'var(--color-primary-400)',
          500: 'var(--color-primary-500)',
          600: 'var(--color-primary-600)',
          700: 'var(--color-primary-700)',
          800: 'var(--color-primary-800)',
          900: 'var(--color-primary-900)',
        },
        info: colors.blue,
        warning: colors.amber,
        danger: colors.red,
        success: colors.green,
      },
      screens: {
        'max-xs': { max: '638px' },
        'max-sm': { max: '767px' },
        'max-md': { max: '1023px' },
        'max-lg': { max: '1279px' },
        'max-xl': { max: '1535px' },
      },
    },
  },
  corePlugins: {
    container: false,
  },
  plugins: [typography],
};
