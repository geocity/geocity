# User story

**En tant que** (type d’utilisateur)

**Je veux** (objectif)

**Afin que** (bénéfice)

## Critères d'acceptation

* [ ]

/label ~type::user-story
