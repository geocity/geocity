---
include:
    # Gitlab default code quality
    - template: Jobs/Code-Quality.gitlab-ci.yml
    - local: ci/pre-commit.yml
    - local: ci/gemnasium.yml
    # Gitlab default container scanning
    - template: Jobs/Container-Scanning.gitlab-ci.yml
    - template: Security/Secret-Detection.gitlab-ci.yml
    - project: liip/containers-builder
      file: tasks.yml
      ref: v3

.when-to-run:
    rules:
        # Always on MRs
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        # On develop
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        # On main
        - if: $CI_COMMIT_BRANCH == "main"
        # On tags
        - if: $CI_COMMIT_TAG
        - when: never

stages:
    - prelint
    - lint
    - build
    - test
    - artifacts
    - release
    - cleanup

code_quality:
    stage: lint
    rules: !reference [.when-to-run, rules]

pre-commit:
    stage: lint
    extends: [.when-to-run, .pre-commit]

secret_detection:
    stage: lint
    rules: !reference [.when-to-run, rules]

.build-images:
    extends: [.when-to-run, .build-definition]
    stage: build
    environment:
        name: $CI_COMMIT_REF_SLUG
        on_stop: "Cleanup branch images"
    before_script:
      - GDAL_VERSION=$(sed -n -e 's/^gdal=\(=\|\)//p' requirements.in)
      - echo "Will build for GDAL version '${GDAL_VERSION}'"
      - BUILD_ARGS="$BUILD_ARGS --build-arg=GDAL_VERSION=${GDAL_VERSION} --build-arg=ENV=${ENV}"

Build images:
    extends: [.build-images]
    needs: [pre-commit]
    parallel:
        matrix:
            - BUILD_IMAGE_NAME: geocity_pdf
              BUILD_PATH: services/pdf
            - BUILD_IMAGE_NAME: geocity_qgis
              BUILD_PATH: services/qgis
            - BUILD_IMAGE_NAME: geocity_web_dev
              ENV: DEV
            - BUILD_IMAGE_NAME: geocity_web_prod
              ENV: PROD

Run tests, 2FA:
    stage: test
    needs: [pre-commit, Build images]
    extends: [.when-to-run, .docker-in-docker]
    variables:
        ARCHIVE_DIR: ./archive
        DOCKER_COMPOSE: docker-compose --progress=quiet
        RUN_IN_WEB: $DOCKER_COMPOSE run --name=web --service-ports --rm --entrypoint="" web
        COMPOSE_FILE: docker-compose.yml:docker-compose.dev.yml:docker-compose.ci.yml
    parallel:
        matrix:
            - ENABLE_2FA: "true"
            - ENABLE_2FA: "false"
    before_script:
        - !reference [.docker-login]
        - mkdir -p ${ARCHIVE_DIR}
        # Configure stack
        - cp .env.example .env
        # Pull the Containers, as built in the previous tasks
        - $DOCKER_COMPOSE pull
        # Start full stack
        - $DOCKER_COMPOSE up -d
        # Sanity check
        - WEB_OK=$(docker ps -aq --filter status="running" --filter name="geocity-web-1") && if [ -z "$WEB_OK" ]; then exit 1; fi
        - POSTGRES_OK=$(docker ps -aq --filter status="running" --filter name="geocity-postgres-1") && if [ -z "$POSTGRES_OK" ]; then exit 1; fi
    script:
        - $DOCKER_COMPOSE stop web
        - >
            $RUN_IN_WEB sh -ec "
            coverage run --source='.' ./manage.py test --keepdb;
            coverage report -m;
            coverage xml"
    artifacts:
        name: test-output-images
        paths:
            - coverage.xml
            - ./geocity/apps/reports/tests/data

Coverage:
    stage: artifacts
    extends: [.docker-amd64, .when-to-run]
    image: busybox:latest
    variables:
        GIT_STRATEGY: none
    needs:
        - job: Run tests, 2FA
          parallel:
              matrix:
                  - ENABLE_2FA: "true"
          artifacts: true
    script:
        - find . -type f
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: coverage.xml

Build final prod image with the code:
    extends: [.build-images]
    needs:
        - job: Build images
          parallel:
              matrix:
                  - BUILD_IMAGE_NAME: geocity_web_prod
                    ENV: PROD
    stage: release
    variables:
        BUILD_IMAGE_NAME: geocity_web
        BUILD_ARGS: >
            --build-arg=CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE}
            --build-arg=CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG}
            --build-arg=IMAGE_NAME=geocity_web_prod
        DOCKERFILE: Dockerfile-prod

container_scanning:
    stage: release
    rules: !reference [.when-to-run, rules]
    needs: [Build final prod image with the code]
    variables:
        CS_IMAGE: ${CI_REGISTRY_IMAGE}/geocity_web:${CI_COMMIT_REF_SLUG}

Cleanup branch images:
    stage: cleanup
    environment:
        name: $CI_COMMIT_REF_SLUG
        action: stop
    when: manual
    extends:
        - .delete-tags-definition
        - .when-to-run
    needs: [Build images]
    variables:
        IMAGE_NAMES: external_statics geocity_pdf geocity_qgis geocity_web
        IMAGE_TAGS: ${CI_COMMIT_REF_SLUG}
