import { resolve } from 'node:path';
import inject from '@rollup/plugin-inject';
import { defineConfig, loadEnv } from 'vite';

export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), '');

  return {
    base: '/static/dist/',

    resolve: {
      alias: {
        '@': resolve(import.meta.dirname, './assets/js/'),
      },
    },

    plugins: [
      inject({
        // This is required for plugins like Idiomorph
        htmx: 'htmx.org',
      }),
    ],

    build: {
      outDir: './geocity/apps/core/static/dist',
      emptyOutDir: true,
      manifest: true,
      rollupOptions: {
        // Prevent unused entry-points exports from being removed in production mode
        // They might be imported and called by some inline JS
        preserveEntrySignatures: 'strict',
        input: {
          common: 'assets/js/common.js',
          htmx: 'assets/js/htmx.js',
          'submissions-list': 'assets/js/submissions-list.js',
          'archived-submissions-list': 'assets/js/archived-submissions-list.js',
          'submission-form': 'assets/js/submission-form.js',
          'submission-form-geo-time': 'assets/js/submission-form-geo-time.js',
          'submission-form-contacts': 'assets/js/submission-form-contacts.js',
          'submission-actions': 'assets/js/submission-actions.js',
          'submission-service-fees': 'assets/js/submission-service-fees.js',
          'advanced-geometry-widget': 'assets/js/advanced-geometry-widget.js',
          'submission-permits-form': 'assets/js/submission-permits-form.js',
          'submission-permits-details':
            'assets/js/submission-permits-details.js',
          'submission-permits-follow-up':
            'assets/js/submission-permits-follow-up.js',
        },
      },
    },

    server: {
      // Do not use another port if busy, its hardcoded elsewhere and required to work properly
      port: 3000,
      strictPort: true,
      host: true,
      proxy: {
        '^(?!/static/dist/)': {
          target: `http://web.${env.ISOLATED_NETWORK_NAME}:9000`,
        },
      },
      watch: {
        awaitWriteFinish: {
          stabilityThreshold: 800,
        },
        ignored: [
          '**/.vite/**',
          '**/__pycache__/**',
          '**/*.py',
          '**/*.pyc',
          '**/.venv/**',
          '**/.direnv/**',
          '**/.devenv/**',
          '**/.mypy_cache/**',
          '**/media/**',
          '**/static/**',
          '**/node_modules/**',
          '**/tests/**',
        ],
      },
    },
  };
});
